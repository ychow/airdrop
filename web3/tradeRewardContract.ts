import Web3 from 'web3'
import BigNumber from 'bignumber.js'
import Decimal from 'decimal.js'

import {DEFAULT_CONTRACT_MAP, TOKEN_ABI} from 'web3/constants'

BigNumber.config({EXPONENTIAL_AT: [-1e9, 1e9]})

let DECIMAL_NUM = 10

export class RewardContractAction {
    protected readonly web3: Web3
    protected readonly tokenInst: any
    protected readonly contractAddress: string
    protected readonly tokenAbi: any

    // ============ Constructor ============

    constructor(web3: Web3, contractName: string) {
        this.web3 = web3
        this.contractAddress = DEFAULT_CONTRACT_MAP[contractName].contractAddress
        this.tokenInst = new web3.eth.Contract(
            DEFAULT_CONTRACT_MAP[contractName].abi,
            DEFAULT_CONTRACT_MAP[contractName].contractAddress
        )
        this.tokenAbi = TOKEN_ABI
    }

    // ============ Functions ============

    public async at(from = '', index) {
        const _t = await this.tokenInst.methods.token().call()
        const _tokenIns = new this.web3.eth.Contract(this.tokenAbi, _t)

        DECIMAL_NUM = await _tokenIns.methods.decimals().call()

        const result = await this.tokenInst.methods.at(index * 1).call({from})

        if (result[2] > 0) {
            result[2] = new Decimal(result[2]).div(new Decimal(Math.pow(10, DECIMAL_NUM))).valueOf()
        }

        return result
    }

    public async getAirdropBalance(from) {
        const _t = await this.tokenInst.methods.token().call()
        const _tokenIns = new this.web3.eth.Contract(this.tokenAbi, _t)

        DECIMAL_NUM = await _tokenIns.methods.decimals().call()

        const result = await this.tokenInst.methods.getAirdropBalance().call({from})

        const res = result > 0 ? new Decimal(result).dividedBy(new Decimal(Math.pow(10, DECIMAL_NUM))) : result

        return res.valueOf()
    }

    public async getAirdropHistory(from) {
        const _t = await this.tokenInst.methods.token().call()
        const _tokenIns = new this.web3.eth.Contract(this.tokenAbi, _t)

        DECIMAL_NUM = await _tokenIns.methods.decimals().call()

        const result = await this.tokenInst.methods.getAirdropHistory().call({from})

        if (result[0] > 0) {
            result[0] = new Decimal(result[0]).dividedBy(new Decimal(Math.pow(10, DECIMAL_NUM))).valueOf()
        }
        if (result[1] > 0) {
            result[1] = new Decimal(result[1]).dividedBy(new Decimal(Math.pow(10, DECIMAL_NUM))).valueOf()
        }
        return result
    }

    public async getLength(from) {
        const result = await this.tokenInst.methods.getLength().call({from})
        return result
    }

    public async collect(from) {
        const result = await this.tokenInst.methods.collect().send({from})
        return result
    }

    public async claim(index = '', from) {
        const result = await this.tokenInst.methods.claim(index).send({from})
        return result
    }

    public async getDuration(from) {
        const result = await this.tokenInst.methods.coolingDuration().call({from})
        return result
    }

    public async getDiscount(from) {
        const result = await this.tokenInst.methods.discount().call({from})
        return result
    }
}
