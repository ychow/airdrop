import Web3 from 'web3'
import BigNumber from 'bignumber.js'

import {STAKE_ABI, STAKE_CONTRACT_ADDRESS} from './stakeConstants'

BigNumber.config({EXPONENTIAL_AT: [-1e9, 1e9]})

export class StakeContractAction {
    protected readonly web3: Web3
    protected readonly tokenInst: any

    // ============ Constructor ============

    constructor(web3: Web3) {
        this.web3 = web3
        this.tokenInst = new web3.eth.Contract(STAKE_ABI as any, STAKE_CONTRACT_ADDRESS)
    }

    // ============ Functions ============

    // public async getHeroStat(tokenId) {
    //     const _result = await this.tokenInst.methods.tokenURI(tokenId).call()
    //     return _result
    // }

    public async ovalAssets(from) {
        const result = await this.tokenInst.methods.ovalAssets().call({from})
        return result
    }

    public async usdtAssets(from = '') {
        const result = await this.tokenInst.methods.usdtAssets().call({from})
        return result
    }

    public async discount(from) {
        const result = await this.tokenInst.methods.discount().call({from})
        return result
    }

    public async getLockupReward(index, from) {
        const result = await this.tokenInst.methods.getLockupReward(index).call({from})
        return result
    }

    public async lockupRewardLength(from) {
        const result = await this.tokenInst.methods.lockupRewardLength().call({from})
        return result
    }

    public async pledge(amount, from) {
        const result = await this.tokenInst.methods.pledge(amount).send({from})
        return result
    }

    public async unPledge(amount, from) {
        const result = await this.tokenInst.methods.unPledge(amount).send({from})
        return result
    }

    public async claimUSDT(amount, from) {
        const result = await this.tokenInst.methods.claimUSDT(amount).send({from})
        return result
    }

    public async allowance(walletAddress: string) {
        const balance = await this.tokenInst.methods.allowance(walletAddress, STAKE_CONTRACT_ADDRESS).call()
        return balance
    }

    public async claimOVAL(id, from) {
        if (id === '') {
            const result = await this.tokenInst.methods.claimOVAL().send({from})
            return result
        } else {
            const result = await this.tokenInst.methods.claimOVAL(id).send({from})
            return result
        }
    }
}
