import Web3 from 'web3'
import BigNumber from 'bignumber.js'
import Decimal from 'decimal.js'

import {DEFAULT_CONTRACT_MAP, BSC_USDT_ABI} from 'web3/constants'

BigNumber.config({EXPONENTIAL_AT: [-1e9, 1e9]})
export class ContractAction {
    protected readonly web3: Web3
    protected readonly tokenInst: any
    protected readonly contractAddress: string
    protected readonly tokenAbi: any

    // ============ Constructor ============

    constructor(web3: Web3, contractName: string) {
        this.web3 = web3
        this.contractAddress = DEFAULT_CONTRACT_MAP[contractName].contractAddress
        this.tokenInst = new web3.eth.Contract(
            DEFAULT_CONTRACT_MAP[contractName].abi,
            DEFAULT_CONTRACT_MAP[contractName].contractAddress
        )
        this.tokenAbi = BSC_USDT_ABI
    }

    // ============ Functions ============

    // 空投
    public async mwDropTokens(data, from = '') {
        const result = await this.tokenInst.methods
            .mwDropTokens(data)
            .send({from})
        return result
    }

    // 获取空投币种
    public async tokenAddr() {
        const result = await this.tokenInst.methods.tokenAddr().call()
        return result
    }

    // 获取空投币种余额

    public async getBalance(tokenAddr) {
        const _tokenIns = new this.web3.eth.Contract(this.tokenAbi, tokenAddr)
        const _n = await _tokenIns.methods._symbol().call()
        const _demical = await _tokenIns.methods._decimals().call()
        const _balance = await _tokenIns.methods.balanceOf(this.contractAddress).call()

        return {
            symbolName: _n,
            symbolBalance: new Decimal(_balance).div(new Decimal(Math.pow(10, _demical))).valueOf(),
            demical: _demical
        }
    }

    // 更新空投币种
    public async mwUpdateTokenAddress(tokenAddr, from) {
        const result = await this.tokenInst.methods.mwUpdateTokenAddress(tokenAddr).send({from})

        return result
    }

    // 提币
    public async mwWithdrawTokens(tokenAddr, from) {
        const result = await this.tokenInst.methods.mwWithdrawTokens(tokenAddr).send({from})
        return result
    }

    // 充值
    public async withDraw(num, tokenAddr, from) {
        const _tokenIns = new this.web3.eth.Contract(this.tokenAbi, tokenAddr)

        const _demical = await _tokenIns.methods._decimals().call()

        const _m: any = new BigNumber(+num).times(new BigNumber(Math.pow(10, _demical))).toString()

        const result = await _tokenIns.methods.transfer(this.contractAddress, _m).send({from})
        return result
    }
}
