import {memo, useState, useEffect, useCallback, useRef, useMemo} from 'react'
import styled from 'styled-components'

import {setTokenCookie, getTokenCookie} from 'utils/auth-cookie'
import {useOvalConfig} from 'utils/globalConfig-context'
import {useAuth} from 'utils/auth-context'
// import {useIsLogin} from 'hooks/swr/useUser'
import {DEFAULT_RPC} from 'web3/constants'
import {formatAddress} from 'web3/format'

import Loading from 'components/common/ButtonLoading'

// const SIGN_MESSAGE = 'Oval-Verification'
// let ethersProvider
// let web3
const WalletLogin = memo(({chain = 'bsc'}: any) => {
    // const walletInited = useRef(false)
    const userAddressRef = useRef('')
    const {configData} = useOvalConfig()
    const [isMetamaskInstalled, setIsMetamaskInstalled] = useState(false)
    const [signResult, setSigned] = useState(false)
    const [isChainCorrect, setIsChainCorrect] = useState(false)
    const RPC_CONFIG = DEFAULT_RPC[chain]

    // const [signMsg, setSignMsg] = useState('')

    const [walletLoading, setWalletLoading] = useState(true)
    const loginPendingRef = useRef(null)

    const {userToken, userAddress: address, setToken, setAddress, setChainCorrect} = useAuth()

    const signFullfilled = useMemo(() => {
        // localStorage.signResult = signResult
        // localStorage.userToken = userToken

        return (signResult || address) && userToken
    }, [signResult, userToken, address])

    const updateUserAddress = (newAddress: string) => {
        setAddress(newAddress)
    }

    useEffect(() => {
        if (!address) {
            const {ethereum} = window as any

            if (typeof ethereum !== 'undefined') {
                // setWalletLoading(true)
                ethereum.enable().then((res) => {
                    if (res?.length) {
                        updateUserAddress(res[0])
                    }
                })
            }
        }
    }, [address])

    // ============ eth new account event handler ============
    const handleNewAccounts = () => {
        handleLogout()
        connectWallet()
    }

    // ============ remove all account func ============
    // const removeAccount = () => {
    //     setToken('')
    //     setSigned(false)
    //     setAddress('')
    // }

    // ============ login token handler ============

    const connectWallet = async () => {
        const {ethereum} = window as any

        if (typeof ethereum !== 'undefined') {
            setWalletLoading(true)
            await ethereum
                .enable()
                .then((res) => {
                    if (res?.length) {
                        updateUserAddress(res[0])
                        handleSign()
                    }
                })
                .catch(() => {
                    setWalletLoading(false)
                })
        }
    }

    // ============ check chain then check address ============

    const checkChain = (needResign = '', addressCorrect = false) => {
        const {ethereum} = window as any
        const {chainId} = ethereum

        if (chainId === (configData?.rpc_config?.chainId || RPC_CONFIG.chainId) || !chainId) {
            setIsChainCorrect(true)
            setChainCorrect(true)
            needResign && !addressCorrect && handleSign()
        } else {
            setIsChainCorrect(false)
            setChainCorrect(false)
        }

        setWalletLoading(false)
    }

    const checkAddress = (needResign = '', chainAddress = '') => {
        const {ethereum} = window as any
        const {selectedAddress} = ethereum
        if (selectedAddress) {
            if (selectedAddress.toUpperCase() === chainAddress.toUpperCase()) {
                updateUserAddress(selectedAddress)
                checkChain('', true)
                setSigned(true)
            } else {
                // removeAccount()
                checkChain(needResign)
            }
        } else {
            // removeAccount()
            checkChain()
            if (loginPendingRef.current) {
                connectWallet()
                loginPendingRef.current = false
            }
        }
    }

    // ============ page loaded todos and chain events ============

    const initWallet = (chainAddress = '') => {
        const {ethereum} = window as any
        if (typeof ethereum !== 'undefined' && ethereum.isMetaMask && typeof ethereum.request === 'function') {
            setIsMetamaskInstalled(true)

            ethereum.on('chainChanged', () => {
                checkAddress('needResign', userAddressRef.current)
            })

            ethereum.on('accountsChanged', () => {
                handleNewAccounts()
            })

            checkAddress('', chainAddress)
        } else {
            setIsMetamaskInstalled(false)
        }
    }

    const checkAccount = () => {
        const token = getTokenCookie()

        if (token) {
            initWallet()

            userAddressRef.current = token
            setWalletLoading(false)

        } else {
            initWallet()
        }
    }

    useEffect(() => {
        checkAccount()
        typeof document !== 'undefined' &&
            document.addEventListener(
                'visibilitychange',
                () => {
                    checkAccount()
                },
                false
            )
    }, [configData, chain])

    // const {data: userRes} = useIsLogin({token: userToken})
    // useEffect(() => {
    //     if (Object.keys(userRes?.data).length && !walletInited.current) {
    //         const {chain_address} = userRes.data
    //         initWallet(chain_address)
    //     }
    // }, [userRes])

    // ============ show/hide sign and emit sign ============


    const handleSign = async () => {
        const ticket = '空投工具'
        const {ethereum} = window as any

        try {
            setWalletLoading(true)
            const from = ethereum.selectedAddress
            const msg = `0x${Buffer.from(ticket, 'utf8').toString('hex')}`
            const sign = await ethereum.request({
                method: 'personal_sign',
                params: [msg, from, 'Example password'],
            })

            handleLogin({
                from,
                sign,
                msg: ticket,
                chain: 'bsc',
                origin: 'Oval',
            })
        } catch (err) {
            setWalletLoading(false)
        }
    }

    // ============ Login and Logout functions ============
    const handleToken = useCallback((token) => {
        setTokenCookie(null, token)
        setToken(token)
    }, [])

    const handleLogin = (data) => {

        localStorage.setItem('reg_account', data.from)
        localStorage.setItem('access_token', data.from)
        handleToken(data.from)
        setSigned(true)
        setWalletLoading(false)
    }

    const handleLogout = () => {
        setToken('')
        updateUserAddress('')
        setSigned(false)
    }

    // ============ Switch chain function ============

    const switchChain = async (needLogin = '') => {
        // setWalletLoading(true)
        const {ethereum} = window as any
        // const {rpc_config} = configData
        // switch chain

        if (chain === 'eth') {
            ethereum.request({
                method: 'wallet_switchEthereumChain',
                params: [{ chainId: RPC_CONFIG.chainId}],
            })
            
        } else {
            ethereum.request({
                method: 'wallet_addEthereumChain',
                params: [RPC_CONFIG],
            })
        }
        
        if (needLogin) {
            loginPendingRef.current = true
        }
    }

    return (
        <WalletLoginWrapper>
            {walletLoading && <Loading />}
            {/* MetaMask not installed */}
            {/* Method: Click to install MetaMask */}
            {!isMetamaskInstalled && (
                <a target="_blank" rel="noopener noreferrer" href="https://metamask.io/">
                    <WalletLoginButton>Install MetaMask</WalletLoginButton>
                </a>
            )}

            {/* MetaMask installed but not right chain and account linked */}
            {/* Method: try switch chain to BSC network */}
            {isMetamaskInstalled && !isChainCorrect && (
                <WalletLoginButton onClick={() => switchChain('needLogin')}>Switch Chain</WalletLoginButton>
            )}

            {/* MetaMask installed and chain right but wallet not connected */}
            {/* Method: try connect to wallet */}
            {isMetamaskInstalled && isChainCorrect && !address && (
                <WalletLoginButton onClick={connectWallet}>Connect Wallet</WalletLoginButton>
            )}

            {/* MetaMask installed, chain right and wallet connected but not signed */}
            {/* Method: try sign signature */}
            {isMetamaskInstalled && isChainCorrect && address && !signFullfilled && (
                <WalletLoginButton onClick={handleSign}>Connect Wallet</WalletLoginButton>
            )}

            {/* MetaMask installed, chain right , wallet connected and signature signed */}
            {/* All tasks fullfilled */}
            {/* Method: try logout (clear address and token) */}
            {isMetamaskInstalled && isChainCorrect && address && signFullfilled && (
                <WalletLoginButton title="Disconnect" onClick={() => handleLogout()}>
                    {/* <img src={mkIcon} alt="metamask" className="metamask" /> */}
                    {formatAddress(address)}{' '}
                    <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M9.42895 2.36512V4.19416C10.6496 4.98954 11.4578 6.36387 11.4578 7.92954C11.4578 10.3909 9.46191 12.3865 6.99995 12.3865C4.53857 12.3865 2.5427 10.3909 2.5427 7.92954C2.5427 6.538 3.18028 5.29608 4.17953 4.47825V2.55354C2.24724 3.56971 0.929199 5.59475 0.929199 7.92896C0.929199 11.2817 3.64753 14 7.00053 14C10.353 14 13.0707 11.2817 13.0707 7.92896C13.0704 5.43987 11.5727 3.30225 9.42895 2.36512Z"
                            fill="black"
                        />
                        <path
                            d="M7.89339 6.28892C7.89339 6.68121 7.49352 6.99971 7.00002 6.99971C6.5071 6.99971 6.10693 6.6815 6.10693 6.28892V0.710792C6.10693 0.318208 6.5071 0 7.00002 0C7.49352 0 7.89339 0.318208 7.89339 0.710792V6.28892Z"
                            fill="black"
                        />
                    </svg>
                </WalletLoginButton>
            )}

        </WalletLoginWrapper>
    )
})

export default WalletLogin

const WalletLoginWrapper = styled.div`
    height: 36px;
    background: linear-gradient(103.01deg, #69e77f 0%, #d0fb48 100%);
    border-radius: 12px;
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative;
    cursor: pointer;
    overflow: hidden;

    a {
        text-decoration: none;
    }
`

const WalletLoginButton = styled.div`
    padding: 0 20px;
    height: 36px;
    font-family: DINPro;
    font-style: normal;
    font-weight: bold;
    font-size: 14px;
    line-height: 20px;
    color: #101215;
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;

    &:hover {
        background: linear-gradient(103.01deg, #4eda67 0%, #b6e12f 100%);
    }

    svg {
        margin-left: 8px;
    }
    .metamask {
        margin-right: 8px;
        width: 20px;
        height: 20px;
    }
`
