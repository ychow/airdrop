import Web3 from 'web3'
import BigNumber from 'bignumber.js'
// import Decimal from 'decimal.js'

import {DEFAULT_CONTRACT_MAP} from 'web3/constants'

BigNumber.config({EXPONENTIAL_AT: [-1e9, 1e9]})

export class DexContractAction {
    protected readonly web3: Web3
    protected readonly tokenInst: any
    protected readonly contractAddress: string

    // ============ Constructor ============

    constructor(web3: Web3, contractName: string) {
        this.web3 = web3
        this.contractAddress = DEFAULT_CONTRACT_MAP[contractName].contractAddress
        this.tokenInst = new web3.eth.Contract(
            DEFAULT_CONTRACT_MAP[contractName].abi,
            DEFAULT_CONTRACT_MAP[contractName].contractAddress
        )
    }

    // ============ Functions ============

    public async deposit(to = '', amount = '', from = '') {
        const decimal = 18
        // const targetAmount = new BigNumber(+amount).shiftedBy(+decimal).toString()
        const targetAmount = new BigNumber(+amount).times(new BigNumber(Math.pow(10, decimal))).toString()
        // const targetAmount = this.web3.utils.toBN(+amount).pow(18)
        const result = await this.tokenInst.methods.deposit(to, targetAmount).send({from})
        return result
    }
}
