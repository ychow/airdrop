import Web3 from 'web3'
import BigNumber from 'bignumber.js'
import Decimal from 'decimal.js'

import {DEFAULT_CONTRACT_MAP} from 'web3/constants'

BigNumber.config({EXPONENTIAL_AT: [-1e9, 1e9]})
export class ContractAction {
    protected readonly web3: Web3
    protected readonly tokenInst: any
    protected readonly contractAddress: string

    // ============ Constructor ============

    constructor(web3: Web3, contractName: string) {
        this.web3 = web3
        this.contractAddress = DEFAULT_CONTRACT_MAP[contractName].contractAddress
        this.tokenInst = new web3.eth.Contract(
            DEFAULT_CONTRACT_MAP[contractName].abi,
            DEFAULT_CONTRACT_MAP[contractName].contractAddress
        )
    }

    // ============ Functions ============

    public async getPureBalance(walletAddress: string) {
        const balance = await this.tokenInst.methods.balanceOf(walletAddress).call()
        return balance
    }

    public async getDecimal() {
        const decimal = await this.tokenInst.methods.decimals().call()
        return decimal
    }

    public async getDecimaledBalance(walletAddress: string, precision = null, fixedType = 'ROUND_DOWN') {
        const balance = await this.tokenInst.methods.balanceOf(walletAddress).call()
        const decimal = await this.tokenInst.methods.decimals().call()
        const result = new Decimal(balance).div(new Decimal(Math.pow(10, decimal)))
        return precision ? +result.toFixed(precision, Decimal[fixedType]).valueOf() : +result.valueOf()
    }

    public async transfer(to = '', amount = '', from = '') {
        const decimal = await this.tokenInst.methods.decimals().call()
        // const targetAmount = new BigNumber(+amount).shiftedBy(+decimal).toString()
        const targetAmount = new BigNumber(+amount).times(new BigNumber(Math.pow(10, decimal))).toString()
        // const targetAmount = this.web3.utils.toBN(+amount).pow(18)
        const result = await this.tokenInst.methods.transfer(to, targetAmount).send({from})
        return result
    }

    public async approve(from = '') {
        // const decimal = await this.tokenInst.methods.decimals().call()
        // const targetAmount = new BigNumber(+amount).shiftedBy(+decimal).toString()
        const targetAmount = '115792089237316195423570985008687907853269984665640564039457584007913129639934'

        const result = await this.tokenInst.methods
            .approve(DEFAULT_CONTRACT_MAP['DEX'].contractAddress, targetAmount)
            .send({from})
        return result
    }

    public async allowance(walletAddress: string) {
        const balance = await this.tokenInst.methods
            .allowance(walletAddress, DEFAULT_CONTRACT_MAP['DEX'].contractAddress)
            .call()
        return balance
    }
    public async allowanceOval(walletAddress, spender) {
        const balance = await this.tokenInst.methods.allowance(walletAddress, spender).call()
        return balance
    }
    public async approveAll(address, from = '') {
        const targetAmount = '115792089237316195423570985008687907853269984665640564039457584007913129639934'
        const result = await this.tokenInst.methods.approve(address, targetAmount).send({from})
        return result
    }
    public async allowanceContribute(walletAddress, spender) {
        const balance = await this.tokenInst.methods.allowance(walletAddress, spender).call()
        return balance
    }
}
