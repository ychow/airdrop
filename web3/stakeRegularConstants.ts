import Web3 from 'web3'
import BigNumber from 'bignumber.js'
import Decimal from 'decimal.js'

import {DEFAULT_CONTRACT_MAP, TOKEN_ABI} from 'web3/constants'

BigNumber.config({EXPONENTIAL_AT: [-1e9, 1e9]})

export class StakeRegularContractAction {
    protected readonly web3: Web3
    protected readonly tokenInst: any
    protected readonly contractAddress: string
    protected readonly tokenAbi: any

    // ============ Constructor ============

    constructor(web3: Web3, contractName: string) {
        this.web3 = web3
        this.contractAddress = DEFAULT_CONTRACT_MAP[contractName].contractAddress
        this.tokenInst = new web3.eth.Contract(
            DEFAULT_CONTRACT_MAP[contractName].abi,
            DEFAULT_CONTRACT_MAP[contractName].contractAddress
        )
        this.tokenAbi = TOKEN_ABI
    }

    // ============ Functions ============

    public async getPoolInfo(from = '', index) {
        const result = await this.tokenInst.methods.getPoolInfo(index * 1).call({from})

        return result
    }

    // 质押
    public async pledge(amount, index, from) {
        const _m: any = new BigNumber(+amount).times(new BigNumber(Math.pow(10, 18))).toString()

        const result = await this.tokenInst.methods.pledge(_m, index * 1).send({from})
        return result
    }

    // 领取一笔到期的质押本金（质押ID，池子下标）
    public async claimPrinciple(pledgeId, poolIndex, from) {
        const result = await this.tokenInst.methods.claimPrinciple(pledgeId * 1, poolIndex * 1).send({from})
        return result
    }

    // 领取所有的质押本金（池子下标）
    public async claimAllDuePrinciple(poolIndex, from) {
        const result = await this.tokenInst.methods.claimAllDuePrinciple(poolIndex * 1).send({from})
        return result
    }

    // 领取一笔到期的奖励（质押ID，池子下标）
    public async claimInterest(pledgeId, poolIndex, from) {
        const result = await this.tokenInst.methods.claimInterest(pledgeId * 1, poolIndex * 1).send({from})
        return result
    }

    // 领取所有的奖励（池子下标）
    public async claimAllDueInterest(poolIndex, from) {
        const result = await this.tokenInst.methods.claimAllDueInterest(poolIndex * 1).send({from})
        return result
    }

    // 个人质押中的记录的长度
    public async getPrincipleLength(from = '', poolIndex) {
        const result = await this.tokenInst.methods.getPrincipleLength(poolIndex * 1).call({from})

        return result
    }

    // 质押 详情（质押记录数组的下标，池子下标
    public async getPrincipleRecord(from = '', index, poolIndex) {
        const result = await this.tokenInst.methods.getPrincipleRecord(index * 1, poolIndex * 1).call({from})
        if (result[1] > 0) {
            result[1] = new Decimal(result[1]).dividedBy(new Decimal(Math.pow(10, 18))).valueOf()
        }

        return result
    }

    // 个人质押中的奖励的长度
    public async getInterestLength(from = '', poolIndex) {
        const result = await this.tokenInst.methods.getInterestLength(poolIndex * 1).call({from})

        return result
    }

    // 奖励 详情（质押记录数组的下标，池子下标
    public async getInterestRecord(from = '', index, poolIndex) {
        const result = await this.tokenInst.methods.getInterestRecord(index * 1, poolIndex * 1).call({from})

        if (result[1] > 0) {
            result[1] = new Decimal(result[1]).dividedBy(new Decimal(Math.pow(10, 18))).valueOf()
        }

        return result
    }
}
