export const BSC_USDT_ABI = [
    {inputs: [], payable: false, stateMutability: 'nonpayable', type: 'constructor'},
    {
        anonymous: false,
        inputs: [
            {indexed: true, internalType: 'address', name: 'owner', type: 'address'},
            {indexed: true, internalType: 'address', name: 'spender', type: 'address'},
            {indexed: false, internalType: 'uint256', name: 'value', type: 'uint256'},
        ],
        name: 'Approval',
        type: 'event',
    },
    {
        anonymous: false,
        inputs: [
            {indexed: true, internalType: 'address', name: 'previousOwner', type: 'address'},
            {indexed: true, internalType: 'address', name: 'newOwner', type: 'address'},
        ],
        name: 'OwnershipTransferred',
        type: 'event',
    },
    {
        anonymous: false,
        inputs: [
            {indexed: true, internalType: 'address', name: 'from', type: 'address'},
            {indexed: true, internalType: 'address', name: 'to', type: 'address'},
            {indexed: false, internalType: 'uint256', name: 'value', type: 'uint256'},
        ],
        name: 'Transfer',
        type: 'event',
    },
    {
        constant: true,
        inputs: [],
        name: '_decimals',
        outputs: [{internalType: 'uint8', name: '', type: 'uint8'}],
        payable: false,
        stateMutability: 'view',
        type: 'function',
    },
    {
        constant: true,
        inputs: [],
        name: '_name',
        outputs: [{internalType: 'string', name: '', type: 'string'}],
        payable: false,
        stateMutability: 'view',
        type: 'function',
    },
    {
        constant: true,
        inputs: [],
        name: '_symbol',
        outputs: [{internalType: 'string', name: '', type: 'string'}],
        payable: false,
        stateMutability: 'view',
        type: 'function',
    },
    {
        constant: true,
        inputs: [
            {internalType: 'address', name: 'owner', type: 'address'},
            {internalType: 'address', name: 'spender', type: 'address'},
        ],
        name: 'allowance',
        outputs: [{internalType: 'uint256', name: '', type: 'uint256'}],
        payable: false,
        stateMutability: 'view',
        type: 'function',
    },
    {
        constant: false,
        inputs: [
            {internalType: 'address', name: 'spender', type: 'address'},
            {internalType: 'uint256', name: 'amount', type: 'uint256'},
        ],
        name: 'approve',
        outputs: [{internalType: 'bool', name: '', type: 'bool'}],
        payable: false,
        stateMutability: 'nonpayable',
        type: 'function',
    },
    {
        constant: true,
        inputs: [{internalType: 'address', name: 'account', type: 'address'}],
        name: 'balanceOf',
        outputs: [{internalType: 'uint256', name: '', type: 'uint256'}],
        payable: false,
        stateMutability: 'view',
        type: 'function',
    },
    {
        constant: false,
        inputs: [{internalType: 'uint256', name: 'amount', type: 'uint256'}],
        name: 'burn',
        outputs: [{internalType: 'bool', name: '', type: 'bool'}],
        payable: false,
        stateMutability: 'nonpayable',
        type: 'function',
    },
    {
        constant: true,
        inputs: [],
        name: 'decimals',
        outputs: [{internalType: 'uint8', name: '', type: 'uint8'}],
        payable: false,
        stateMutability: 'view',
        type: 'function',
    },
    {
        constant: false,
        inputs: [
            {internalType: 'address', name: 'spender', type: 'address'},
            {internalType: 'uint256', name: 'subtractedValue', type: 'uint256'},
        ],
        name: 'decreaseAllowance',
        outputs: [{internalType: 'bool', name: '', type: 'bool'}],
        payable: false,
        stateMutability: 'nonpayable',
        type: 'function',
    },
    {
        constant: true,
        inputs: [],
        name: 'getOwner',
        outputs: [{internalType: 'address', name: '', type: 'address'}],
        payable: false,
        stateMutability: 'view',
        type: 'function',
    },
    {
        constant: false,
        inputs: [
            {internalType: 'address', name: 'spender', type: 'address'},
            {internalType: 'uint256', name: 'addedValue', type: 'uint256'},
        ],
        name: 'increaseAllowance',
        outputs: [{internalType: 'bool', name: '', type: 'bool'}],
        payable: false,
        stateMutability: 'nonpayable',
        type: 'function',
    },
    {
        constant: false,
        inputs: [{internalType: 'uint256', name: 'amount', type: 'uint256'}],
        name: 'mint',
        outputs: [{internalType: 'bool', name: '', type: 'bool'}],
        payable: false,
        stateMutability: 'nonpayable',
        type: 'function',
    },
    {
        constant: true,
        inputs: [],
        name: 'name',
        outputs: [{internalType: 'string', name: '', type: 'string'}],
        payable: false,
        stateMutability: 'view',
        type: 'function',
    },
    {
        constant: true,
        inputs: [],
        name: 'owner',
        outputs: [{internalType: 'address', name: '', type: 'address'}],
        payable: false,
        stateMutability: 'view',
        type: 'function',
    },
    {
        constant: false,
        inputs: [],
        name: 'renounceOwnership',
        outputs: [],
        payable: false,
        stateMutability: 'nonpayable',
        type: 'function',
    },
    {
        constant: true,
        inputs: [],
        name: 'symbol',
        outputs: [{internalType: 'string', name: '', type: 'string'}],
        payable: false,
        stateMutability: 'view',
        type: 'function',
    },
    {
        constant: true,
        inputs: [],
        name: 'totalSupply',
        outputs: [{internalType: 'uint256', name: '', type: 'uint256'}],
        payable: false,
        stateMutability: 'view',
        type: 'function',
    },
    {
        constant: false,
        inputs: [
            {internalType: 'address', name: 'recipient', type: 'address'},
            {internalType: 'uint256', name: 'amount', type: 'uint256'},
        ],
        name: 'transfer',
        outputs: [{internalType: 'bool', name: '', type: 'bool'}],
        payable: false,
        stateMutability: 'nonpayable',
        type: 'function',
    },
    {
        constant: false,
        inputs: [
            {internalType: 'address', name: 'sender', type: 'address'},
            {internalType: 'address', name: 'recipient', type: 'address'},
            {internalType: 'uint256', name: 'amount', type: 'uint256'},
        ],
        name: 'transferFrom',
        outputs: [{internalType: 'bool', name: '', type: 'bool'}],
        payable: false,
        stateMutability: 'nonpayable',
        type: 'function',
    },
    {
        constant: false,
        inputs: [{internalType: 'address', name: 'newOwner', type: 'address'}],
        name: 'transferOwnership',
        outputs: [],
        payable: false,
        stateMutability: 'nonpayable',
        type: 'function',
    },
]

export const TOKEN_ABI = [
    {
        anonymous: false,
        inputs: [
            {
                indexed: true,
                internalType: 'address',
                name: 'owner',
                type: 'address',
            },
            {
                indexed: true,
                internalType: 'address',
                name: 'spender',
                type: 'address',
            },
            {
                indexed: false,
                internalType: 'uint256',
                name: 'value',
                type: 'uint256',
            },
        ],
        name: 'Approval',
        type: 'event',
    },
    {
        anonymous: false,
        inputs: [
            {
                indexed: true,
                internalType: 'address',
                name: 'from',
                type: 'address',
            },
            {
                indexed: true,
                internalType: 'address',
                name: 'to',
                type: 'address',
            },
            {
                indexed: false,
                internalType: 'uint256',
                name: 'value',
                type: 'uint256',
            },
        ],
        name: 'Transfer',
        type: 'event',
    },
    {
        inputs: [
            {
                internalType: 'address',
                name: 'owner',
                type: 'address',
            },
            {
                internalType: 'address',
                name: 'spender',
                type: 'address',
            },
        ],
        name: 'allowance',
        outputs: [
            {
                internalType: 'uint256',
                name: '',
                type: 'uint256',
            },
        ],
        stateMutability: 'view',
        type: 'function',
    },
    {
        inputs: [
            {
                internalType: 'address',
                name: 'spender',
                type: 'address',
            },
            {
                internalType: 'uint256',
                name: 'amount',
                type: 'uint256',
            },
        ],
        name: 'approve',
        outputs: [
            {
                internalType: 'bool',
                name: '',
                type: 'bool',
            },
        ],
        stateMutability: 'nonpayable',
        type: 'function',
    },
    {
        inputs: [
            {
                internalType: 'address',
                name: 'account',
                type: 'address',
            },
        ],
        name: 'balanceOf',
        outputs: [
            {
                internalType: 'uint256',
                name: '',
                type: 'uint256',
            },
        ],
        stateMutability: 'view',
        type: 'function',
    },
    {
        inputs: [],
        name: 'decimals',
        outputs: [
            {
                internalType: 'uint8',
                name: '',
                type: 'uint8',
            },
        ],
        stateMutability: 'view',
        type: 'function',
    },
    {
        inputs: [],
        name: 'name',
        outputs: [
            {
                internalType: 'string',
                name: '',
                type: 'string',
            },
        ],
        stateMutability: 'view',
        type: 'function',
    },
    {
        inputs: [],
        name: 'symbol',
        outputs: [
            {
                internalType: 'string',
                name: '',
                type: 'string',
            },
        ],
        stateMutability: 'view',
        type: 'function',
    },
    {
        inputs: [],
        name: 'totalSupply',
        outputs: [
            {
                internalType: 'uint256',
                name: '',
                type: 'uint256',
            },
        ],
        stateMutability: 'view',
        type: 'function',
    },
    {
        inputs: [
            {
                internalType: 'address',
                name: 'to',
                type: 'address',
            },
            {
                internalType: 'uint256',
                name: 'amount',
                type: 'uint256',
            },
        ],
        name: 'transfer',
        outputs: [
            {
                internalType: 'bool',
                name: '',
                type: 'bool',
            },
        ],
        stateMutability: 'nonpayable',
        type: 'function',
    },
    {
        inputs: [
            {
                internalType: 'address',
                name: 'from',
                type: 'address',
            },
            {
                internalType: 'address',
                name: 'to',
                type: 'address',
            },
            {
                internalType: 'uint256',
                name: 'amount',
                type: 'uint256',
            },
        ],
        name: 'transferFrom',
        outputs: [
            {
                internalType: 'bool',
                name: '',
                type: 'bool',
            },
        ],
        stateMutability: 'nonpayable',
        type: 'function',
    },
]

const AIRDROP_ABI = [
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "_tokenAddr",
          "type": "address"
        }
      ],
      "stateMutability": "nonpayable",
      "type": "constructor"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "address",
          "name": "previousOwner",
          "type": "address"
        },
        {
          "indexed": true,
          "internalType": "address",
          "name": "newOwner",
          "type": "address"
        }
      ],
      "name": "OwnershipTransferred",
      "type": "event"
    },
    {
      "inputs": [
        {
          "internalType": "address[]",
          "name": "_recipients",
          "type": "address[]"
        },
        {
          "internalType": "uint256[]",
          "name": "_amounts",
          "type": "uint256[]"
        }
      ],
      "name": "mwDropTokens",
      "outputs": [
        {
          "internalType": "bool",
          "name": "",
          "type": "bool"
        }
      ],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "newTokenAddr",
          "type": "address"
        }
      ],
      "name": "mwUpdateTokenAddress",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address payable",
          "name": "beneficiary",
          "type": "address"
        }
      ],
      "name": "mwWithdrawEther",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "beneficiary",
          "type": "address"
        }
      ],
      "name": "mwWithdrawTokens",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "owner",
      "outputs": [
        {
          "internalType": "address",
          "name": "",
          "type": "address"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "renounceOwnership",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "tokenAddr",
      "outputs": [
        {
          "internalType": "address",
          "name": "",
          "type": "address"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "newOwner",
          "type": "address"
        }
      ],
      "name": "transferOwnership",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    }
  ]

export const TEST_CONTRACT_MAP = {
    bsc: {
        abi: AIRDROP_ABI,
        contractAddress: '0xda32a2d6963e9764853c6795da1404ee36c06da9',
        usdtAddress: '0x338f031348b151b2f69b0abee1f795261b298e2a',
    },
    eth: {
        abi: AIRDROP_ABI,
        contractAddress: '0xa639bfcd15452e1ec963e8648a9677bbad541798',
        usdtAddress: '0x55d398326f99059ff775485246999027b3197955',
    },
}

export const MAIN_CONTRACT_MAP = {
    bsc: {
        abi: AIRDROP_ABI,
        contractAddress: '0xda32a2d6963e9764853c6795da1404ee36c06da9',
        usdtAddress: '0x55d398326f99059ff775485246999027b3197955',
    },
    eth: {
        abi: AIRDROP_ABI,
        contractAddress: '0xa639bfcd15452e1ec963e8648a9677bbad541798',
        usdtAddress: '0xdAC17F958D2ee523a2206206994597C13D831ec7',
    },
}

export const DEFAULT_CONTRACT_MAP = process.env.NEXT_PUBLIC_APP_ENV === 'prod' ? MAIN_CONTRACT_MAP : TEST_CONTRACT_MAP

export const BSC_TESTNET_RPC = {
    chainId: '0x61',
    rpcUrls: ['https://data-seed-prebsc-1-s1.binance.org:8545'],
    chainName: 'BSC TESTNET',
    nativeCurrency: {name: 'BNB', decimals: 18, symbol: 'BNB'},
    blockExplorerUrls: ['https://testnet.bscscan.com/'],
}

export const BSC_MAINNET_RPC = {
    chainId: '0x38',
    rpcUrls: ['https://bsc-dataseed.binance.org'],
    chainName: 'BSC MAINNET',
    nativeCurrency: {name: 'BNB', decimals: 18, symbol: 'BNB'},
    blockExplorerUrls: ['https://bscscan.com'],
}

export const ETH_TESTNET_RPC = {
    chainId: '0x3',
    rpcUrls: ['https://ropsten.infura.io/v3/'],
    chainName: 'Ropsten TESTNET',
    nativeCurrency: {name: 'ETH', decimals: 18, symbol: 'ETH'},
    blockExplorerUrls: ['https://ropsten.etherscan.io'],
}

export const ETH_MAINNET_RPC = {
    chainId: '0x1',
    rpcUrls: ['https://mainnet.infura.io/v3/'],
    chainName: 'ETH MAINNET',
    nativeCurrency: {name: 'ETH', decimals: 18, symbol: 'ETH'},
    blockExplorerUrls: ['https://etherscan.io'],
}

export const DEFAULT_RPC = process.env.NEXT_PUBLIC_APP_ENV === 'prod' ? {bsc: BSC_MAINNET_RPC, eth: ETH_MAINNET_RPC} : {bsc: BSC_TESTNET_RPC, eth: ETH_TESTNET_RPC}

export const DEPOSIT_ADDRESS = '0xCC1F486fe6563aF2157A597A94F9C636E9b5deD3'
