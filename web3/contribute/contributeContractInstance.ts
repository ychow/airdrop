import Web3 from 'web3'
import {CONTRIBUTE_ABI, CONTRIBUTE_CONTRACT_ADDRESS} from '../contribute/contributeConstants'

export class ContributeContractAction {
    protected readonly web3: Web3
    protected readonly tokenInst: any
    protected readonly contractAddress: string

    // ============ Constructor ============

    constructor(web3: Web3) {
        this.web3 = web3
        this.contractAddress = CONTRIBUTE_CONTRACT_ADDRESS
        this.tokenInst = new web3.eth.Contract(CONTRIBUTE_ABI as any, CONTRIBUTE_CONTRACT_ADDRESS)
    }

    public async postDonate(token, amount, from = '') {
        const result = await this.tokenInst.methods.donate(token, amount).send({from})
        return result
    }
    public async getStartTime() {
        const result = await this.tokenInst.methods.startTime().call()
        return result
    }

    public async getEndTime(wallet) {
        const result = await this.tokenInst.methods.endTime(wallet).call()
        return result
    }

    public async getTotalAmount() {
        const result = await this.tokenInst.methods.getTotalAmount().call()
        return result
    }
    public async getConfigInfo(wallet) {
        const result = await this.tokenInst.methods.getConfigInfo(wallet).call()
        return result
    }
    public async isPaused() {
        const result = await this.tokenInst.methods.isPaused().call()
        return result
    }
}
