import {createContext, useState, useContext, ReactNode, useEffect} from 'react'
import {getTokenCookie, setTokenCookie} from 'utils/auth-cookie'

const AuthContext = createContext<
    | {
          userToken: string
          userAddress: string
          setToken: (token: string) => void
          setAddress: (address: string) => void
          chainCorrect: boolean
          setChainCorrect: (flag: boolean) => void
          allFullfilled: boolean
          pending?: boolean
      }
    | undefined
>(undefined)

export const AuthProvider = ({children}: {children: ReactNode}) => {
    const [userToken, setUserToken] = useState<string>('')
    const [userAddress, setUserAddress] = useState<string>('')
    const [chainCorrect, setChainCorrect] = useState<boolean>(false)
    // const [pending, setPending] = useState(false)
    const setToken = (token: string) => {
        if (!token) {
            setTokenCookie(null, '')
        }
        setUserToken(token)
    }

    const setAddress = (address: string) => {
        setUserAddress(address)
    }

    useEffect(() => {
        const token = getTokenCookie()
        // setPending(false)
        setUserToken(token)
    }, [])

    // const setFullfilled = (flag) => {
    //     setFullfilled(flag)
    // }

    return (
        <AuthContext.Provider
            value={{
                userToken,
                setToken,
                userAddress,
                setAddress,
                chainCorrect,
                setChainCorrect,
                allFullfilled: Boolean(userAddress && userToken),
            }}
        >
            {children}
        </AuthContext.Provider>
    )
}

export const useAuth = () => {
    const context = useContext(AuthContext)

    if (!context) {
        throw new Error('useAuth必须在AuthProvider中使用')
    } else {
        return context
    }
}
