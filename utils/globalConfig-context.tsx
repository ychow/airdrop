import {createContext, useState, useContext, ReactNode} from 'react'
import {ConfigProps} from 'hooks/swr/usePerp'

const ConfigContext = createContext<
    | {
          configData?: ConfigProps
          updateConfigData: (newData) => void
      }
    | undefined
>(undefined)

export const GlobalConfigProvider = ({children}: {children: ReactNode}) => {
    const [configData, setConfigData] = useState<ConfigProps>({
        deposit_address: '',
        leverage: 0,
        default_swap: '',
        rpc_config: {
            chainId: '',
            rpcUrls: [],
            chainName: '',
            nativeCurrency: {
                name: '',
                decimals: 0,
                symbol: '',
            },
        },
    })

    const updateConfigData = (newData: ConfigProps) => {
        setConfigData(newData)
    }

    return <ConfigContext.Provider value={{configData, updateConfigData}}>{children}</ConfigContext.Provider>
}

export const useOvalConfig = () => {
    const context = useContext(ConfigContext)

    if (!context) {
        throw new Error('useIM必须在AuthProvider中使用')
    } else {
        return context
    }
}
