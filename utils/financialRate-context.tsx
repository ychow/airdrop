import {createContext, useMemo, useState, useContext, ReactNode, useEffect, useCallback} from 'react'
import {useRouter} from 'next/router'

import {FinanceObj} from 'hooks/swr/useIndexUtils'
import {useFinanceRate} from 'hooks/swr/useIndexUtils'

const FinancialRate = createContext<
    | {
          financialRate?: FinanceObj
          defaultRate: string
          financeList?: FinanceObj[]
          updateFinancialRate: (newRate) => void
      }
    | undefined
>(undefined)

export const FinancialRateProvider = ({children}: {children: ReactNode}) => {
    const router = useRouter()

    // declare financialRate and defaultRate
    const [financialRate, setFinancialRate] = useState<FinanceObj>({
        pair: '',
        rate: '',
        symbol: '',
        name: '',
    })
    const [defaultRate, setDefaultRate] = useState(
        typeof window !== 'undefined' && localStorage.saved_rate ? localStorage.saved_rate : ''
    )
    // store financeRate list
    const {data: financeRateData} = useFinanceRate()
    const financeList = useMemo(() => {
        const {data: resData} = financeRateData || {}

        if (resData && resData.length) {
            return resData
        }

        return []
    }, [financeRateData])

    // init financeRate while finance list updates
    useEffect(() => {
        const {data: resData} = financeRateData || {}
        if (resData && resData.length) {
            initFinanceRate(resData)
        }
    }, [financeRateData])

    // export a method to update financialRate
    const updateFinancialRate = useCallback((rateItem) => {
        localStorage.setItem('saved_rate', rateItem.pair)
        localStorage.setItem('FINANCE_RATE_ITEM', rateItem.pair)
        localStorage.setItem('FINANCE_RATE', rateItem.rate)
        setDefaultRate(rateItem.pair)
        setFinancialRate(rateItem)
    }, [])

    const initFinanceRate = useCallback(
        (list, lang = router.locale) => {
            if (!defaultRate || !list.some((x) => x.pair === defaultRate)) {
                if (lang === 'en') {
                    const _rate = list.find((x) => x.pair === 'USD_USDT')
                    if (_rate) {
                        localStorage.setItem('FINANCE_RATE_ITEM', _rate.pair)
                        localStorage.setItem('FINANCE_RATE', _rate.rate)
                        localStorage.setItem('FINANCE_RATE_SYMBOL', _rate.symbol)
                        setFinancialRate(_rate)
                    }
                } else {
                    const _rate = list.find((x) => x.pair === 'CNY_USDT')
                    if (_rate) {
                        localStorage.setItem('FINANCE_RATE_ITEM', _rate.pair)
                        localStorage.setItem('FINANCE_RATE', _rate.rate)
                        localStorage.setItem('FINANCE_RATE_SYMBOL', _rate.symbol)
                        setFinancialRate(_rate)
                    }
                }
            } else {
                const _rate = list.find((x) => x.pair === defaultRate)
                if (_rate) {
                    localStorage.setItem('FINANCE_RATE_ITEM', _rate.pair)
                    localStorage.setItem('FINANCE_RATE', _rate.rate)
                    localStorage.setItem('FINANCE_RATE_SYMBOL', _rate.symbol)
                    setFinancialRate(_rate)
                }
            }
        },
        [defaultRate]
    )

    return (
        <FinancialRate.Provider value={{financialRate, updateFinancialRate, defaultRate, financeList}}>
            {children}
        </FinancialRate.Provider>
    )
}

export const useRate = () => {
    const context = useContext(FinancialRate)

    if (!context) {
        throw new Error('useRate必须在AuthProvider中使用')
    } else {
        return context
    }
}
