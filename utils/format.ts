import moment from 'moment'
import Decimal from 'decimal.js'
import pako from 'pako'
// import BigNumber from 'bignumber.js'
import {getFullNum} from 'utils/index'

/* eslint-disable */
export function formatDecimal(num: number | string, decimal = 2): number | string {
    if (num === null || Number.isNaN(Number(num))) return '--'

    if (+num === 0) return '0'

    let prefix = ''
    num = num.toString()

    if (num[0] === '-') {
        prefix = '-'
        num = num.substr(1)
    }

    const index = num.indexOf('.')
    if (index !== -1) {
        num = num.substring(0, decimal + index + 1)
    } else {
        num = num.substring(0)
    }

    const result = parseFloat(num).toFixed(decimal)
    return prefix ? -1 * +result : result
}

export function changeToPercentWithToFixed(num: number, decimal = 2): string {
    if (Number.isNaN(Number(num))) {
        return `${num}`
    }

    let str = Number(num * 100).toFixed(decimal)
    str += '%'
    return str
}

export function toThousands(val: number | string): string {
    if (!val) return ''
    return val.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',')
}

export function formatDate(date, fmt) {
    // author: meizz
    if (typeof date === 'string') {
        date = date.replace(/-/g, '/')
    }
    date = new Date(date * 1000)
    const o = {
        'M+': date.getMonth() + 1, // 月份
        'd+': date.getDate(), // 日
        'h+': date.getHours(), // 小时
        'm+': date.getMinutes(), // 分
        's+': date.getSeconds(), // 秒
        'q+': Math.floor((date.getMonth() + 3) / 3), // 季度
        S: date.getMilliseconds(), // 毫秒
    }
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, `${date.getFullYear()}`.substr(4 - RegExp.$1.length))

    Object.keys(o).forEach((k) => {
        if (new RegExp(`(${k})`).test(fmt))
            fmt = fmt.replace(RegExp.$1, RegExp.$1.length === 1 ? o[k] : `00${o[k]}`.substr(`${o[k]}`.length))
    })
    return fmt
}

export function formatLeftTime(time: number | string, maxTime = null): string {
    if (time) {
        const _t = Number(moment(time).format('m'))
        if (_t > 0) {
            return `${_t + 1 > maxTime && maxTime ? maxTime : _t + 1}分钟`
        }
        // if (Number(moment(time).format('s')) > 0) {
        //   return `${moment(time).format('s')}秒`
        // }
        return '1分钟'
    }
    return ''
}

export function bankFormat(val: string): string {
    if (!val) return ''
    const str = `${val}`
    return str
        .replace(/[^\dA-Z]/g, '')
        .replace(/(.{4})/g, '$1 ')
        .trim()
}

export function noSpaceNumber(val) {
    if (!val) return ''
    const str = `${val}`
    return str.replace(/\s|\xA0/g, '')
}
/*
format a string|number to a string without scientific notation
*/
export function noScifiFormat(x: number | string): string {
    if (!x) return ''
    if (Math.abs(+x) < 1.0) {
        const e = parseInt(x.toString().split('e-')[1], 10)
        if (e) {
            const a = new Decimal(10).pow(new Decimal(e).minus(1).valueOf()).valueOf()
            x = new Decimal(x).mul(a).valueOf()
            x = `0.${new Array(e).join('0')}${x.toString().substring(2)}`
        }
    } else {
        let e = parseInt(x.toString().split('+')[1], 10)
        if (e > 20) {
            e -= 20
            x = +x / 10 ** e
            x = x.toString() + new Array(e + 1).join('0')
        }
    }
    return x.toString()
}

export function formatWsData(data: any) {
    return new Promise((resolve) => {
        if (data instanceof Blob) {
            try {
                const reader = new FileReader()
                reader.onload = () => {
                    const result = pako.ungzip(reader.result, {to: 'string'})
                    resolve(result)
                }
                reader.readAsArrayBuffer(data)
            } catch (error) {
                resolve('')
            }
        } else if (typeof data === 'string') {
            resolve(data)
        }
    })
}

export function toThousandsWithFloat(val) {
    if (!val) return ''
    const str = `${val}`
    if (str.indexOf('.') !== -1) {
        return `${str.split('.')[0].replace(/\d{1,3}(?=(\d{3})+(\.\d*)?$)/g, '$&,')}.${str.split('.')[1]}`
    }
    return str.replace(/\d{1,3}(?=(\d{3})+(\.\d*)?$)/g, '$&,')
}

export function getLimitNumber(value, precision) {
    const [intPart, floatPart] = value.split('.')
    if (!value) {
        return value
    }
    if (value.indexOf('.') === -1) {
        return value
    }
    if (intPart.length + floatPart.length > precision) {
        if (intPart.length === precision) {
            return intPart
        }
        const lastFloats = floatPart.slice(0, precision - 1 - intPart.length)
        return Number(`${intPart}.${lastFloats}`)
    }

    return Number(value) || '-'
}

export function toDecimalInt(num: number, decimal: number): number {
    // decimal is 2,3,4,5

    if (!decimal) {
        return num
    }

    const _baseNum = +`1${'0'.repeat(decimal)}` * 1

    return +parseInt(`${+num * _baseNum}`, 10)
}

export function accMul(arg1, arg2): any {
    let m = 0
    const s1 = arg1.toString(),
        s2 = arg2.toString()
    try {
        m += s1.split('.')[1].length
    } catch (e) {
        //
    }
    try {
        m += s2.split('.')[1].length
    } catch (e) {
        //
    }
    return (Number(s1.replace('.', '')) * Number(s2.replace('.', ''))) / Math.pow(10, m)
}

export function formatLocalTime(time) {
    // moment.tz.setDefault(moment.tz.guess())
    return moment(time).format('YYYY-MM-DD HH:mm:ss')
}

export function formatNumToSmall(num) {
    if (!num || num === 0) {
        return num
    }

    const _sNum = `${num}`
    const [a, b] = _sNum.split('.')

    if (b && b.length > 6) {
        const nB = b.slice(0, 6)
        return `${a}.${nB}...`
    } else {
        return num
    }
}

export function formatSeconds(second) {
    const date = (second / 60 / 60 / 24) >> 0
    const hour = (second / 60 / 60) % 24 >> 0
    const min = (second / 60) % 60 >> 0
    const sec = second % 60 >> 0
    return `${date === 0 ? '' : date + 'D'} ${hour === 0 ? '' : hour + 'H'} ${min === 0 ? '' : min + 'M'} ${
        sec === 0 ? '' : sec + 'S'
    }`
}

export function createSort(propertyName) {
    return function (object1, object2) {
        const value1 = object1[propertyName]

        const value2 = object2[propertyName]

        if (value1 > value2) {
            return 1
        } else if (value1 < value2) {
            return -1
        } else {
            return 0
        }
    }
}

export function formatNumToSmallLv(num, length) {
    num = getFullNum(num)
    if (!num || num === 0) {
        return num
    }
    const _sNum = `${num}`
    const [a, b] = _sNum.split('.')

    if (b && b.length > Number(length)) {
        const nB = b.slice(0, Number(length))
        return `${a}.${nB}...`
    } else {
        return num
    }
}
// 乘法
export function mul(a, b) {
    let c = 0,
        d = a.toString(),
        e = b.toString()
    try {
        c += d.split('.')[1].length
    } catch (f) {}
    try {
        c += e.split('.')[1].length
    } catch (f) {}
    return (Number(d.replace('.', '')) * Number(e.replace('.', ''))) / Math.pow(10, c)
}
// 除法
export function multi(a, b) {
    var c,
        d,
        e = 0,
        f = 0
    try {
        e = a.toString().split('.')[1].length
    } catch (g) {
        e = 0
    }
    try {
        f = b.toString().split('.')[1].length
    } catch (g) {
        f = 0
    }
    c = Number(a.toString().replace('.', ''))
    d = Number(b.toString().replace('.', ''))
    return mul(c / d, Math.pow(10, f - e))
}

export function formatNumToSmallLv2(num, length) {
    num = getFullNum(num)
    if (!num || num === 0) {
        return num
    }
    const _sNum = `${num}`
    const [a, b] = _sNum.split('.')

    if (b && b.length > Number(length)) {
        const nB = b.slice(0, Number(length))
        return `${a}.${nB}`
    } else {
        return num
    }
}
export function curtZero(item) {
    const regexp = /(?:\.0*|(\.\d+?)0+)$/
    return item.replace(regexp, '$1')
}
