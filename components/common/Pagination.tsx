import {useState, useEffect, memo, useCallback} from 'react'
import styled from 'styled-components'
import _isEqual from 'lodash/isEqual'

// import qs from 'querystring'
import {usePost} from 'hooks/useRequest'

interface PageObjProps {
    prev?: object
    next?: object
}
interface PaginationProps {
    page: {
        prev?: string
        next?: string
    } | null
    lastParams: object | null
    handleChange: (params) => void
    baseUrl: string
}

interface SingleNavProps {
    disabled?: boolean
}

export default memo(({page = {}, lastParams = null, handleChange, baseUrl}: PaginationProps) => {
    // prev: 下一页, next: 上一页 233

    // page: 查询结果得到的分页配置，内部有 next 和 prev 俩字段，分别表示上一页和下一页的请求 api
    // lastParams: 由调用页面传入的上一页请求的参数，用来判断是否可返回本次操作逆向操作的页面
    // handleCahgne: 点击翻页时的操作
    // baseUrl: 传入的当前页面数据 url,结合参数请求下一页数据

    // 当前页面查询结果得到的分页配置进行格式化为 params 后存储到 curPages 内
    const [storedPage, setStoredPage] = useState(null)
    const [curPages, setCurPages] = useState({})
    // 记录最后的点击操作是上一页还是下一页
    const [lastAction, setLastAction] = useState(null)
    // 判断是否可翻页，默认无法点击上一页，可点击下一页
    const [hasNext, setHasNext] = useState(false)
    const [hasPrev, setHasPrev] = useState(false)
    const [loadingNext, setLoadingNext] = useState(false)
    // 解析后端传回的 api，解析为 params
    const getParams = useCallback((api) => {
        const params = api.split('?')[1]
        return params
    }, [])

    // 检查是否可进入下一页，根据请求下一页数据的返回结果来判断
    const checkNext = useCallback(
        (params, type) => {
            setLoadingNext(true)
            usePost({
                url: `${baseUrl}?${params}`,
                method: 'get',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
            })
                .then((res) => {
                    if (res.status === 200 && res?.data.errno === 0 && res?.data?.data?.items?.length) {
                        if (type === 'next') {
                            setHasNext(true)
                        } else {
                            setHasPrev(true)
                        }
                    } else if (type === 'next') {
                        setHasNext(false)
                    } else {
                        setHasPrev(false)
                    }
                })
                .finally(() => {
                    setLoadingNext(false)
                })
        },
        [baseUrl]
    )

    useEffect(() => {
        if (!lastParams) {
            setLastAction(null)
        }
    }, [lastParams])

    // 监听传入的 page 变化，来更新分页状态
    useEffect(() => {
        if (!storedPage) {
            setStoredPage(page)
        } else {
            if (!_isEqual(page, storedPage)) {
                setStoredPage(page)
            } else {
                return
            }
        }
        const {prev, next} = page

        // 解析 page 内的参数，存入 curPages
        const pageObj: PageObjProps = {}

        let hasPrevFlag = false
        let hasNextFlag = false

        if (prev) {
            pageObj.prev = getParams(prev)
            hasPrevFlag = true
        } else {
            hasPrevFlag = false
        }

        if (next) {
            pageObj.next = getParams(next)
            // setHasNext(true)
            hasNextFlag = true
        } else {
            // setHasNext(false)
            hasNextFlag = false
        }

        // 边界情况，如果请求到了一页空数据（目前后端设置有这样的可能），此时既不会返回 next 也不会返回 prev，这样就需要根据 lastParams 来允许用户返回上一页  PS.（这种情况这一版已避免）
        // 第二种情况就是用户一些操作导致当前页由 1 条数据变成了 0 条，这时也需要相同操作
        if (lastAction === 'next' && !prev) {
            pageObj.prev = lastParams
            hasPrevFlag = true
        }
        if (lastAction === 'prev' && !next) {
            pageObj.next = lastParams
            hasNextFlag = true
        }

        setCurPages({...pageObj})

        // 没有 lastParams 表示当前为逻辑上的第一页，如果此时 prev 有值，则请求下一页数据判断是否可点击下一页
        if (!lastParams) {
            if (prev) {
                checkNext(getParams(prev), 'prev')
            } else {
                hasNextFlag = false
            }
        }
        // 有 lastParams 表示已经不是第一页，那么就根据上一次点击的动作来判断是否可点击下一页，当然判断的前提是有返回对应下一页的请求参数
        // 上一次点击的是下一页，并且新的返回里还有下一页参数
        if (lastParams && lastAction === 'prev' && prev) {
            checkNext(getParams(prev), 'prev')
            hasNextFlag = true
        }
        // 上一次点击的是上一页，并且新的返回里还有上一页参数
        if (lastParams && lastAction === 'next' && next) {
            checkNext(getParams(next), 'next')
            hasPrevFlag = true
        }

        setHasPrev(hasPrevFlag)
        setHasNext(hasNextFlag)
    }, [page, lastParams])

    // 点击翻页
    const handleClick = useCallback(
        (type, disabled) => {
            if (disabled) {
                return
            }
            // 如果有对应的请求参数，则请求接口，并记录动作
            if (curPages[type]) {
                handleChange(curPages[type])
                setLastAction(type)
                setHasNext(false)
                setHasPrev(false)
                // if (lastAction) {
                //   setLastParams(curPages[type])
                // }
            }
        },
        [curPages, lastAction]
    )

    // useEffect(()=>{
    //   console.log(lastAction, lastParams, 'check all data')
    // }, [lastAction, lastParams])

    if (!page?.next && !page?.prev) {
        return null
    }

    return (
        <Wrapper>
            {/* <span style={{color: 'red'}}>{JSON.stringify({loadingNext, hasPrev, hasNext, lastParams})}</span> */}
            {((hasNext && lastParams) || hasPrev) && (
                <>
                    <SingleNav
                        disabled={!hasNext || !lastParams || loadingNext}
                        onClick={() => handleClick('next', !hasNext || !lastParams || loadingNext)}
                    >
                        <div className="normal-svg">
                            <svg
                                width="9"
                                height="14"
                                viewBox="0 0 9 14"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path d="M8 1L2 7L8 13" stroke="#A7ACB8" strokeWidth="2" />
                            </svg>
                        </div>
                        <div className="hovered-svg">
                            <svg
                                width="9"
                                height="14"
                                viewBox="0 0 9 14"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path d="M8 1L2 7L8 13" stroke="white" strokeWidth="2" />
                            </svg>
                        </div>
                    </SingleNav>

                    <SingleNav
                        disabled={!hasPrev || loadingNext}
                        onClick={() => handleClick('prev', !hasPrev || loadingNext)}
                    >
                        <div className="normal-svg">
                            <svg
                                width="9"
                                height="14"
                                viewBox="0 0 9 14"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path d="M1 1L7 7L1 13" stroke="#A7ACB8" strokeWidth="2" />
                            </svg>
                        </div>
                        <div className="hovered-svg">
                            <svg
                                width="9"
                                height="14"
                                viewBox="0 0 9 14"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path d="M1 1L7 7L1 13" stroke="white" strokeWidth="2" />
                            </svg>
                        </div>
                    </SingleNav>
                </>
            )}
        </Wrapper>
    )
})

export const Wrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: flex-end;
    /* background: #242433; */
    /* margin-top: 10px; */
`

export const SingleNav = styled.div<SingleNavProps>`
    width: 28px;
    height: 28px;
    border-radius: 2px;
    border: 1px solid rgba(98, 105, 119, 0.2);
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: ${(props) => (props.disabled ? 'not-allowed' : 'pointer')};

    &:last-child {
        margin-left: 12px;
    }

    .normal-svg {
        display: flex;
        align-items: center;
        justify-content: center;
    }
    .hovered-svg {
        display: none;
    }

    &:hover {
        .normal-svg {
            display: ${(props) => (props.disabled ? 'flex' : 'none')};
        }
        .hovered-svg {
            display: ${(props) => (props.disabled ? 'none' : 'flex')};
        }
        background: ${(props) => (props.disabled ? 'none' : '#1665E3')};
        border-color: ${(props) => (props.disabled ? 'rgba(98, 105, 119, 0.2)' : '#1665E3')};
    }
`
