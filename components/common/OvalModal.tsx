import {memo} from 'react'
import styled from 'styled-components'
import Modal, {ModalProps} from './ClassicModal'
import Button from './Button'

interface ConfirmButtonProps {
    full?: boolean
}

export default memo(({visible, onCancel, className, loading, children}: ModalProps) => {
    return (
        <OvalModalWrap
            visible={visible}
            width={600}
            onCancel={onCancel}
            className={className}
            loading={loading}
            style={{content: {backgroundColor: '#2e2e40', borderRadius: '36px'}}}
        >
            <CloseBtn onClick={onCancel}>
                <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M23.0452 8.99999L9.02441 23.0208" stroke="#848D98" strokeWidth="2.5" />
                    <path d="M9.00017 8.99999L23.021 23.0208" stroke="#848D98" strokeWidth="2.5" />
                </svg>
            </CloseBtn>
            {children}
        </OvalModalWrap>
    )
})

export const OvalModalWrap = styled(Modal)`
    /* height: ${(props) => (props.loading ? '300px' : 'auto')} !important; */
    height: 300px;
    padding: 32px;

    &:focus-visible {
        outline: none;
    }
`

export const HeaderLine = styled.div`
    font-family: 'LaoMuangKhong';
    font-style: normal;
    font-weight: bold;
    font-size: 20px;
    line-height: 150%;
    margin-bottom: 24px;
    color: #fff;
    text-align: left;
`

export const ContentLine = styled.div`
    position: relative;
    padding: 20px 0;
    font-family: 'LaoMuangKhong';
    font-style: normal;
    font-weight: bold;
    font-size: 20px;
    line-height: 150%;
    color: #fff;
    text-align: left;

    .selectList {
        position: absolute;
        width: 100%;
        height: 140px;
        top: 78px;
        left: 0;
        background: #fff;
        z-index: 5;
        border-radius: 2px;

        .item {
            width: 100%;
            height: 45px;
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 5px 12px;
            font-size: 14px;
            font-weight: 700;
            color: rgba(0,0,0,.85);
            cursor: pointer;

            &:hover {
                background: #f5f5f5;
            }

            div {
                display: flex;
                align-items: center;
            }

            span.title {
                margin-left: 10px;
            }
        }

        
    }

    p.link {
        cursor: pointer;
        color: #76ea79;
        text-decoration: underline;
    }
`

export const IntroLine = styled.div`
    font-family: 'LaoMuangKhong';
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 150%;
    color: #676f81;
    margin-top: 24px;
    text-align: left;
`

export const BtnGroup = styled.div`
    height: 58px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: 24px;
`

export const LeftButton = styled.div`
    background: rgba(103, 111, 129, 0.5);
    box-shadow: 0px 12px 24px rgba(74, 78, 96, 0.1);
    border-radius: 16px;
    width: 163px;
    height: 58px;
    font-family: 'LaoMuangKhong';
    font-style: normal;
    font-weight: bold;
    font-size: 20px;
    line-height: 150%;
    display: flex;
    align-items: center;
    justify-content: center;
    color: #a7acb8;
    cursor: pointer;

    &:hover {
        background: rgba(103, 111, 129, 0.2);
        box-shadow: 0px 12px 24px rgba(74, 78, 96, 0.1);
    }
`

export const ConfirmButton = styled(Button)<ConfirmButtonProps>`
    width: ${(props) => (props.full ? '100%' : '163px')};
    height: 58px;
    border-radius: 16px;
    background: ${(props) =>
        props.disabled
            ? 'rgba(103, 111, 129, 0.5) !important'
            : 'linear-gradient(103.01deg, #69e77f 0%, #d0fb48 100%)'};
    box-shadow: ${(props) => (props.disabled ? 'none !important' : '0px 12px 24px rgba(138, 238, 110, 0.1)')};
    border: none;
    display: flex;
    align-items: center;
    justify-content: center;
    font-family: 'LaoMuangKhong';
    font-style: normal;
    font-weight: bold;
    font-size: 20px;
    color: #101215;
    cursor: ${props => props.loading ? 'not-allowed' : 'pointer'};
    position: relative;
    overflow: hidden;

    &:hover {
        background: linear-gradient(103.01deg, #4eda67 0%, #b6e12f 100%);
        box-shadow: 0px 12px 24px rgba(138, 238, 110, 0.1);
    }
`

const CloseBtn = styled.div`
    width: 32px;
    height: 32px;
    cursor: pointer;
    position: absolute;
    right: 32px;
    top: 32px;
`

export const CancelContentBlock = styled.div`
    width: 100%;
    height: 80px;
    padding-left: 20px;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    background: #333447;
    border-radius: 12px;

    h3.title {
        margin-right: 12px;
        color: #fff;
        font-family: 'LaoMuangKhong';
        font-style: normal;
        font-weight: 500;
        font-size: 20px;
    }

    span {
        padding: 4px 6px;
        background: rgba(14, 167, 134, 0.1);
        color: #0ea786;
        border-radius: 4px;
        font-family: 'LaoMuangKhong';

        &.short {
            background: rgba(241, 85, 99, 0.1);
            color: #e94747;
        }

        &:first-of-type {
            margin-right: 8px;
        }
    }
`
