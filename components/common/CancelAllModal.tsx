import {memo, useState, useCallback} from 'react'
import styled from 'styled-components'
import {useTranslation} from 'next-i18next'

import Modal from 'components/common/Modal'

import {useRouter} from 'next/router'
import {usePost} from 'hooks/useRequest'
import useAlert from 'hooks/useAlert'

interface ModalProps {
    code?: string
    type?: string
    close?: () => void
    loading?: boolean
    visible?: boolean
}

export default memo(({code, type, close}: ModalProps) => {
    const {t} = useTranslation('spot')
    const [setAlert] = useAlert()
    const router = useRouter()

    const [state, setState] = useState(0)

    const handleCancelOrders = useCallback(() => {
        if (code && type) {
            setState(1)

            usePost({
                url: '/x/api/v1/order/symbol_batch_cancel',
                method: 'post',
                data: {
                    market: type,
                    symbol: code,
                    direction: 0,
                },
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'FT-language': router.locale === 'en' ? 'en-US' : router.locale,
                },
            })
                .then((res) => {
                    if (res && res.status === 200) {
                        if (res.data.errno === 0) {
                            // success
                            setAlert({
                                type: 'success',
                                message: t('tradeListCancelRequested'),
                            })
                            close && close()
                        } else if (res.data.errmsg) {
                            setAlert({
                                type: 'error',
                                message: t('common:apiError'),
                            })
                        }
                    } else {
                        // call api fail
                        setAlert({
                            type: 'error',
                            message: t('common:apiError'),
                        })
                    }
                })
                .catch(() => {
                    setAlert({
                        type: 'error',
                        message: t('common:apiError'),
                    })
                })
        }
    }, [code, type])

    return (
        <Modal
            isOpen={true}
            onRequestClose={close}
            // footer={null}
            overlayDismiss={true}
            // closable={false}
            style={{
                content: {
                    width: '382px',
                    borderRadius: '2px',
                },
            }}
        >
            <ContentWrap>
                {state === 0 && (
                    <>
                        <p className="title">{t('confirmCancelAll')}</p>
                        <div className="buttonWrap">
                            <button type="button" className="button sureCancel" onClick={close}>
                                {t('palCancelText')}
                            </button>
                            <button className="button submit" onClick={handleCancelOrders}>
                                {t('submitButton')}
                            </button>
                        </div>
                    </>
                )}
                {state === 1 && (
                    <>
                        <p className="title">{t('tradeStateCanceling')}...</p>
                        <div className="buttonWrap" style={{justifyContent: 'center'}}>
                            <button type="button" className="button cancel" onClick={close}>
                                {t('turnOff')}
                            </button>
                        </div>
                    </>
                )}
            </ContentWrap>
        </Modal>
    )
})

const ContentWrap = styled.div`
    height: 124px;
    padding: 46px 24px 0;
    box-sizing: content-box;

    p.title {
        height: 22px;
        font-weight: 500;
        font-size: 16px;
        line-height: 22px;
        text-align: center;
        color: #101215;
    }

    div.buttonWrap {
        width: 284px;
        height: 44px;
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin: 32px auto 0;

        .button {
            width: 130px;
            height: 44px;
            display: flex;
            justify-content: center;
            align-items: center;
            line-height: 44px;
            text-align: center;
            font-weight: 500;
            font-size: 16px;
            cursor: pointer;
            border-radius: 2px;
            border: 0;

            &.sureCancel {
                background: rgba(103, 111, 129, 0.1);
                color: #1665e3;
            }

            &.submit {
                background: #1665e3;
                color: #fff;
            }

            &.cancel {
                background: #e94747;
                color: #fff;
            }
        }
    }
`
