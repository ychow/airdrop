import {memo} from 'react'
import styled from 'styled-components'
interface FocusModalProps {
    contenText?: string
    contenText2?: string
    contenText3?: string
    contenText4?: string
    isMore?: boolean
    className?: string
}

export default memo(({contenText, contenText2, contenText3, contenText4, isMore, className}: FocusModalProps) => {
    return (
        <FocusModal className={`${className}`}>
            <div>
                <p>
                    <p>{contenText}</p>
                    <p>{contenText2}</p>
                    <p>{contenText3}</p>
                    <p>
                        {contenText4}
                        {isMore && (
                            <a
                                target="_blank"
                                rel="noopener noreferrer"
                                href="https://exchange-oval.gitbook.io/oval.exchange/"
                            >
                                view more
                            </a>
                        )}
                    </p>
                </p>
            </div>
        </FocusModal>
    )
})

const FocusModal = styled.div`
    position: absolute;
    top: -70px;
    max-width: 452px;
    div {
        display: flex;
        height: auto;
        padding: 20px;
        min-height: auto;
        background: #3c3d54;
        border-radius: 8px;
        font-family: DINPro;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 150%;
        display: flex;
        align-items: center;
        text-align: left;
        color: #ffffff;
    }
    div:after {
        content: '';
        border-top: solid 8px #3c3d54;
        border-right: solid 8px transparent;
        border-left: solid 8px transparent;
        bottom: -8px;
        margin-right: -4px;
        right: 50%;
        position: absolute;
        width: 0;
        height: 0;
    }
    a {
        text-decoration: none;
        font-family: DINPro;
        font-style: normal;
        font-weight: bold;
        font-size: 16px;
        line-height: 150%;
        color: #76ea79;
        margin-left: 10px;
        cursor: pointer;
    }
`
