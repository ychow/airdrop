import {useEffect} from 'react'

import styled from 'styled-components'

interface StyleProps {
    global?: string
    hasBac?: boolean
    theme?: string
    loadingBac?: string
}

// const BACK_COLOR = {
//     light: 'rgba(255, 255, 255, 0.6)',
//     dark: '#181a1e',
// }

const LoadingWrap = styled.div<StyleProps>`
    position: ${(props) => (props.global ? 'fixed' : 'absolute')};
    left: 0;
    top: 0;
    width: ${(props) => (props.global ? '100vw' : '100%')};
    height: ${(props) => (props.global ? '100vh' : '100%')};
    display: flex;
    align-items: center;
    justify-content: center;
    z-index: 100;
    overflow: hidden;
    background: ${(props) => (props.loadingBac ? props.loadingBac : 'none')};

    img {
        width: 32px;
        height: 32px;
    }
`

const Loading = ({theme, hasBac, global, loadingBac}: StyleProps) => {
    useEffect(() => {
        document.body.style.overflow = 'hidden'

        return () => {
            document.body.style.overflow = 'auto'
        }
    }, [])

    return (
        <LoadingWrap loadingBac={loadingBac} theme={theme} hasBac={hasBac} global={global}>
            <ImgWrap>
                <img
                    src={require('../../public/static/img/oval_loading.gif')}
                    width="68"
                    height="68"
                    alt="loading..."
                />
            </ImgWrap>
        </LoadingWrap>
    )
}

export default Loading

const ImgWrap = styled.div`
    width: 102px;
    height: 62px;
    border-radius: 12px;
    background-color: #2e2e40;
    display: flex;
    align-items: center;
    justify-content: center;
`
