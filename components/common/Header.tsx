import {memo} from 'react'
import styled from 'styled-components'
import WalletLogin from 'web3/components/WalletLogin'

interface Props {
    isIndex?: boolean
    chain?: string
}

const Header = memo(({isIndex, chain}: Props) => {

    return (
        <HeaderWrap isIndex={isIndex}>
             <h1>空投工具</h1>
                <HeaderOptWrap>

                    <WalletLogin chain={chain} />
                </HeaderOptWrap>
            
        </HeaderWrap>
    )
})

export default Header

const HeaderWrap = styled.div<Props>`
    background: #0c0e11;
    box-shadow: inset 0px -1px 1px #242433;
    height: 64px;
    /* min-width: 1600px; */
    width: 100%;
    padding: 0 20px;
    display: flex;

    align-items: center;
    margin-bottom: ${(props) => (props.isIndex ? '0' : '1px')};

    h1 {
        font-size: 30px;
    color: #fff;
    }
`

const HeaderOptWrap = styled.div`
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: flex-end;
`

