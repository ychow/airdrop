import styled from 'styled-components'

interface StyleProps {
    global?: string
    hasBac?: boolean
    theme?: string
    loadingBac?: string
}

const LoadingWrap = styled.div<StyleProps>`
    position: ${(props) => (props.global ? 'fixed' : 'absolute')};
    left: 0;
    top: 0;
    width: ${(props) => (props.global ? '100vw' : '100%')};
    height: ${(props) => (props.global ? '100vh' : '100%')};
    display: flex;
    align-items: center;
    justify-content: center;
    z-index: 100;
    overflow: hidden;
    background: linear-gradient(103.01deg, #69e77f 0%, #d0fb48 100%);

    img {
        width: 32px;
        height: 32px;
    }
`

const Loading = () => {
    // useEffect(() => {
    //     document.body.style.overflow = 'hidden'

    //     return () => {
    //         document.body.style.overflow = 'auto'
    //     }
    // }, [])

    return (
        <LoadingWrap>
            <img src={require('../../public/static/img/loading.gif')} width="68" height="68" alt="loading..." />
        </LoadingWrap>
    )
}

export default Loading
