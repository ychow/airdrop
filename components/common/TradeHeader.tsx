import {memo, useState} from 'react'
import Link from 'next/link'
import styled from 'styled-components'

import OvalModal, {HeaderLine, ContentLine, BtnGroup, ConfirmButton} from 'components/common/OvalModal'
import {ModalProps} from 'components/common/ClassicModal'
import WalletLogin from 'web3/components/WalletLogin'

interface Props {
    isIndex?: boolean
    isActive?: boolean
}

const SWAP_SYMBOL_LIST = [
    {
        name: 'BTCUSDT',
        code: 'BTCUSDT_SWAP',
        normalImg: <img src={require('../../public/static/img/icons/btc_off.png')} width="24" height="24" />,
        activeImg: <img src={require('../../public/static/img/icons/btc_on.png')} width="24" height="24" />,
    },
    {
        name: 'ETHUSDT',
        code: 'ETHUSDT_SWAP',
        normalImg: <img src={require('../../public/static/img/icons/eth_off.png')} width="24" height="24" />,
        activeImg: <img src={require('../../public/static/img/icons/eth_on.png')} width="24" height="24" />,
    },
    {
        name: 'LTCUSDT',
        code: 'LTCUSDT_SWAP',
        normalImg: <img src={require('../../public/static/img/icons/ltc_off.png')} width="24" height="24" />,
        activeImg: <img src={require('../../public/static/img/icons/ltc_on.png')} width="24" height="24" />,
    },
    {
        name: 'BNBUSDT',
        code: 'BNBUSDT_SWAP',
        normalImg: <img src={require('../../public/static/img/icons/bnb_off.png')} width="24" height="24" />,
        activeImg: <img src={require('../../public/static/img/icons/bnb_on.png')} width="24" height="24" />,
    },
    {
        name: 'SOLUSDT',
        code: 'SOLUSDT_SWAP',
        normalImg: <img src={require('../../public/static/img/icons/sol_off.png')} width="24" height="24" />,
        activeImg: <img src={require('../../public/static/img/icons/sol_on.png')} width="24" height="24" />,
    },
]

const RuleModal = memo(({visible, onCancel}: ModalProps) => {
    return (
        <OvalModal visible={visible} onCancel={onCancel}>
            <HeaderLine>Rule</HeaderLine>

            <ContentLine>
                1. Trading Fee: Taker 0.07%，Maker 0.03% <br />
                2. Funding payments occur every 1 hour
            </ContentLine>

            <BtnGroup>
                <ConfirmButton onClick={onCancel} full={true}>
                    Confirm
                </ConfirmButton>
            </BtnGroup>
        </OvalModal>
    )
})

const Header = memo(({symbolName}: any) => {
    const [showRule, setShowRule] = useState(false)
    return (
        <HeaderWrap>
            <a rel="noopener noreferrer" href={'/'}>
                <img
                    src={require('../../public/static/img/header_logo.png')}
                    alt="logo of Oval"
                    width={110}
                    height={32}
                />
            </a>

            <div className="line">
                <svg width="1" height="28" viewBox="0 0 1 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <line x1="0.5" x2="0.5" y2="28" stroke="#676F81" strokeOpacity="0.2" />
                </svg>
            </div>

            <NavList>
                {SWAP_SYMBOL_LIST.map((item) => (
                    <NavItem key={item.name} isActive={symbolName === item.code}>
                        <Link href={`/trade/${item.code}`}>
                            <a className={`${symbolName === item.code ? 'active' : ''}`} rel="noopener noreferrer">
                                {symbolName === item.code ? item.activeImg : item.normalImg}
                                {item.name}
                            </a>
                        </Link>
                    </NavItem>
                ))}
            </NavList>

            <HeaderOptWrap>
                <RuleWrap onClick={() => setShowRule(true)}>
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="12" cy="12" r="7.35" stroke="#676F81" strokeWidth="1.3" />
                        <path
                            d="M10 9.78571C10 9.19048 10.4 8 12 8C13.6 8 14 9.19048 14 9.78571C14 10.0833 14 10.5 13.3333 10.9762C12.8333 11.3333 12 12.5238 12 13"
                            stroke="#676F81"
                            strokeWidth="1.3"
                        />
                        <path d="M12 14.5V16" stroke="#676F81" strokeWidth="1.3" />
                    </svg>

                    <span>Rule</span>
                </RuleWrap>
                <WalletLogin />
            </HeaderOptWrap>

            {showRule && <RuleModal visible={showRule} onCancel={() => setShowRule(false)} />}
        </HeaderWrap>
    )
})

export default Header

const HeaderWrap = styled.div`
    display: flex;
    background: #242433;
    box-shadow: inset 0px -1px 1px #242433;
    height: 64px;
    width: 100%;
    padding: 0 16px;
    align-items: center;
    justify-content: flex-start;
    margin-bottom: 1px;

    .line {
        margin: 0 34px 0 32px;
    }

    .inner {
        /* width: 1600px; */
        width: 100%;
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: space-between;
        margin: 0 auto;

        a {
            display: flex;
        }

        &.indexPage {
            justify-content: flex-start;
        }
    }
`

const HeaderOptWrap = styled.div`
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: flex-end;
`

const RuleWrap = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    margin-right: 32px;
    cursor: pointer;

    span {
        font-family: 'LaoMuangKhong';
        font-style: normal;
        font-weight: bold;
        font-size: 14px;
        color: #676f81;
    }
`

const NavList = styled.ul`
    display: flex;
    height: 100%;
`

const NavItem = styled.li<Props>`
    width: 150px;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    background: ${(props) => (props.isActive ? '#1F1F2C' : 'transparent')};

    &:hover {
        background: rgba(103, 111, 129, 0.1);
    }

    a {
        width: 100%;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        cursor: pointer;
        font-family: 'LaoMuangKhong';
        font-weight: 500;
        font-size: 14px;
        color: #676f81;
        text-decoration: none;

        img {
            margin-right: 10px;
        }

        &.active {
            color: #fff;
        }

        &:hover {
            /* background: rgba(103, 111, 129, 0.1); */
        }
    }
`
