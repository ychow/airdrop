import React, {useState, useEffect, useCallback, useRef, memo} from 'react'
import NP from 'number-precision'
import styled from 'styled-components'
import {useSWRInfinite} from 'swr'
import {useRouter} from 'next/router'

import Loading from 'components/common/Loading'
import KlineTechnicalModal from './KlineTechnicalModal'
import DepthChart from './DepthChart'
import KlineTimeTool from './KlineTimeTool'

import useWsState from 'hooks/useWsState'
import {formatDecimal, changeToPercentCeil} from 'utils/index'
import {SymbolObj} from 'hooks/swr/useSpot'
import {KlineProps} from 'hooks/useWebsocket'

interface StyleProps {
    isActive?: boolean
    isFullScreen?: boolean
}

interface KlinePageProps {
    symbolData: SymbolObj
    sideMenuIsOpen?: boolean
    klineWsData: KlineProps | null
    wsOrderBookData?: any
    setKlineType: any
    changeKey?: any
    syncFullScreen?: any
}

let kLineChart
let volId = ''
let kdjId = ''
let rsiId = ''
let macdId = ''

export default memo(
    ({
        symbolData,
        syncFullScreen,
        sideMenuIsOpen,
        klineWsData,
        setKlineType,
        wsOrderBookData,
        changeKey,
    }: KlinePageProps) => {
        const router = useRouter()
        const [wsData] = useWsState()

        const [timeChoose, setTimeChoose] = useState({
            class: 'selected',
            label: `15m`,
            resolution: '15',
            historyType: 5,
            wsType: 'kline-15min',
            wsAddTime: 900000,
            chartType: 1,
        })
        const [isShowTechModal, setShowTechModal] = useState(false)
        const [historyParams, setHistoryParams] = useState(null)
        const [isAddMore, setAddMore] = useState(false)
        const [isFullScreen, setFullScreen] = useState(false)
        const [tech, setTech] = useState(
            typeof window !== 'undefined' && localStorage.tech
                ? JSON.parse(localStorage.tech)
                : {
                      main: 'MA',
                      sub: 'VOL',
                  }
        )
        const [isLoad, setLoad] = useState(true)
        const [chartType, setChartType] = useState('kline')

        const {data: historyKlineData} = useSWRInfinite(
            historyParams && symbolData?.code && typeof symbolData?.code === 'string'
                ? () =>
                      `/x/pub/api/v1/hq/kline?source=dex&type=${timeChoose.historyType}&symbol=${symbolData?.code}&num=500&ts=${historyParams}`
                : null,
            async function (request) {
                const res = await fetch(request)
                return res.json()
            }
        )

        const timeChooseRef = useRef({
            class: 'selected',
            label: `15m`,
            resolution: '15',
            historyType: 5,
            wsType: 'kline-15min',
            wsAddTime: 900000,
            chartType: 1,
        })
        const historyDataRef = useRef(null)

        useEffect(() => {
            if (!(historyKlineData?.length && historyKlineData[0]?.data?.length)) {
                setLoad(false)
                return
            }
            if (historyKlineData && symbolData) {
                const historyKlineData2 = historyKlineData[0]
                if (historyKlineData2.errno !== 0 || !historyKlineData2.data || !historyKlineData2.data.length) {
                    if (historyDataRef.current && historyDataRef.current.length === 1) {
                        const _old = historyDataRef.current[0]
                        if (_old.trade === symbolData.code) {
                            setLoad(false)
                            return null
                        }
                    } else {
                        historyDataRef.current = []
                        kLineChart.applyNewData([], true)
                        setLoad(false)
                    }
                }

                if (historyKlineData2.data && historyKlineData2.data.length) {
                    historyKlineData2.data.reverse()
                    const {multiple} = symbolData

                    const bars =
                        historyKlineData2.data.map((el) => {
                            return {
                                trade: el.symbol,
                                timestamp: el.time * 1000, // TradingView requires bar time in ms
                                low: el.low,
                                high: el.high,
                                open: el.open,
                                close: el.close,
                                volume: formatDecimal(
                                    el.volume * multiple,
                                    multiple.toString().indexOf('.') > -1 && multiple.toString().split('.')[1].length
                                ),
                                // volume: el.volumefrom
                            }
                        }) || []

                    historyDataRef.current = bars

                    // setHistoryData(bars)
                    // kLineChart.applyMoreData(bars, true)
                    isAddMore ? kLineChart.applyMoreData(bars, true) : kLineChart.applyNewData(bars, true)
                    handleSwitchTech()
                    setHistoryParams(null)
                    setAddMore(false)
                    setLoad(false)
                }
            }
        }, [historyKlineData, isAddMore, kLineChart, symbolData])

        useEffect(() => {
            if (klineWsData && kLineChart && symbolData) {
                const _timeChoose = timeChooseRef.current
                const _old = historyDataRef.current || []

                const lastData = (_old && _old[_old.length - 1]) || ''
                if (lastData) {
                    const {timestamp} = lastData

                    let realTime = Math.floor(klineWsData.time / 1000)

                    if (_timeChoose.resolution === '1D') {
                        const toDayTimeStamp = (Number(new Date(new Date().setHours(0, 0, 0, 0))) / 1000) * 1000
                        realTime = toDayTimeStamp
                    }

                    if (realTime === timestamp) {
                        _old[_old.length - 1] = {
                            timestamp: realTime, // TradingView requires bar time in ms
                            low: klineWsData.low,
                            high: klineWsData.high,
                            open: klineWsData.open,
                            close: klineWsData.close,
                            volume: formatDecimal(
                                klineWsData.volume * symbolData.multiple * 0.01,
                                symbolData.multiple.toString().indexOf('.') > -1 &&
                                    symbolData.multiple.toString().split('.')[1].length
                            ),
                        }

                        historyDataRef.current = _old
                    } else {
                        historyDataRef.current = [
                            ..._old,
                            {
                                timestamp: realTime, // TradingView requires bar time in ms
                                low: klineWsData.low,
                                high: klineWsData.high,
                                open: klineWsData.open,
                                close: klineWsData.close,
                                volume: formatDecimal(
                                    klineWsData.volume * symbolData.multiple * 0.01,
                                    symbolData.multiple.toString().indexOf('.') > -1 &&
                                        symbolData.multiple.toString().split('.')[1].length
                                ),
                            },
                        ]
                    }

                    kLineChart.updateData({
                        timestamp: realTime, // TradingView requires bar time in ms
                        low: klineWsData.low,
                        high: klineWsData.high,
                        open: klineWsData.open,
                        close: klineWsData.close,
                        volume: formatDecimal(
                            klineWsData.volume * symbolData.multiple * 0.01,
                            symbolData.multiple.toString().indexOf('.') > -1 &&
                                symbolData.multiple.toString().split('.')[1].length
                        ),
                    })
                } else {
                    let realTime = Math.floor(klineWsData.time / 1000)

                    if (_timeChoose.resolution === '1D') {
                        const toDayTimeStamp = (Number(new Date(new Date().setHours(0, 0, 0, 0))) / 1000) * 1000
                        realTime = toDayTimeStamp
                    }

                    historyDataRef.current = [
                        ..._old,
                        {
                            trade: klineWsData.symbol,
                            timestamp: realTime, // TradingView requires bar time in ms
                            low: klineWsData.low,
                            high: klineWsData.high,
                            open: klineWsData.open,
                            close: klineWsData.close,
                            volume: formatDecimal(
                                klineWsData.volume * symbolData.multiple * 0.01,
                                symbolData.multiple.toString().indexOf('.') > -1 &&
                                    symbolData.multiple.toString().split('.')[1].length
                            ),
                        },
                    ]

                    kLineChart.updateData({
                        timestamp: realTime, // TradingView requires bar time in ms
                        low: klineWsData.low,
                        high: klineWsData.high,
                        open: klineWsData.open,
                        close: klineWsData.close,
                        volume: formatDecimal(
                            klineWsData.volume * symbolData.multiple * 0.01,
                            symbolData.multiple.toString().indexOf('.') > -1 &&
                                symbolData.multiple.toString().split('.')[1].length
                        ),
                    })
                }
            }
        }, [klineWsData, kLineChart, symbolData, wsData])

        useEffect(() => {
            setLoad(true)
        }, [timeChoose])

        const getHistoryData = useCallback(
            (ts, isMore) => {
                setLoad(true)
                setHistoryParams(Math.floor(ts / 1000))
                setAddMore(isMore)
            },
            [symbolData]
        )

        const handleRemoveTech = useCallback(() => {
            if (kLineChart) {
                kLineChart.removeTechnicalIndicator('candle_pane')
                kdjId && kLineChart.removeTechnicalIndicator(kdjId, 'KDJ')
                volId && kLineChart.removeTechnicalIndicator(volId, 'VOL')
                rsiId && kLineChart.removeTechnicalIndicator(rsiId, 'RSI')
                macdId && kLineChart.removeTechnicalIndicator(macdId, 'MACD')
            }
        }, [kLineChart, symbolData])

        const handleSwitchTech = useCallback(() => {
            const {main, sub} = tech
            // if (kLineChart && main && sub) {
            handleRemoveTech()
            if (timeChoose.chartType !== 3) {
                switch (main) {
                    case 'MA':
                        kLineChart.removeTechnicalIndicator('candle_pane')
                        kLineChart.createTechnicalIndicator('MA', false, {
                            id: 'candle_pane',
                        })
                        break
                    case 'BOLL':
                        kLineChart.removeTechnicalIndicator('candle_pane')
                        kLineChart.createTechnicalIndicator('BOLL', false, {
                            id: 'candle_pane',
                        })
                        break
                }

                switch (sub) {
                    case 'KDJ':
                        kdjId = kLineChart.createTechnicalIndicator('KDJ', false, {
                            height: 60,
                        })
                        break
                    case 'VOL':
                        volId = kLineChart.createTechnicalIndicator('VOL', false, {
                            height: 60,
                        })
                        break
                    case 'RSI':
                        rsiId = kLineChart.createTechnicalIndicator('RSI', false, {
                            height: 60,
                        })
                        break
                    case 'MACD':
                        macdId = kLineChart.createTechnicalIndicator('MACD', false)
                        break
                }
            }
            // }

            // return ''
        }, [tech, timeChoose, kLineChart])

        const hideTechModal = useCallback(() => {
            setShowTechModal(false)
        }, [])

        useEffect(() => {
            setTimeout(() => {
                kLineChart?.resize()
            }, 500)
        }, [sideMenuIsOpen, changeKey, kLineChart])

        const escFunction = (event) => {
            if (event.keyCode === 27) {
                setFullScreen(false)
                syncFullScreen(false)
            }
        }

        useEffect(() => {
            let isDepth = false
            setChartType((prev) => {
                if (prev === 'depth') {
                    isDepth = true
                    return ''
                }
                return prev
            })

            isDepth &&
                setTimeout(() => {
                    setChartType('depth')
                }, 500)
        }, [sideMenuIsOpen, symbolData])

        useEffect(() => {
            const {init} = require('klinecharts')

            // 初始化图表
            kLineChart = init('klineWrapper')

            kLineChart.addCustomTechnicalIndicator({
                name: 'VOL',
                // 指定y轴上的最小值
                minValue: 0,
                // 设置两个技术参数，分别用来计算ma5和ma10
                calcParams: ['V', 5, 10],
                precision: 2,
                plots: [
                    // 成交量矩形柱
                    {
                        key: 'volume',
                        title: 'VOL：',
                        // 需要绘制的是矩形柱
                        type: 'bar',
                        // 这里我们想让矩形柱根据涨跌来显示不同的颜色
                        color: (data, options) => {
                            const kLineData = data.currentData.kLineData || {}
                            if (kLineData.close > kLineData.open) {
                                return options.bar.upColor
                            }
                            if (kLineData.close < kLineData.open) {
                                return options.bar.downColor
                            }
                            return options.bar.noChangeColor
                        },
                    },
                    // 第一条线ma5
                    {
                        key: 'ma5',
                        // 我们希望提示的显示的是  MA5: 'xxx'
                        title: 'MA5：',
                        // 需要绘制的是线
                        type: 'line',
                    },
                    // 第二条线ma10
                    {
                        key: 'ma10',
                        title: 'MA10：',
                        type: 'line',
                    },
                ],
                calcTechnicalIndicator: (dataList, calcParams, plots) => {
                    const volSums = []
                    // 注意：返回数据个数需要和dataList的数据个数一致，如果无值，用{}代替即可。
                    // 计算参数最好取回调参数calcParams，如果不是，后续计算参数发生变化的时候，这里计算不能及时响应
                    return dataList.map((kLineData, i) => {
                        const volume = kLineData.volume || 0
                        const vol = {volume}
                        calcParams.forEach((param, j) => {
                            volSums[j] = (volSums[j] || 0) + volume
                            if (i >= param - 1) {
                                vol[plots[j].key] = volSums[j] / param
                                volSums[j] -= dataList[i - (param - 1)].volume
                            }
                        })
                        // 如果有值的情况下，这里每一项的数据格式应该是 { ma5: xxx, ma10: xxx, volume: xxx }
                        // 每个key需要和plots中的子项key对应的值一致
                        return vol
                    })
                },
            })

            kLineChart.addCustomTechnicalIndicator({
                name: 'RSI',
                calcParams: [14],
                precision: 2,
                plots: [{key: 'rsi14', title: '', type: 'line'}],
                styles: {
                    line: {
                        size: 1,
                        colors: ['rgba(222, 148, 82, 1)', 'rgba(89, 181, 201, 1)', 'rgba(131, 74, 184, 1)'],
                    },
                },
                calcTechnicalIndicator(dataList, calcParams) {
                    const mapRes = []
                    const result = []
                    const _n = calcParams[0]
                    let _sumGain = 0
                    let _sumLoss = 0

                    dataList.forEach((kLineData, i) => {
                        const rsi = {}
                        const {close} = kLineData
                        const _change = i === 0 ? 0 : close - dataList[i - 1].close
                        const _gain = _change < 0 ? 0 : _change
                        const _loss = _change < 0 ? Math.abs(_change) : 0
                        let _avGain: number
                        let _avLoss: number

                        if (i >= 1 && i <= _n) {
                            _sumGain += _gain
                            _sumLoss += _loss
                        }

                        if (i === _n - 1) {
                            _avGain = _sumGain / 14
                            _avLoss = _sumLoss / 14
                        }

                        if (i >= _n) {
                            const _curObj = mapRes[i - 1] || {
                                avGain: _avGain,
                                avLoss: _avLoss,
                            }

                            _avGain = (_curObj.avGain * 13 + _gain) / 14
                            _avLoss = (_curObj.avLoss * 13 + _loss) / 14
                        }

                        if (i >= _n - 1) {
                            rsi[`rsi${_n}`] = formatDecimal((_avGain / (_avGain + _avLoss)) * 100 || 0, 6)
                        }

                        mapRes.push({
                            change: i === 0 ? 0 : _change,
                            gain: i === 0 ? 0 : _gain,
                            loss: i === 0 ? 0 : _loss,
                            avGain: i === 0 ? '' : _avGain,
                            avLoss: i === 0 ? '' : _avLoss,
                            rsi,
                        })

                        result.push(rsi)
                    })

                    return result
                },
            })

            kLineChart.addCustomTechnicalIndicator({
                name: 'KDJ',
                calcParams: [9, 3, 3],
                plots: [
                    {key: 'k', title: 'K: ', type: 'line'},
                    {key: 'd', title: 'D: ', type: 'line'},
                    {key: 'j', title: 'J: ', type: 'line'},
                ],
                precision: 2,
                styles: {
                    line: {
                        size: 1,
                        colors: ['rgba(222, 148, 82, 1)', 'rgba(89, 181, 201, 1)', 'rgba(131, 74, 184, 1)'],
                    },
                    tooltip: {
                        showParams: false,
                    },
                },
                calcTechnicalIndicator(dataList, calcParams) {
                    const result = []
                    const _res = []
                    function calcHnLn(data = []) {
                        let hn = -Infinity
                        let ln = Infinity
                        data.forEach((d) => {
                            hn = Math.max(d.high, hn)
                            ln = Math.min(d.low, ln)
                        })
                        return {hn, ln}
                    }

                    dataList.forEach((kLineData, i) => {
                        const kdj: {
                            rsv?: number
                            k?: number | string
                            d?: number | string
                            j?: number | string
                        } = {}
                        const _r: {rsv?: number; k?: number | string; d?: number | string; j?: number | string} = {}
                        const {close} = kLineData

                        if (i >= calcParams[0] - 1) {
                            const lhn = calcHnLn(dataList.slice(i - (calcParams[0] - 1), i + 1))
                            const {ln} = lhn
                            const {hn} = lhn
                            const hnSubLn = hn - ln
                            const rsv = ((close - ln) / (hnSubLn === 0 ? 1 : hnSubLn)) * 100
                            _r.rsv = rsv

                            kdj.k = (_res[i - 2].rsv + _res[i - 1].rsv + rsv) / calcParams[1]
                            kdj.d = (_res[i - 2].k + _res[i - 1].k + kdj.k) / calcParams[2]
                            kdj.j = 3.0 * kdj.k - 2.0 * kdj.d

                            kdj.k = Number.isNaN(kdj.k) ? '' : kdj.k
                            kdj.d = Number.isNaN(kdj.d) ? '' : kdj.d
                            kdj.j = Number.isNaN(kdj.j) ? '' : kdj.j

                            _r.k = kdj.k
                            _r.d = kdj.d
                            _r.j = kdj.j
                        }
                        result.push(kdj)
                        _res.push(_r)
                    })

                    return result
                },
            })

            kLineChart.addCustomTechnicalIndicator({
                name: 'MACD',
                calcParams: [12, 26, 9],
                baseValue: 0,
                precision: 2,
                plots: [
                    {
                        key: 'macd',
                        title: 'MACD: ',
                        type: 'bar',
                        color: (data, options) => {
                            const {currentData} = data
                            const {macd} = currentData.technicalIndicatorData || {}
                            if (macd > 0) {
                                return options.bar.upColor
                            }
                            if (macd < 0) {
                                return options.bar.downColor
                            }
                            return options.bar.noChangeColor
                        },
                        isStroke: (data) => {
                            const {preData, currentData} = data
                            const {macd} = currentData.technicalIndicatorData || {}
                            const preMacd = (preData.technicalIndicatorData || {}).macd
                            return preMacd < macd
                        },
                    },
                    {key: 'dif', title: 'DIF: ', type: 'line'},
                    {key: 'dea', title: 'DEA: ', type: 'line'},
                ],
                calcTechnicalIndicator: (dataList, calcParams) => {
                    let closeSum = 0,
                        emaShort,
                        emaLong,
                        dif = 0,
                        difSum = 0,
                        dea = 0
                    const maxPeriod = Math.max(calcParams[0], calcParams[1])
                    return dataList.map((kLineData, i) => {
                        const macd = {
                            dif: 0,
                            macd: 0,
                            dea: 0,
                        }
                        const {close} = kLineData
                        closeSum += close
                        if (i >= calcParams[0] - 1) {
                            if (i > calcParams[0] - 1) {
                                emaShort = (2 * close + (calcParams[0] - 1) * emaShort) / (calcParams[0] + 1)
                            } else {
                                emaShort = closeSum / calcParams[0]
                            }
                        }
                        if (i >= calcParams[1] - 1) {
                            if (i > calcParams[1] - 1) {
                                emaLong = (2 * close + (calcParams[1] - 1) * emaLong) / (calcParams[1] + 1)
                            } else {
                                emaLong = closeSum / calcParams[1]
                            }
                        }
                        if (i >= maxPeriod - 1) {
                            dif = emaShort - emaLong
                            macd.dif = dif
                            difSum += dif
                            if (i >= maxPeriod + calcParams[2] - 2) {
                                if (i > maxPeriod + calcParams[2] - 2) {
                                    dea = (dif * 2 + dea * (calcParams[2] - 1)) / (calcParams[2] + 1)
                                } else {
                                    dea = difSum / calcParams[2]
                                }
                                macd.macd = dif - dea
                                macd.dea = dea
                            }
                        }
                        return macd
                    })
                },
            })

            kLineChart.loadMore((timestamp) => {
                getHistoryData(timestamp, true)
            })

            document.addEventListener('keydown', escFunction, false)

            window.addEventListener(
                'resize',
                () => {
                    kLineChart.resize()
                },
                true
            )

            return () => {
                volId = ''
                kdjId = ''
                rsiId = ''
                document.removeEventListener('keydown', escFunction, false)
                window.removeEventListener(
                    'resize',
                    () => {
                        kLineChart.resize()
                    },
                    true
                )
                //   wss.current && wss.current.ws.readyState === 1 && wss.current.close()
                //   dispose('klineWrapper')
            }
        }, [])

        useEffect(() => {
            // dispose('klineWrapper')

            if (kLineChart && symbolData) {
                // historyDataRef.current = []
                // kLineChart.applyNewData([], true)
                // 为图表添加数据
                // setLoad(true)
                getHistoryData(new Date().getTime(), false)

                kLineChart.setPriceVolumePrecision(symbolData.price_precision || 2, symbolData.volume_precision || 2)
                kLineChart.overrideTechnicalIndicator({
                    name: 'MA',
                    calcParams: [5, 10, 30],
                    precision: symbolData.price_precision || 2,
                })

                kLineChart.overrideTechnicalIndicator({
                    name: 'BOLL',
                    precision: symbolData.price_precision || 2,
                })

                kLineChart.overrideTechnicalIndicator({
                    name: 'VOL',
                    precision: symbolData.price_precision || 2,
                })

                kLineChart.overrideTechnicalIndicator({
                    name: 'MACD',
                    precision: symbolData.price_precision || 2,
                })

                kLineChart.overrideTechnicalIndicator({
                    name: 'RSI',
                    precision: symbolData.price_precision || 2,
                })

                kLineChart.overrideTechnicalIndicator({
                    name: 'KDJ',
                    precision: symbolData.price_precision || 2,
                })

                kLineChart.setStyleOptions({
                    grid: {
                        display: true,
                        horizontal: {
                            display: true,
                            size: 1,
                            color: '#313343',
                            // 'solid'|'dash'
                            style: 'solid',
                            dashValue: [2, 2],
                        },
                        vertical: {
                            display: true,
                            size: 1,
                            color: '#313343',
                            // 'solid'|'dash'
                            style: 'solid',
                            dashValue: [2, 2],
                        },
                    },
                    realTime: {
                        averageLine: {
                            display: false,
                            color: '#F5A623',
                            size: 1,
                        },
                    },
                    xAxis: {
                        display: true,
                        height: null,
                        axisLine: {
                            display: false,
                            color: '#313343',
                            size: 1,
                        },
                        tickText: {
                            display: true,
                            color: '#676F81',
                            fontFamily: 'LaoMuangKhong',
                        },
                        tickLine: {
                            display: true,
                            color: '#242433',
                        },
                    },
                    yAxis: {
                        display: true,
                        height: null,
                        axisLine: {
                            display: false,
                            color: '#313343',
                            size: 1,
                        },
                        tickText: {
                            display: true,
                            color: '#676F81',
                            fontFamily: 'LaoMuangKhong',
                        },
                        tickLine: {
                            display: true,
                            color: '#242433',
                        },
                    },
                    technicalIndicator: {
                        tooltip: {
                            showRule: 'follow_cross',
                            text: {
                                marginLeft: 4,
                                marginRight: 4,
                            },
                            defaultValue: '-',
                        },
                        bar: {
                            // upColor: 'rgb(21, 68, 61)',
                            // downColor: 'rgb(87, 39, 42)',
                            noChangeColor: '#76808F',
                            upColor: 'rgba(14, 167, 134, 1)',
                            downColor: 'rgba(233, 71, 71, 1)',
                        },
                    },
                    separator: {
                        size: 1,
                        color: 'rgba(37, 39, 46, 1)',
                        fill: true,
                    },
                    candle: {
                        priceMark: {
                            high: {
                                color: 'rgba(103, 111, 129, 1)',
                                fontFamily: 'LaoMuangKhong',
                            },
                            low: {
                                color: 'rgba(103, 111, 129, 1)',
                                fontFamily: 'LaoMuangKhong',
                            },
                        },
                        bar: {
                            upColor: 'rgba(14, 167, 134, 1)',
                            downColor: 'rgba(233, 71, 71, 1)',
                        },
                        tooltip: {
                            showRule: 'follow_cross',
                            text: {
                                color: 'rgba(103, 111, 129, 1)',
                                marginLeft: 4,
                                marginRight: 4,
                            },
                            values: (klineData) => {
                                const _diff = NP.minus(klineData.close, klineData.open)
                                const _percent = changeToPercentCeil(NP.divide(_diff, klineData.open))
                                let _color = '#76808F'
                                if (_diff < 0) {
                                    _color = 'rgba(233, 71, 71, 1)'
                                } else if (_diff > 0) {
                                    _color = 'rgba(14, 167, 134, 1)'
                                }

                                return [
                                    // {
                                    //   value: moment(klineData.timestamp).format('YYYY-MM-DD hh:mm:ss'),
                                    // },
                                    {
                                        value: klineData.open,
                                    },
                                    {
                                        value: klineData.close,
                                    },
                                    {
                                        value: klineData.high,
                                    },
                                    {
                                        value: klineData.low,
                                    },
                                    {
                                        value: klineData.volume || 0,
                                    },
                                    {
                                        value: `${_diff > 0 ? `+${_diff}` : _diff}(${
                                            _percent > 0 ? `+${_percent}` : _percent
                                        })`,
                                        color: _color,
                                    },
                                ]
                            },
                            defaultValue: 0,
                            labels:
                                router.locale === 'zh-CN'
                                    ? ['开：', '收：', '高：', '低：', '量：', '']
                                    : router.locale === 'zh-HK'
                                    ? ['開：', '收：', '高：', '低：', '量：', '']
                                    : ['O: ', 'C: ', 'H: ', 'L: ', 'Vol: ', ''],
                        },
                    },
                })
            }
        }, [symbolData, kLineChart])

        useEffect(() => {
            kLineChart.resize()
        }, [isFullScreen])

        const handleSetFull = useCallback(() => {
            setFullScreen((prev) => !prev)
            syncFullScreen(!isFullScreen)
        }, [isFullScreen])

        const handleSetTech = useCallback(
            (k, v) => {
                const _newTech = {...tech, [k]: v}

                setTech(_newTech)
                localStorage.tech = JSON.stringify(_newTech)

                switch (v) {
                    case 'KDJ':
                        volId && kLineChart.removeTechnicalIndicator(volId, 'VOL')
                        rsiId && kLineChart.removeTechnicalIndicator(rsiId, 'RSI')
                        macdId && kLineChart.removeTechnicalIndicator(macdId, 'MACD')
                        kdjId = kLineChart.createTechnicalIndicator('KDJ', false, {
                            height: 60,
                        })
                        break
                    case 'MACD':
                        volId && kLineChart.removeTechnicalIndicator(volId, 'VOL')
                        kdjId && kLineChart.removeTechnicalIndicator(kdjId, 'KDJ')
                        rsiId && kLineChart.removeTechnicalIndicator(rsiId, 'RSI')
                        macdId = kLineChart.createTechnicalIndicator('MACD', false)
                        break
                    case 'RSI':
                        volId && kLineChart.removeTechnicalIndicator(volId, 'VOL')
                        kdjId && kLineChart.removeTechnicalIndicator(kdjId, 'KDJ')
                        macdId && kLineChart.removeTechnicalIndicator(macdId, 'MACD')
                        rsiId = kLineChart.createTechnicalIndicator('RSI', false, {
                            height: 60,
                        })
                        break
                    case 'MA':
                        kLineChart.removeTechnicalIndicator('candle_pane')
                        kLineChart.createTechnicalIndicator('MA', false, {
                            id: 'candle_pane',
                        })
                        break
                    case 'VOL':
                        kdjId && kLineChart.removeTechnicalIndicator(kdjId, 'KDJ')
                        rsiId && kLineChart.removeTechnicalIndicator(rsiId, 'RSI')
                        macdId && kLineChart.removeTechnicalIndicator(macdId, 'MACD')
                        volId = kLineChart.createTechnicalIndicator('VOL', false, {
                            height: 60,
                        })
                        break
                    case 'BOLL':
                        kLineChart.removeTechnicalIndicator('candle_pane')
                        kLineChart.createTechnicalIndicator('BOLL', false, {
                            id: 'candle_pane',
                        })
                        break
                    case '':
                        if (k === 'main') {
                            kLineChart.removeTechnicalIndicator('candle_pane')
                        } else {
                            kdjId && kLineChart.removeTechnicalIndicator(kdjId, 'KDJ')
                            rsiId && kLineChart.removeTechnicalIndicator(rsiId, 'RSI')
                            volId && kLineChart.removeTechnicalIndicator(volId, 'VOL')
                            macdId && kLineChart.removeTechnicalIndicator(macdId, 'MACD')
                        }
                        break
                    default:
                        return null
                }
            },
            [tech, kLineChart]
        )

        const handleTimeChoose = useCallback(
            (item) => {
                setTimeChoose(item)
                setKlineType(item.wsType)
                timeChooseRef.current = item

                getHistoryData(new Date().getTime(), false)

                handleSwitchTech()

                localStorage.timeChoose = JSON.stringify(item)
                if (item.chartType === 3) {
                    kLineChart.setStyleOptions({
                        candle: {
                            type: 'area',
                        },
                    })
                } else {
                    kLineChart.setStyleOptions({
                        candle: {
                            type: 'candle_solid',
                        },
                    })
                }
            },
            [symbolData, timeChoose, handleSwitchTech]
        )

        return (
            <Wrap isFullScreen={isFullScreen}>
                {isLoad && <Loading />}
                <HeaderTool>
                    {chartType === 'kline' && (
                        <KlineToolWrap>
                            <KlineTimeTool handleTimeClick={(v) => handleTimeChoose(v)} />

                            {timeChoose.chartType !== 3 && chartType === 'kline' && (
                                <HeaderIconWrapper onClick={() => setShowTechModal(!isShowTechModal)}>
                                    <img
                                        src={require('../../public/static/img/kline_setting_icon.png')}
                                        alt="kline setting icon"
                                        width="16"
                                        height="14"
                                    />
                                </HeaderIconWrapper>
                            )}
                            <HeaderIconWrapper onClick={handleSetFull}>
                                <svg
                                    width="14"
                                    height="14"
                                    viewBox="0 0 14 14"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path
                                        d="M1.49615 1.49615V5.09182H0.25V0.250001H5.09183V1.49615H1.49615Z"
                                        fill="#676F81"
                                    />
                                    <path
                                        d="M12.5038 5.93558V2.37733L9.34875 5.53241L8.46759 4.65125L11.6227 1.49617H8.06442V0.250016H13.75V5.93558H12.5038Z"
                                        fill="#676F81"
                                    />
                                    <path
                                        d="M1.49615 8.06444V11.6227L4.65125 8.4676L5.53241 9.34876L2.37732 12.5038H5.93558V13.75H0.25V8.06444H1.49615Z"
                                        fill="#676F81"
                                    />
                                    <path
                                        d="M12.5038 8.90817V12.5038H8.90817V13.75H13.75V8.90817H12.5038Z"
                                        fill="#676F81"
                                    />
                                </svg>
                            </HeaderIconWrapper>
                        </KlineToolWrap>
                    )}

                    {/* <SideTool>
                        <ModelButton
                            isActive={chartType === 'kline'}
                            onClick={() => {
                                setChartType('kline')
                            }}
                        >
                            Original
                        </ModelButton> */}
                    {/* {!isFullScreen && (
                            <ModelButton
                                isActive={chartType === 'depth'}
                                onClick={() => {
                                    setChartType('depth')
                                }}
                            >
                                {t('klineDepthTitle')}
                            </ModelButton>
                        )} */}
                    {/* </SideTool> */}
                </HeaderTool>

                {isShowTechModal && (
                    <KlineTechnicalModal theme="dark" close={hideTechModal} handleChange={handleSetTech} data={tech} />
                )}

                {/* {chartType === 'kline' &&
                
            } */}
                <div className={`kline klineWrap ${chartType === 'kline' ? 'show' : 'hidden'}`} id="klineWrapper" />

                {chartType === 'depth' && (
                    <div className={`klineWrap ${chartType === 'depth' ? 'show' : 'hidden'}`}>
                        {symbolData && (
                            <DepthChart
                                data={wsOrderBookData}
                                volumePrecision={symbolData.volume_precision}
                                pricePrecision={symbolData.price_precision}
                            />
                        )}
                    </div>
                )}
            </Wrap>
        )
    }
)

const Wrap = styled.div<StyleProps>`
    height: 100%;
    width: 100%;
    max-height: 100%;
    padding: 0 16px;
    visibility: auto;
    position: ${(props) => (props.isFullScreen ? 'fixed' : 'relative')};
    background: #242433;
    left: 0;
    top: 0;
    bottom: 0;
    right: 0;
    z-index: ${(props) => (props.isFullScreen ? 10 : 1)};

    .kline {
        height: calc(100% - 58px);
    }

    .klineWrap {
        position: absolute;
        top: 58px;
        left: 16px;
        right: 16px;
        bottom: 0;
        visibility: hidden;

        &.show {
            visibility: visible;
        }
    }
`

const HeaderTool = styled.div`
    width: 100%;
    height: 56px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    box-sizing: content-box;
    border-bottom: 1px solid rgba(55, 55, 75, 1);

    .item {
        padding: 0 16px 0 0;
        font-size: 12px;
        line-height: 12px;
        color: rgba(103, 111, 129, 1);
        border: 0;
        outline: none;
        background: #242433;
        cursor: pointer;

        &.active {
            color: #3b75e1;
        }
    }
`

const HeaderIconWrapper = styled.div`
    width: 24px;
    height: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-left: 12px;
    background: transparent;
    cursor: pointer;
    border-radius: 4px;
    flex: 0 0 auto;

    img {
        width: 16px;
        height: 14px;
    }

    &:hover {
        background: rgba(103, 111, 129, 0.1);
    }
`

const KlineToolWrap = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
`
