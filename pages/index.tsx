import {useState, useMemo, useEffect, useCallback} from 'react'
import styled from 'styled-components'
import Web3 from 'web3'
import {Scrollbars} from 'react-custom-scrollbars'
import FileReaderInput from 'react-file-reader-input'
import Decimal from 'decimal.js'

import { DEFAULT_CONTRACT_MAP } from 'web3/constants'
import Header from 'components/common/Header'
import {ContractAction} from 'web3/airDropInstance'
import {useAuth} from 'utils/auth-context'
import useAlert from 'hooks/useAlert'

import OvalModal, {HeaderLine, ContentLine, BtnGroup, ConfirmButton, LeftButton} from 'components/common/OvalModal'
interface StyleProps {
    chg?: number
    isActive?: boolean
}

let web3
let AirdropContract

const Index = (): JSX.Element => {
    const {userToken, allFullfilled, chainCorrect} = useAuth()
    const [chain, setChain] = useState('bsc')
    const [userAddr, setUserAddr] = useState('')
    const [airDropAddress, setAirDropAddress] = useState(DEFAULT_CONTRACT_MAP['bsc']['usdtAddress'])
    const [airDropName, setAirDropName] = useState('USDT')
    const [airDropBlance, setAirDropBalance] = useState('0')
    const [settingModal, setSetModal] = useState(false)
    const [setmodalInput, setSetModalInput] = useState('')
    const [showSymbolList, setShowSymbolList] = useState(false)
    const [setAlert] = useAlert()
    const [submitLoad, setSubmitLoad] = useState(false)

    const [switchTab, setTab] = useState(1)

    // 页面显示
    const [pageData, setPageData] = useState(null)
    // 上传
    const [upData, setUpData] = useState(null)
    // 记录有错误的
    const [errorLines, setErrorLines] = useState('')
    // 记录地址数量
    const [addressNum, setAddrNum] = useState(0)
    // 记录空投总数
    const [airDropNum, setAirDropNum] = useState(0)

    const [chongNum, setChongNum] = useState('')
    const [chongModal, setChongModal] = useState(false)

    const [tiAddr, setTiAddr] = useState('')
    const [tiModal, setTiModal] = useState(false)
    const [tiLoad, setTiLoad] = useState(false)

    const initData = useCallback(
        () => {
          setPageData(null)
          setUpData(null)
          setErrorLines('')
          setAddrNum(0)
          setAirDropNum(0)
        },
        [],
      )

    const contractAddr = useMemo(() => {
        return DEFAULT_CONTRACT_MAP[chain]['contractAddress']
    }, [chain])

    const initContract = () => {
        return new Promise<void>((resolve, reject) => {
            const {ethereum} = window as any
            if (!ethereum) {
                return
            }
            web3 = new Web3(ethereum)
            AirdropContract = new ContractAction(web3, chain)

            ethereum.enable().then((res) => {
                if (res?.length) {
                    setUserAddr(res[0])
                }
            })

            if (AirdropContract) {
                resolve()
            } else {
                reject()
            }
        })
    }

    const getTokenDetail = async () => {
        if (!AirdropContract) {
            return
        }

        try {
            const res = await AirdropContract.tokenAddr()
            
            if (res) {
                setAirDropAddress(res)
                getAirDropInfo()
            }

        } catch (err) {
            //
        }
    }

    const [symbolDemical, setDemical] = useState(18)

    const getAirDropInfo = async () => {
        if (!AirdropContract) {
            return
        }

        try {
            const res = await AirdropContract.getBalance(airDropAddress)
          
            if (res && res.symbolName) {
                setAirDropName(res.symbolName)
                setAirDropBalance(res.symbolBalance)
                setDemical(res.demical)
            }
        } catch (err) {
            //
        }
    }

    const [setLoad, setLoads] = useState(false)
    // 设置空投地址
    const handleSetAirDrop = async () => {
        if (!AirdropContract || setLoad) {
            return
        }

        setLoads(true)

        try {
            const res = await AirdropContract.mwUpdateTokenAddress(setmodalInput, userToken)
       
            if (res && res.status) {
                res && setAlert({
                    type: 'success',
                    message: '设置成功',
                })
                setAirDropAddress(setmodalInput)
                getTokenDetail()
                setSetModal(false) 
                setSetModalInput('')
            } else {
                setAlert({
                    type: 'error',
                    message: '失败',
                })
            }
            
            setLoads(false)
        } catch (err) {
            //
            setAlert({
                type: 'error',
                message: '失败',
            })
            setLoads(false)
        }
    }

    useEffect(() => {
        airDropAddress && getAirDropInfo()
    }, [airDropAddress])

    useEffect(() => {
        initContract()
        initData()
    }, [chain])

    useEffect(() => {
      if (chainCorrect && allFullfilled && userToken) {
        getTokenDetail()
      }
    }, [userToken, allFullfilled, chainCorrect])

    

    const handleFileChange = useCallback(
      (event, results) => {
        results.forEach(result => {
            const [e, file] = result;
            const _all = e.target.result.split('\r\n')

            const _page = []
            const _up = [[], []]
            let _lines = ''
            let _addrnum = 0
            let _airDropNum = 0

            if (_all && _all.length) {
                _all.map((item, index) => {
                    if (index > 1) {
                        const _i = item.split(',')

                        _page.push({
                            address: _i[1] || '',
                            num: _i[2] || ''
                        })

                        _up[0].push(_i[1])
                        _up[1].push(_i[2])

                        if (!_i[1] || !_i[2] || _i[1].indexOf('0x') === -1) {
                            _lines = _lines.length === 0 ? `${index - 1}` : `${_lines}, ${index - 1}`
                        }

                        _addrnum = (new Decimal(_addrnum).plus(1).valueOf() as any) * 1
                        _airDropNum = (new Decimal(_addrnum).plus(_i[2] * 1 || 0).valueOf() as any) * 1
                    }
                })

                if (_addrnum > 500) {
                    setAlert({
                        type: 'error',
                        message: '空投地址最多支持500个',
                    })

                    return false
                }

                setPageData(_page)
                setUpData(_up)
                setErrorLines(_lines)
                setAddrNum(_addrnum)
                setAirDropNum(_airDropNum)
            }
          });
      },
      [],
    )


    const handleSend = async () => {
        if (!AirdropContract || setLoad) {
            return
        }

        setSubmitLoad(true)

        try {
            const res = await AirdropContract.mwDropTokens(upData, userToken)
            
            res && setAlert({
                type: 'success',
                message: '成功',
            })
            initData()
            setSubmitLoad(false)
        } catch (err) {
            //
            setAlert({
                type: 'error',
                message: '失败',
            })
            setSubmitLoad(false)
        }
    }

    const [chongLoad, setChongLoad] = useState(false)

    const handleSubmitChong = async () => {
        if (!AirdropContract || setLoad) {
            return
        }

        setChongLoad(true)

        try {
            const res = await AirdropContract.withDraw(chongNum, airDropAddress, userToken)
            
            res && setAlert({
                type: 'success',
                message: '成功',
            })

            getAirDropInfo()
            setChongLoad(false)
        } catch (err) {
            //
            setAlert({
                type: 'error',
                message: '失败',
            })
            setChongLoad(false)
        }
    }

    const handleSubmitTi = async () => {
        if (!AirdropContract || setLoad) {
            return
        }

        setTiLoad(true)

        try {
            const res = await AirdropContract.mwWithdrawTokens(tiAddr, userToken)
            
            res && setAlert({
                type: 'success',
                message: '成功',
            })

            getAirDropInfo()
            setTiLoad(false)
        } catch (err) {
            //
            setAlert({
                type: 'error',
                message: '失败',
            })
            setTiLoad(false)
        }
    }

    const handleSubmit = useCallback(
      () => {
        handleSend()
      },
      [],
    )

    const handleChong = useCallback(
      (v) => {
        const re = new RegExp(`^\\d*(\\.?\\d{0,${symbolDemical}})`, 'g')
        const val = v.match(re)[0]

        setChongNum(val)
      },
      [symbolDemical],
    )

    return (
        <div style={{}} className="container">
            <style jsx global>{`
                body ::-webkit-scrollbar {
                    width: 0;
                }
            `}</style>

            <Header isIndex={true} chain={chain} />

            <MainWrap>
                <div className="main">
                    <div className="symbolList">
                        <div className="item">
                            <div className="title">当前网络</div>
                            <div className="sideContent">
                                <div 
                                    onClick={() => {
                                        setChain('bsc')
                                    }}
                                    className={`btn ${chain === 'bsc' ? 'active' : ''}`}
                                >BSC</div>
                                <div 
                                    onClick={() => {
                                        setChain('eth')
                                    }}
                                    className={`btn ${chain === 'eth' ? 'active' : ''}`}
                                >
                                    Ethereum</div>
                            </div>
                        </div>

                        <div className="item">
                            <div className="title">当前合约地址</div>
                            <div className="sideContent">
                                <p className="addr">{contractAddr}</p>
                            </div>
                        </div>

                        <div className="item">
                            <div className="title">当前空投币种</div>
                            <div className="sideContent">
                                <p className="addr">
                                    {airDropName} <br />
                                    {airDropAddress}
                                </p>
                            </div>
                            <div className="setBtn" onClick={() => { setSetModal(true) }}>设置</div>
                        </div>

                        <div className="item">
                            <div className="title">余额</div>
                            <div className="sideContent">
                                <p className="balance">
                                    {airDropBlance} {airDropName}
                                </p>
                            </div>
                        </div>
                    </div>

                    <div className="switchTab">
                        <div className={`btn ${switchTab === 1 ? 'active' : ''}`} onClick={() => { setTab(1)}}>空投/刷holders</div>
                        <div className={`btn ${switchTab === 2 ? 'active' : ''}`} onClick={() => { setTab(2)}}>充币</div>
                        <div className={`btn ${switchTab === 3 ? 'active' : ''}`} onClick={() => { setTab(3)}}>提币</div>
                    </div>

                    {switchTab === 1 &&
                        <div className="air1">
                            <div className="tools">
                                <a className="download" href="./public/static/模板.zip">下载模板</a>
                                <div className="upload">
                                    <FileReaderInput as="text" id="my-file-input"
                            onChange={handleFileChange} accept=".csv" >
                                    <span>上传文件</span>
                                    </FileReaderInput>
                                    
                                </div>
                                
                            </div>

                            <div className="uploadTips">
                                    <p className="item">
                                        1、文件格式支持  '.csv' '.xlsm' '.xlsx' '.xls'
                                    </p>
                                    <p className="item">
                                        2、格式： <b>地址</b>, <b>数量</b>
                                    </p>
                                    <p className="item red">
                                        3、每次提交最多500条数据
                                    </p>
                                </div>

                            <div className="uploadDetail">
                                <h4 className="title">上传信息:</h4>


                                <div className="list">
                                    <ScrollBar
                                        renderThumbVertical={({style, ...props}) => (
                                            <div {...props} style={{...style, backgroundColor: '#ECEEF1', width: '2px !important'}} />
                                        )}
                                        style={{height: 330}}
                                    >
                                        {pageData && pageData.length > 0 && pageData.map((item,index) => (
                                            <div className="item" key={index}>
                                                <span className="dot">{index + 1}</span>
                                                <span className="val">{item.address || ''}, {item.num}</span>
                                            </div>
                                        ))}
                                        
                                    </ScrollBar>
                                </div>

                                <h4 className="title">错误行:</h4>
                                <p className="lineWrap">{errorLines}</p>

                                <div className="divide"></div>
                                            <div className="row">
                                                <div className="sideItem">
                                                    <span className="title">地址总数</span>
                                                    <span className="val">{addressNum}</span>
                                                </div>
                                                <div className="sideItem">
                                                    <span className="title">空投总数</span>
                                                    <span className="val">{airDropNum}</span>
                                                </div>
                                            </div>
                                <div className="divide"></div>

                                <ConfirmButton 
                                    onClick={handleSubmit} 
                                    loading={submitLoad}
                                    className="submitBtn"
                                    disabled={!upData || (upData && upData[0] && upData[0].length === 0) || errorLines.length > 0}
                                >
                                    提交
                                </ConfirmButton>

                            </div>
                        </div>
                    }

                    {switchTab === 2 && 
                        <div className="air2">
                            <div className="item">
                                <p className="label">充币数量:</p>
                                <InputWrap
                                    type="text"
                                    placeholder=""
                                    onChange={(e) => handleChong(e.target.value)}
                                    value={chongNum}  
                                />
                            </div>

                            <ConfirmButton
                                 className="submitBtn" 
                                 onClick={() => {
                                    setChongModal(true) 
                                 }} 
                                 
                                 disabled={!chongNum}
                                 >
                                确认
                            </ConfirmButton>
                        </div>
                    }

{switchTab === 3 && 
                        <div className="air2">
                            <div className="item">
                                <p className="label">提币目标地址:</p>
                                <InputWrap
                                    type="text"
                                    placeholder="输入提币目标地址"
                                    onChange={(e) => setTiAddr(e.target.value)}
                                    value={tiAddr}  
                                />
                            </div>

                            <ConfirmButton
                                 className="submitBtn" 
                                 onClick={() => {
                                    setTiModal(true) 
                                 }} 
                                 
                                 disabled={!tiAddr}
                                 >
                                确认
                            </ConfirmButton>
                        </div>
                    }
                </div>
            </MainWrap>

            {settingModal &&
                <OvalModal 
                    visible={settingModal} 
                    onCancel={() => { 
                        setSetModal(false) 
                        setShowSymbolList(false)
                        setSetModalInput('')
                    }}
                >
                    <HeaderLine>设置空投币种</HeaderLine>
                   
                    <ContentLine onMouseLeave={() => {
                                setShowSymbolList(false)
                            }}>
                        <InputWrap
                            type="text"
                            placeholder=""
                            onChange={(e) => setSetModalInput(e.target.value)}
                            value={setmodalInput}
                            onMouseEnter={() => {
                                setShowSymbolList(true)
                            }}
                            
                        />
                        {showSymbolList && 
                            <div className="selectList">
                                <div className="item" onClick={(e) => {
                                    setSetModalInput(DEFAULT_CONTRACT_MAP[chain]['usdtAddress'])
                                    
                                    setShowSymbolList(false)
                                }}>
                                    <div>
                                        <img src={'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/ethereum/assets/0xdAC17F958D2ee523a2206206994597C13D831ec7/logo.png'} width="25" height="25" alt="logo" />
                                        <span className="title">USDT</span>
                                    </div>
                                    
                                    <span className="address">{DEFAULT_CONTRACT_MAP[chain]['usdtAddress']}</span>
                                </div>
                            </div>
                        }
                        
                    </ContentLine>
                    <BtnGroup>
                        <LeftButton onClick={() => { 
                            setSetModal(false) 
                            setSetModalInput('')
                            }} style={{ marginRight: '16px'}}>取消</LeftButton>
                        <ConfirmButton onClick={handleSetAirDrop} disabled={setLoad} loading={setLoad}>
                            确认
                        </ConfirmButton>
                    </BtnGroup>
                </OvalModal>
            }

            {chongModal &&
                <OvalModal 
                    visible={chongModal} 
                    onCancel={() => { 
                        setChongModal(false) 
                    }}
                >
                    <HeaderLine>充币</HeaderLine>
                   
                    <ContentLine>
                       <BalanceList>
                           <div className="balanceItem">
                               <span className="label">充币合约</span>
                               <span className="val">{contractAddr}</span>
                           </div>
                           <div className="balanceItem">
                               <span className="label">充币币种</span>
                               <span className="val">{airDropName}<br />{airDropAddress}</span>
                           </div>
                           <div className="balanceItem">
                               <span className="label">充币数量</span>
                               <span className="val">{chongNum}</span>
                           </div>
                       </BalanceList>
                        
                    </ContentLine>
                    <BtnGroup>
                        <LeftButton 
                            onClick={() => { 
                                setChongModal(false) 
                            }} 
                            style={{ marginRight: '16px'}}
                        >
                            取消
                        </LeftButton>
                        <ConfirmButton onClick={handleSubmitChong} loading={chongLoad}>
                            确认
                        </ConfirmButton>
                    </BtnGroup>
                </OvalModal>
            }

{tiModal &&
                <OvalModal 
                    visible={tiModal} 
                    onCancel={() => { 
                        setTiModal(false) 
                    }}
                >
                    <HeaderLine>提币</HeaderLine>
                   
                    <ContentLine>
                       <BalanceList>
                           <div className="balanceItem">
                               <span className="label">提币从合约</span>
                               <span className="val">{contractAddr}</span>
                           </div>
                           <div className="balanceItem">
                               <span className="label">到目标地址</span>
                               <span className="val">{tiAddr}</span>
                           </div>
                           
                           <div className="balanceItem">
                               <span className="label">提币币种</span>
                               <span className="val">{airDropName}<br />{airDropAddress}</span>
                           </div>
                           <div className="balanceItem">
                               <span className="label">提币数量</span>
                               <span className="val">{airDropBlance}</span>
                           </div>
                       </BalanceList>
                        
                    </ContentLine>
                    <BtnGroup>
                        <LeftButton 
                            onClick={() => { 
                                setTiModal(false) 
                            }} 
                            style={{ marginRight: '16px'}}
                        >
                            取消
                        </LeftButton>
                        <ConfirmButton onClick={handleSubmitTi} loading={tiLoad}>
                            确认
                        </ConfirmButton>
                    </BtnGroup>
                </OvalModal>
            }
            
        </div>
    )
}

const InputWrap = styled.input`
    width: 100%;
    border-radius: 10px;
    padding: 0 10px;
    position: relative;
    background: rgba(0,0,0,.85);
    border: 2px solid rgba(0,0,0,.85);
    height: 50px;
    color: #eecc4f;
    font-size: 14px;
    font-weight: 400;
    line-height: 48px;
    text-align: left;
    outline: none;
    position: relative;
    z-index: 0;
`

const BalanceList = styled.div`
    .balanceItem {
                width: 100%;
                height: 50px;
                display: flex;
                justify-content: flex-start;
                align-items: center;
                font-size: 14px;
                line-height: 20px;

                .label {
                    display: block;
                    width: 100px;
                    color: #fff;
                }
                .val {
                    color: #f9a17e;
                    font-weight: 600;
                }
            }
`

const MainWrap = styled.div`
    width: 100%;
    height: auto;
    padding: 50px 0;

    .air2 {
        .item {

            .label {
                color: #f9a17e;
                font-size: 15px;
                margin-bottom: 10px;
                font-weight: 600;
            }
        }
    }

    .submitBtn {
            margin: 20px auto;
        }

    .air1 {

        .divide {
            height: 1px;
            background: hsla(0,0%,100%,.2);
            margin: 20px 0;
        }

        .row {
            display: flex;
            width: 100%;
            height: 40px;
            align-items: center;

            .sideItem {
                width: 50%;
                display: flex;
                justify-content: space-between;
                align-items: center;
                padding: 0 18px;
                font-size: 14px;

                .title {
                    color: #fff;
                }

                .val {
                    color: #f9a17e;
                    font-weight: 600;
                }
            }
        }

        .uploadDetail {
            margin-top: 20px;

            h4.title {
                height: 24px;
                width: 100%;
                color: #f9a17e;
                font-size: 15px;
                margin-bottom: 10px;
                font-weight: 600;
            }

            .lineWrap {
                height: 40px;
                display: flex;
                align-items: center;
                padding-left: 16px;
                font-size: 14px;
                background: #14161a;
                border: 2px solid #2a2d34;
                border-radius: 10px;
                color: #fff;
            }

            .list {
                width: 100%;
                height: 350px;
                background: #14161a;
                border: 2px solid #2a2d34;
                border-radius: 10px;
                padding: 10px 10px 10px 0;
                margin-bottom: 20px;

                .item {
                    width: 100%;
                    height: 22px;
                    line-height: 22px;
                    display: flex;
                    justify-content: flex-start;
                    align-items: center;
                    color: #fff;
                    font-size: 14px;

                    span.dot {
                        display: block;
                        width: 30px;
                        color: #546e7a;
                        text-align: right;
                        margin-right: 8px;
                    }

                    span.val {
                        letter-spacing: .1px;
                    }
                }
            }
        }
        .uploadTips {
            width: 300px;
            margin: 10px auto 0;

            p.item {
                width: 100%;
                height: 20px;
                line-height: 20px;
                color: #fff;
                font-size: 14px;
                text-align: left;
                margin-bottom: 2px;

                &.red {
                    color: red !important;
                }

                b {
                    color: #eecc4f;
                }
            }
        }
        .tools {
            width: 100%;
            height: 36px;
            display: flex;
            align-items: center;
            color: #eecc4f;
            text-align: center;
            font-weight: 600;

            .fileInput {
                position: absolute;
                left: 0;
                top: 0;
                right: 0;
                bottom: 0;
                z-index: 2;
                opacity: 0;
                cursor: pointer;
            }

            .download, .upload {
                width: 50%;
                position: relative;
                text-decoration: none;
                color: #eecc4f;
                user-select: none;
                cursor: pointer;
            }

            .upload {
               position: relative;

            }
        }
    }

    .setBtn {
        display: flex;
        justify-content: center;
        align-items: center;
        box-sizing: border-box;
        border-radius: 10px;
        padding: 6px 10px;
        cursor: pointer;
        user-select: none;
        color: #fff;
        font-size: 14px;
        background: #ff8f6c;
        margin-left: 14px;
    }

    .btn {
        display: flex;
        justify-content: center;
        align-items: center;
        background: #14161a;
        border: 2px solid #2a2d34;
        box-sizing: border-box;
        border-radius: 10px;
        padding: 10px 24px;
        cursor: pointer;
        user-select: none;
        color: #fff;
        font-size: 14px;
        margin-right: 12px;

        &.active {
            border-color: #eecc4f!important;
        }
    }

    .main {
        width: 900px;
        margin: 0 auto;
        padding: 24px 30px;
        border-radius: 12px;
        background: #14161a;
        border: 10px solid #2a2d34;
        border-radius: 20px;
        margin-bottom: 30px;

        .switchTab {
            width: 100%;
            height: 60px;
            display: flex;
            justify-content: center;
            align-items: center;
            margin: 40px auto;
        }

        .symbolList {

            .item {
                width: 100%;
                height: 50px;
                display: flex;
                justify-content: flex-start;
                align-items: center;

                .title {
                    width: 90px;
                    font-size: 14px;
                    color: #f9a17e;
                    margin-right: 40px;
                }

                .addr {
                    font-size: 16px;
                    color: #eecc4f;
                }

                .balance {
                    font-size: 16px;
                    color: #fff;
                }

                .sideContent {
                    display: flex;
                    justify-content: flex-start;
                    align-items: center;
                }
            }
        }
    }
`


const ScrollBar = styled(Scrollbars)`
    width: 100%;
    margin: -1px auto 0;
    & > div {
        overflow-x: hidden !important;
        margin-bottom: 0 !important;
    }

    & > div:nth-of-type(2) {
        display: none !important;
    }
`


export default Index
