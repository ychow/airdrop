# bw-web-v2 (Nextjs + Typescript)

## node env

```bash
node: v14+
```

## How to Use

1. install

```bash
yarn install
```

2. development mode

```bash
yarn dev
```

## commit msg rules

[commitlint](https://github.com/conventional-changelog/commitlint/tree/master/@commitlint/config-conventional)

## Test

-   Testing with [Jest](https://jestjs.io/) and [`react-testing-library`](https://testing-library.com/docs/react-testing-library/intro)
