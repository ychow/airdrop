import useSWR, {ConfigInterface, responseInterface} from 'swr'
import axios, {AxiosRequestConfig, AxiosResponse, AxiosError} from 'axios'
import qs from 'qs'
import DeviceDetector from 'device-detector-js'
import {getTokenCookie, getUUIDCookie} from 'utils/auth-cookie'

const deviceDetector = new DeviceDetector()
const device = deviceDetector.parse(typeof window !== 'undefined' ? window.navigator.userAgent : '')

export type GetRequest = AxiosRequestConfig | null

interface Return<Data, Error>
    extends Pick<
        responseInterface<AxiosResponse<Data>, AxiosError<Error>>,
        'isValidating' | 'revalidate' | 'error' | 'mutate'
    > {
    data: Data | undefined
    response: AxiosResponse<Data> | undefined
}

export interface Config<Data = unknown, Error = unknown>
    extends Omit<ConfigInterface<AxiosResponse<Data>, AxiosError<Error>>, 'initialData'> {
    initialData?: Data
}

export interface ResponseData {
    errno: number
    errmsg: string
    data: unknown
}

export function useRequest<Data = ResponseData, Error = unknown>(
    key: string,
    request: GetRequest,
    {initialData, ...config}: Config<Data, Error> = {}
): Return<Data, Error> {
    // const lang = getLangCookie()
    const token = getTokenCookie(null) || null
    const uuid = getUUIDCookie(null) || ''
    const _channel = (typeof window !== 'undefined' && localStorage.Channel) || ''

    const realRequest: GetRequest = {
        ...request,
        url: `${request.url}`,
        headers: {
            Authorization: token || null,
            // 'FT-language': lang === 'en' ? 'en-US' : lang,
            Uuid: uuid || '',
            ...request.headers,
        },
        data: request?.data && qs.stringify(request.data),
    }

    if (_channel) {
        realRequest['headers']['Channel'] = _channel
    }

    const {data: response, error, isValidating, revalidate, mutate} = useSWR<AxiosResponse<Data>, AxiosError<Error>>(
        key,
        /**
         * NOTE: Typescript thinks `request` can be `null` here, but the fetcher
         * function is actually only called by `useSWR` when it isn't.
         */
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        () => axios(realRequest!),
        {
            ...config,
            initialData: initialData && {
                status: 200,
                statusText: 'InitialData',
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                config: request!,
                headers: {},
                data: initialData,
            },
        }
    )

    if (error?.response?.status) {
        const logData = {
            rows: [
                {
                    t: new Date().getTime(),
                    l: 'error',
                    uu: uuid,
                    d: device?.client?.name,
                    v: device?.client?.version,
                    // lg: lang === 'en' ? 'en-US' : lang,
                    mt: 'api',
                    message: {
                        url: request.url,
                        csf: new Date().getTime(),
                        cet: new Date().getTime(),
                        cft: new Date().getTime(),
                        rc: error?.response?.status,
                        err: error?.response?.data,
                    },
                },
            ],
        }

        const realRequest: GetRequest = {
            method: 'post',
            url: 'http://test-api.oval.lan/pub/report/log',
            data: JSON.stringify(logData),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        axios(realRequest)
    }

    return {
        data: response && response.data,
        response,
        error,
        isValidating,
        revalidate,
        mutate,
    }
}

export function usePost(request: GetRequest): Promise<any> {
    const lang = ''
    const token = getTokenCookie(null) || null
    const uuid = getUUIDCookie(null) || ''
    const _channel = (typeof window !== 'undefined' && localStorage.Channel) || ''

    const realRequest: GetRequest = {
        ...request,
        headers: {
            Authorization: token || null,
            'oval-language': lang || 'en',
            Uuid: uuid || '',
            ...request.headers,
        },
        data: request?.data && qs.stringify(request.data),
    }

    if (_channel) {
        realRequest['headers']['Channel'] = _channel
    }

    return axios(realRequest)
}

export function useNoneStringPost(request: GetRequest): Promise<any> {
    const lang = ''
    const token = getTokenCookie(null) || null
    const uuid = getUUIDCookie(null) || ''
    const _channel = (typeof window !== 'undefined' && localStorage.Channel) || ''
    let data = null
    if (request.method === 'PUT' || request.method === 'put') {
        data = request.data
    } else {
        data = JSON.stringify(request?.data)
    }
    const realRequest: GetRequest = {
        ...request,
        headers: {
            Authorization: token || null,
            'FT-language': lang || 'en',
            Uuid: uuid || '',
            ...request.headers,
        },
        data: data,
    }

    if (_channel) {
        realRequest['headers']['Channel'] = _channel
    }

    return axios(realRequest)
}

export function useGet(url: any): Promise<any> {
    return axios.get(url)
}

interface SimpleRequestProps {
    url: string
    type: 'get' | 'post' | 'put'
    data: object
    userToken?: string
    language: string
    headers?: any
}

export function useSimplePost({url, type, data, language, userToken, headers}: SimpleRequestProps): Promise<any> {
    return new Promise<any>((resolve, reject) => {
        usePost({
            url: type === 'get' ? `${url}?${qs.stringify(data)}` : url,
            method: type,
            data: type === 'get' ? '' : data,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                Authorization: userToken || null,
                'FT-language': language === 'en' ? 'en-US' : language,
                ...headers,
            },
        }).then((res) => {
            if (res && res.status === 200) {
                if (res.data && res.data.errno === 0) {
                    resolve(res.data)
                } else {
                    reject(res)
                }
            } else {
                reject(res)
            }
        })
    })
}

export function useUploadPost({url, data, language, userToken, headers}: SimpleRequestProps): Promise<any> {
    return new Promise<any>((resolve, reject) => {
        useNoneStringPost({
            url: url,
            method: 'put',
            data,
            headers: {
                // 'Content-Type': 'm',
                Authorization: userToken || null,
                'FT-language': language,
                ...headers,
            },
        })
            .then((res) => {
                if (res && res.status === 200) {
                    resolve({errno: 0})
                } else {
                    reject(res)
                }
            })
            .catch((err) => {
                reject(err)
            })
    })
}
