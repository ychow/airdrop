import {useEffect, useState} from 'react'

const useDebounce = (val, delay = 500) => {
    const [debounceVal, setDebounceVal] = useState(val)

    useEffect(() => {
        const timer = setTimeout(() => {
            setDebounceVal(val)
        }, delay)

        return () => {
            clearTimeout(timer)
        }
    }, [val, delay])

    return debounceVal
}

export default useDebounce
