import {useRequest} from '../useRequest'

interface KlineSymbol {
    amount: number
    close: number
    high: number
    low: number
    open: number
    symbol: string
    time: number
    type: number
    volume: number
}

interface KlineDataProps {
    data: KlineSymbol[]
    errmsg: string
    errno: number
}

const klineAPI = '/x/pub/api/v1/hq/kline'

export const useKlineData = (data, swrConfig = {}) => {
    return useRequest<KlineDataProps>(
        data ? klineAPI : null,
        {
            url: klineAPI,
            method: 'get',
            params: data,
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}
