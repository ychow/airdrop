import {useRequest} from '../useRequest'

interface TypeObj {
    name: string
    value: string
}
interface BuzTypeRes {
    data: TypeObj[]
}
interface CsrfTokenType {
    csrf_token: string
}
interface CsrfRes {
    data: CsrfTokenType
}

const getBuzTypes = '/x/pub/api/v1/base/zd/biz_types'
const getCsrfToken = '/x/pub/api/v1/base/zd/ticket/csrf_token'

export const useGetType = (swrConfig = {}) => {
    return useRequest<BuzTypeRes>(
        getBuzTypes,
        {
            url: getBuzTypes,
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useGetCsrf = (swrConfig = {}) => {
    return useRequest<CsrfRes>(
        getCsrfToken,
        {
            url: getCsrfToken,
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}
