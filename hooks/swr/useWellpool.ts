import {useRequest} from '../useRequest'

export interface EventProps {
    id: number
    title: string
    desc: string
    content_url: string
    icon_url: string
    image_url: string
    end_at: number
    deposit_start_at: number
    deposit_volume: number
    min_deposit_volume: number
    deposit_asset_code: string
    deposit_asset_name: string
    award_asset_code: string
    award_asset_name: string
    award_volume: number
}
interface EventListProps {
    data?: {
        doing_list: EventProps[]
        over_list: EventProps[]
    }
}

export interface DetailProps {
    id: number
    title: string
    desc: string
    content_url: string
    icon_url: string
    image_url: string
    end_at: number
    deposit_start_at: number
    deposit_volume: number
    min_deposit_volume: number
    deposit_asset_code: string
    deposit_asset_name: string
    award_asset_code: string
    award_asset_name: string
    award_volume: number
    now_unix_time: number
}

interface DetailResProps {
    data: DetailProps
}

interface UserStatProps {
    activity_id: number
    deposit_total: number
    refund: number
    award_total: number
}

interface UserStatResProps {
    data: UserStatProps
}

interface EventHistoryProps {
    id?: number
    user_id?: number
    asset_code?: string
    volume?: number
    transaction_type?: number
    created_at?: string
}

export interface EventHistoryResProps {
    data: {
        items: EventHistoryProps[]
        page: {
            page_index: number
            total: number
            page_size: number
        }
    }
}

export enum PoolTableApis {
    DEPOSIT = '/x/api/v1/pool/bonus/my_orders',
    REWARD = '/x/api/v1/pool/bonus/my_rewards',
}

export const useEventHistory = (url, data = {}, swrConfig = {}) => {
    return useRequest<EventHistoryResProps>(
        url,
        {
            url: url,
            method: 'get',
            params: data,
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useEventList = (swrConfig = {}) => {
    return useRequest<EventListProps>(
        '/x/pub/api/v1/pool/bonus/activity_list',
        {
            url: '/x/pub/api/v1/pool/bonus/activity_list',
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useEventDetail = (data = {id: ''}, swrConfig = {}) => {
    return useRequest<DetailResProps>(
        data?.id ? '/x/pub/api/v1/pool/bonus/activity_detail' : null,
        {
            url: '/x/pub/api/v1/pool/bonus/activity_detail',
            method: 'get',
            params: {...data, _t: new Date().getTime()},
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useUserStat = (data = {activity_id: ''}, swrConfig = {}) => {
    return useRequest<UserStatResProps>(
        data?.activity_id ? '/x/api/v1/pool/bonus/my_order_summary' : null,
        {
            url: '/x/api/v1/pool/bonus/my_order_summary',
            method: 'get',
            params: {...data, _t: new Date().getTime()},
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}
