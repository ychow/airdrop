import {useRequest} from '../useRequest'

interface WellUpdateObj {
    announce_issued_amount?: number
    circulate_amount?: number
    destroyed_amount?: number
    destroyed_percentage?: number
    lock_amount?: number
    lock_percentage?: number
}

interface WeixinObj {
    android_apk?: string
    email?: string
    tg_cn?: string
    tg_en?: string
    weixin_id?: string
    weixin_url?: string
    download_link?: {
        link: string
        pic: string
    }
}

export interface FinanceObj {
    pair: string
    rate: string
    symbol: string
    name: string
}

interface WellUpdateRes {
    data: WellUpdateObj
}

interface WeixinRes {
    data: WeixinObj
}

interface FinanceRes {
    data: FinanceObj[]
}

interface PalConfig {
    data: {
        bullbear: boolean
        region_code?: string
        otc_guide?: string
        otc_precautions?: string
        support?: string
        support_web?: string
        voice_support?: boolean
    }
}

const getWellUpdate = '/x/pub/api/v1/collateral/well_updates'
const getWeixin = '/x/pub/api/v1/common/index/weixin'
const getFinanceRate = '/x/pub/api/v1/common/get_currency'
const getPalConfig = '/x/pub/api/v1/common/config/info'

export const useWellUpdate = (swrConfig = {}) => {
    return useRequest<WellUpdateRes>(
        getWellUpdate,
        {
            url: getWellUpdate,
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useWeixin = (swrConfig = {}) => {
    return useRequest<WeixinRes>(
        getWeixin,
        {
            url: getWeixin,
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useFinanceRate = (swrConfig = {}) => {
    return useRequest<FinanceRes>(
        getFinanceRate,
        {
            url: getFinanceRate,
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useGlobalConfig = (swrConfig = {}) => {
    return useRequest<PalConfig>(
        getPalConfig,
        {
            url: getPalConfig,
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}
