import {useRequest} from '../useRequest'

export interface SymbolObj {
    tag: number
    code: string
    name: string
    quote_asset: string
    strike_price: number
    volume_precision: number
    price_precision: number
    base_asset: string
    amount_precision: number
    quote: object
    underlying_asset: any
    multiple: any
}

export interface MakerPalSymbolObj {
    base_asset: string
    code: string
    duration: number
    index_code: string
    min_volume: number
    name: string
    price_limit_rate: {
        buy_down: number
        sell_up: number
        buy_up: number
        sell_down: number
    }
    price_precision: number
    quote_asset: string
    volume_precision: number
}

interface SpotGroupObj {
    big_group: string
    category: number
    group: string
}

interface SpotTypeObj {
    big_group: string
    category: number
    group: string
    items: SymbolObj[]
}

interface SpotTypeList extends SpotGroupObj {
    items: SpotTypeObj[]
}

interface SpotTypeListProps {
    data: {
        spot: SpotTypeList[]
    }
}

interface SpotConditionProps {
    data: {
        amount_precision: number
        base_asset: string
        base_available: number
        market_freeze_rate: number
        max_volume: number
        min_amount: number
        min_volume: number
        price_precision: number
        quote_asset: string
        quote_available: number
        volume_precision: number
    }
}

interface MakerPalConditionProps {
    data: {
        base_asset: string
        base_available: string | number
        interest_rate: string | number
        price_precision: number
        quote_asset: string
        quote_available: string | number
        volume_precision: number
    }
}

interface SpotAllListProps {
    data: SymbolObj[]
}

interface MakerPalAllListProps {
    data: MakerPalSymbolObj[]
}

interface SpotPendingItem {
    asset?: string
    available?: number
    amount: number
    code: string
    direction: string
    name: string
    oid: string
    open_mode: number
    order_time: number
    order_type: number
    price: number
    price_type: string
    queue_amount: number
    queue_price: number
    queue_volume: number
    quote_asset: string
    remain_type: string
    state: number
    symbol: string
    symbol_id: number
    tri_direction: string
    tri_price: number
    volume: number
}

interface OrderDataProps {
    items?: SpotPendingItem[]
    data?: SpotPendingItem[]
    page: {
        prev: string
        next: string
    }
    total?: number
}

export interface BalanceData {
    asset: string
    asset_name: string
    available: number
    btc_total: number
    covered_frozen: number
    deposit_enable: true
    frozen: number
    icon: string
    locked: number
    margin_frozen: number
    market: string
    package_power: number
    precision: number
    price_code: string
    total: number
}

interface AllBalanceResProps {
    data: BalanceData[]
    errno?: number
}

interface SpotOrderProps {
    data: OrderDataProps
    errno?: number
}

export const DOMAIN_LIST = {
    local: 'http://localhost:5000',
    prod: 'https://oval.exchange',
    beta: 'https://oval.exchange',
    test: 'https://test.oval.exchange',
}
export const DEFAULT_DOMAIN = DOMAIN_LIST?.[process.env.NEXT_PUBLIC_APP_ENV] || DOMAIN_LIST.prod

export enum SpotTabApiProps {
    PENDING = '/x/api/v1/order/pending',
    FILLED = '/x/api/v1/order/filled',
    MATCHES = '/x/api/v1/order/matches',
    ASSETS = '/x/api/v1/asset/all_balance',
    PAL_PENDING = '/x/api/v1/wellmaker/order/pending',
    PAL_FILLED = '/x/api/v1/wellmaker/order/filled',
}

const getSpotList = '/x/pub/api/v1/symbol/all'
const getMakerPalList = '/x/pub/api/v1/wellmaker/symbol/all'
const getSpotWithType = '/x/pub/api/v1/symbol/all_category_new?market=spot'
const getSpotCondition = '/x/api/v1/order/condition?market=spot'
const getMakerPalCondition = '/x/api/v1/wellmaker/order/condition'
const getSwapHoldTotal = '/x/api/v1/swap/get_ops_num'
const getSwapPendingTotal = '/x/api/v1/order/pending_num'
export const getSwapList = '/x/pub/api/v1/symbol/all?market=swap'

export const useAllBalance = (swrConfig = {}) => {
    return useRequest<AllBalanceResProps>(
        SpotTabApiProps.ASSETS,
        {
            url: SpotTabApiProps.ASSETS,
            method: 'get',
            params: {market: 'spot'},
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const usePendingTotalNum = (swrConfig = {}) => {
    return useRequest<SpotOrderProps>(
        `${SpotTabApiProps.PENDING}_total`,
        {
            url: SpotTabApiProps.PENDING,
            method: 'get',
            params: {market: 'spot', _t: new Date().getTime(), use: 'pending_num'},
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useSpotList = (swrConfig = {}) => {
    return useRequest<SpotAllListProps>(
        getSpotList,
        {
            url: getSpotList,
            method: 'get',
            params: {market: 'spot'},
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useMakerPalList = (swrConfig = {}) => {
    return useRequest<MakerPalAllListProps>(
        getMakerPalList,
        {
            url: getMakerPalList,
            method: 'get',
            params: {},
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useSpotTypeList = (swrConfig = {}) => {
    return useRequest<SpotTypeListProps>(
        getSpotWithType,
        {
            url: getSpotWithType,
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const userSpotCondition = (data, swrConfig = {}) => {
    return useRequest<SpotConditionProps>(
        data ? `${getSpotCondition}_${data.symbol}` : null,
        {
            url: getSpotCondition,
            method: 'get',
            params: data,
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useMakerPalCondition = (data, swrConfig = {}) => {
    return useRequest<MakerPalConditionProps>(
        data ? `${getMakerPalCondition}_${data.symbol}` : null,
        {
            url: getMakerPalCondition,
            method: 'get',
            params: data,
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useSpotOrder = (url, data = {}, swrConfig = {}) => {
    return useRequest<SpotOrderProps>(
        url,
        {
            url: url,
            method: 'get',
            params: {...data, _t: new Date().getTime()},
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useHoldSwapTotal = (isGet, swrConfig = {}) => {
    return useRequest<SpotOrderProps>(
        isGet ? getSwapHoldTotal : null,
        {
            url: getSwapHoldTotal,
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const usePendingSwapTotal = (isGet, swrConfig = {}) => {
    return useRequest<SpotOrderProps>(
        isGet ? getSwapPendingTotal : null,
        {
            url: getSwapPendingTotal,
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

// export const useMakerPalOrder = (url, data = {}, swrConfig = {})  => {
//     return useRequest<SpotOrderProps>(
//         url,
//         {
//             url: url,
//             method: 'get',
//             params: data,
//         },
//         {revalidateOnFocus: false, ...swrConfig}
//     )
// }
