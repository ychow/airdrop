import {useRequest} from '../useRequest'

interface NewsObj {
    created_at: string
    edited_at: string
    html_url: string
    title: string
    url: string
}
interface NewsListRes {
    data: NewsObj[]
}

interface BannerObj {
    appLink: string
    appPic: string
    appType: string
    endTime: string
    info: string
    lang: string
    largePic: string
    link: string
    pic: string
    priority: string
    startTime: string
    status: string
    title: string
    type: string
}
interface BannerListRes {
    data: BannerObj[]
}

const getNewsList = '/x/pub/api/v1/common/report/articles?auth_page=1'
const getBannerList = '/x/pub/api/v1/common/index/banner'

export const useNewsList = (swrConfig = {}) => {
    return useRequest<NewsListRes>(
        getNewsList,
        {
            url: getNewsList,
            method: 'get',
            params: {_t: new Date().getTime()},
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useBannerList = (swrConfig = {}) => {
    return useRequest<BannerListRes>(
        getBannerList,
        {
            url: getBannerList,
            method: 'get',
            params: {_t: new Date().getTime()},
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}
