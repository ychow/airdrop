import {useRequest} from '../useRequest'

export interface EventProps {
    id: number
    weight: number
    name: string //活动名称
    annualized_rate: number //预计年化
    lock_day_num: number //锁定天数
    icon: string //币图标
    store_coin: string //存入币种
    reward_coin: string //奖励币种
    state: number //活动状态(1-未开始 2-存币中 3-冻结中 4-已解冻)
    user_min_store: number //用户最小存入
    cpp_max_store: number //活动最大存入
    display: number //是否显示(后台设置是否在前端展示 1展示，2不展示)
    store_volume_count: number //活动目前总存币量
    start_date: string //开始时间
    end_date: string //结束时间
    unfreeze_date: string //解冻时间
    server_timestamp: number //服务器当前时间戳
    start_date_timestamp: number //开始时间时间戳
    end_date_timestamp: number //存币结束时间戳
    unfreeze_date_timestamp: number //解冻时间戳
    user_store_volume_count: number //当前用户存币数
}
interface EventListProps {
    data: {
        items?: EventProps[]
        page: {
            page_index: number
            total: number
            page_size: number
        }
    }
}

export interface UserStatProps {
    items: object
}

interface UserStatResProps {
    data: UserStatProps
}

interface EventHistoryProps {
    id?: number
    user_id?: number
    asset_code?: string
    volume?: number
    transaction_type?: number
    created_at?: string
}

export interface EventHistoryResProps {
    data: {
        items: EventHistoryProps[]
        page: {
            page_index: number
            total: number
            page_size: number
        }
    }
}

export enum SavingsTableApis {
    DEPOSIT = '/x/api/v1/pool/cpp/store_history',
    REWARD = '/x/api/v1/pool/cpp/reward_history',
}

export const useEventHistory = (url, data = {}, swrConfig = {}) => {
    return useRequest<EventHistoryResProps>(
        url,
        {
            url: url,
            method: 'get',
            params: data,
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useEventList = (swrConfig = {}) => {
    return useRequest<EventListProps>(
        '/x/pub/api/v1/pool/cpp/list',
        {
            url: '/x/pub/api/v1/pool/cpp/list',
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useUserStat = (data = {userToken: ''}, swrConfig = {}) => {
    return useRequest<UserStatResProps>(
        data?.userToken ? '/x/api/v1/pool/cpp/store_history_total' : null,
        {
            url: '/x/api/v1/pool/cpp/store_history_total',
            method: 'get',
            params: {_t: new Date().getTime()},
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}
