import {useRequest} from '../useRequest'

export interface EventProps {
    active: number
    asset: string
    base_asset_precision: number
    gain_asset: string
    icon: string
    is_show: boolean
    min_amount: number
    target_asset_precision: number
    type: number
}
interface EventListProps {
    data?: EventProps[]
}

interface EventHistoryProps {
    id?: number
    user_id?: number
    asset_code?: string
    volume?: number
    transaction_type?: number
    created_at?: string
}

export interface EventHistoryResProps {
    data: {
        items: EventHistoryProps[]
        page: {
            page_index: number
            total: number
            page_size: number
        }
    }
}

export enum PoolTableApis {
    DEPOSIT = '/x/api/v1/pool/bonus/my_orders',
    REWARD = '/x/api/v1/pool/bonus/my_rewards',
}

export const useEventHistory = (url, data = {}, swrConfig = {}) => {
    return useRequest<EventHistoryResProps>(
        url,
        {
            url: url,
            method: 'get',
            params: data,
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useEventList = (swrConfig = {}) => {
    return useRequest<EventListProps>(
        '/x/pub/api/v1/pool/deposit/deposit_asset_info',
        {
            url: '/x/pub/api/v1/pool/deposit/deposit_asset_info',
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}
