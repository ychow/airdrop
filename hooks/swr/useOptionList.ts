import {useRequest} from '../useRequest'
import {SymbolObj} from './useSpot'

interface SpotGroupObj {
    big_group: string
    category: number
    group: string
}

interface OptionsTypeObj {
    big_group: string
    category: number
    group: string
    items: SymbolObj[]
}

interface SpotTypeList extends SpotGroupObj {
    items: OptionsTypeObj[]
}

interface OptionsTypeList {
    data: {
        options: SpotTypeList[]
    }
}

// const getSpotList = '/api/pub/api/v1/symbol/all'
const getSpotWithType = '/x/pub/api/v1/symbol/all_category_new?market=options'

// export const useSpotList = (swrConfig = {}) => {
//     return useRequest(getSpotList, {
//         url: getSpotList,
//         method: 'get',
//         params: {market: 'spot'},
//     }, { revalidateOnFocus: false, ...swrConfig})
// }

export const useOptionTypeList = (swrConfig = {}) => {
    return useRequest<OptionsTypeList>(
        getSpotWithType,
        {
            url: getSpotWithType,
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}
