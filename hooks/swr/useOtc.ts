import {useRequest} from '../useRequest'

// export interface SymbolObj {
//     tag: number
//     code: string
//     name: string
//     quote_asset: string
//     strike_price: number
//     volume_precision: number
//     price_precision: number
//     base_asset: string
//     amount_precision: number
//     quote: object
// }

// export interface MakerPalSymbolObj {
//     base_asset: string
//     code: string
//     duration: number
//     index_code: string
//     min_volume: number
//     name: string
//     price_limit_rate: {
//         buy_down: number
//         sell_up: number
//         buy_up: number
//         sell_down: number
//     }
//     price_precision: number
//     quote_asset: string
//     volume_precision: number
// }

// interface SpotGroupObj {
//     big_group: string
//     category: number
//     group: string
// }

// interface SpotTypeObj {
//     big_group: string
//     category: number
//     group: string
//     items: SymbolObj[]
// }

// interface SpotTypeList extends SpotGroupObj {
//     items: SpotTypeObj[]
// }

// interface SpotTypeListProps {
//     data: {
//         spot: SpotTypeList[]
//     }
// }

// interface SpotConditionProps {
//     data: {
//         amount_precision: number
//         base_asset: string
//         base_available: number
//         market_freeze_rate: number
//         max_volume: number
//         min_amount: number
//         min_volume: number
//         price_precision: number
//         quote_asset: string
//         quote_available: number
//         volume_precision: number
//     }
// }

// interface MakerPalConditionProps {
//     data: {
//         base_asset: string
//         base_available: string | number
//         interest_rate: string | number
//         price_precision: number
//         quote_asset: string
//         quote_available: string | number
//         volume_precision: number
//     }
// }

// interface SpotAllListProps {
//     data: SymbolObj[]
// }

// interface MakerPalAllListProps {
//     data: MakerPalSymbolObj[]
// }

export interface OtcOrderItemProps {
    allow_chat?: number
    amount?: string
    appeal_remark?: string
    appeal_type?: number
    chat_room_key?: string
    coin?: string
    created_at?: number
    currency?: string
    dispute_minute?: number
    dispute_surplus?: number
    is_voice_alert?: number
    mobile?: number
    order_id?: string
    order_sn?: string
    pay_info?: {
        alipay_id?: string
        alipay_name?: string
        alipay_url?: string
        card_address?: string
        card_bank?: string
        card_code?: string
        card_user_name?: string
        weixin_id?: string
        weixin_name?: string
        weixin_url?: string
    }
    paytype?: string
    price?: string
    price_type?: number
    state?: string
    timeout_minute?: number
    timeout_surplus?: number
    trade_amount?: string
    trade_price?: string
    trade_volume?: string
    type?: string
    uid?: number
    unfreeze?: number
    username?: string
    voice_alert_minute?: number
    voice_alert_surplus?: number
    volume?: string
}
interface OrderDataProps {
    data?: OtcOrderItemProps[]
    items?: OtcOrderItemProps[]
    page: {
        page_index: number
        page_size: number
        total: number
    }
}

export interface OtcOrderProps {
    data: OrderDataProps
}

interface OtcImInfo {
    sdkAppId: string
    userId: string
    userSig: string
}

interface OtcImDataProps {
    data: OtcImInfo
}

interface PayTypeItemProps {
    alipay_id?: string | null
    alipay_url?: string | null
    card_address?: string | null
    card_bank?: string | null
    card_code?: string | null
    created_at?: string | null
    id?: string | null
    oid?: string | null
    pay_type?: string | null
    uid?: string | null
    updated_at?: string | null
    weixin_id?: string | null
    weixin_url?: string | null
}

interface PayTypeRes {
    data: PayTypeItemProps[]
}

export interface EnterpriseInfoProps {
    buy_price: string
    sell_price: string
    buy_lowest: number
    buy_min_amount: string
    buy_max_amount: string
    sell_min_amount: string
    sell_max_amount: string
    buy_status: number
    sell_status: number
    buy_paytype_list: {
        ALIPAY: boolean
        WEIXIN: boolean
        EBANK: boolean
    }
    sell_paytype_list: {
        ALIPAY: boolean
        WEIXIN: boolean
        EBANK: boolean
    }
    assets_enough: number
    buy_min_volume: string
    buy_max_volume: string
    sell_min_volume: string
    sell_max_volume: string
    coin?: string
}

interface EnterpriseInfoRes {
    data: EnterpriseInfoProps
}

interface UserRecieveProps {
    paytype_list: {
        ALIPAY: boolean
        WEIXIN: boolean
        EBANK: boolean
    }
    user_paytype_list: {
        ALIPAY: boolean
        WEIXIN: boolean
        EBANK: boolean
    }
}

interface UserRecieveRes {
    data: UserRecieveProps
}

interface CoinData {
    items: string[]
    currency: string
}

interface CoinListRes {
    data: CoinData
}

export enum OtcTableApis {
    ALL = '/x/api/v1/otc/order/all',
    ONGOING = '/x/api/v1/otc/order/ongoing',
    FINISHED = '/x/api/v1/otc/order/finished',
    CANCELED = '/x/api/v1/otc/order/canceled',
}

// const getSpotList = '/x/pub/api/v1/symbol/all'
// const getMakerPalList = '/x/pub/api/v1/wellmaker/symbol/all'
// const getSpotWithType = '/x/pub/api/v1/symbol/all_category_new?market=spot'
// const getSpotCondition = '/x/api/v1/order/condition?market=spot'
// const getMakerPalCondition = '/x/api/v1/wellmaker/order/condition'
export const useOtcOrders = (url, data = {}, swrConfig = {}) => {
    return useRequest<OtcOrderProps>(
        url,
        {
            url: url,
            method: 'get',
            params: data,
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useImInfo = (swrConfig = {}) => {
    return useRequest<OtcImDataProps>(
        '/x/api/v1/otc/message/im',
        {
            url: '/x/api/v1/otc/message/im',
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const usePayType = (swrConfig = {}) => {
    return useRequest<PayTypeRes>(
        '/x/api/v1/otc/user/get_pay',
        {
            url: '/x/api/v1/otc/user/get_pay',
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useEnterpriseInfo = (data = {coin: '', currency: ''}, swrConfig = {}) => {
    return useRequest<EnterpriseInfoRes>(
        data.coin && data.currency ? '/x/pub/api/v1/otc/enterprise/info' : null,
        {
            url: '/x/pub/api/v1/otc/enterprise/info',
            method: 'get',
            params: data,
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useUserRecieve = (data = {coin: ''}, swrConfig = {}) => {
    return useRequest<UserRecieveRes>(
        data?.coin ? '/x/api/v1/otc/user/paytype' : null,
        {
            url: '/x/api/v1/otc/user/paytype',
            method: 'get',
            params: {...data, _t: new Date().getTime()},
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useCoinList = (swrConfig = {}) => {
    return useRequest<CoinListRes>(
        '/x/pub/api/v1/otc/coin/list',
        {
            url: '/x/pub/api/v1/otc/coin/list',
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}
