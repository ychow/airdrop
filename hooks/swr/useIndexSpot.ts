import {useRequest} from '../useRequest'

interface QuoteObj {
    amount_24: number
    high_24: number
    highest_bid: number
    leverage: number
    low_24: number
    lowest_ask: number
    prev_24: number
    sn: number
    state: number
    subtype: string
    symbol: string
    time: number
    trade: number
    type: string
    volume_24: number
}
interface ListObj {
    code: string
    cycle_length: number
    cycle_type: number
    icon: string
    id: number
    index_code: string
    market: number
    name: string
    quote: QuoteObj
    type: string
    underlying_asset?: string
    base_asset?: string
    multiple?: number
}
interface SpotListRes {
    data: ListObj[]
}

export enum SwapTabApiProps {
    OPS = '/x/api/v1/swap/get_ops',
    PENDING = '/x/api/v1/order/pending',
    HISTORY = '/x/api/v1/order/filled',
}

const getIndexSpotList = '/x/pub/api/v1/symbol/index_list?market=spot'
const getSwapList = '/x/pub/api/v1/symbol/all?market=swap'

export const useIndexSpotList = (swrConfig = {}) => {
    return useRequest<SpotListRes>(
        getIndexSpotList,
        {
            url: getIndexSpotList,
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useGetSwapList = (swrConfig = {}) => {
    return useRequest<SpotListRes>(
        getSwapList,
        {
            url: getSwapList,
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}
