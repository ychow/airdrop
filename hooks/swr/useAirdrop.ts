import {useRequest} from '../useRequest'

interface EventObj {
    id?: number
    name?: string
    asset?: string
    asset_name?: string
    icon?: string
    amount?: string
    get_amount?: string
    start_time?: string
    end_time?: string
    join_people?: string
    join_num?: string
    max_gain?: number
    time_unix?: number
}
interface EventRes {
    data: EventObj
}
interface ClaimResObj {
    gain_amount: number
}
interface ClaimRes {
    data: ClaimResObj
}

const getEvent = '/x/pub/api/v1/base/airdrop/get_config?id=2'
const getClaim = '/api/v1/base/airdrop/gain'

export const useGetEvent = (swrConfig = {}) => {
    return useRequest<EventRes>(
        getEvent,
        {
            url: getEvent,
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useClaimEvent = (swrConfig = {}) => {
    return useRequest<ClaimRes>(
        getClaim,
        {
            url: getClaim,
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}
