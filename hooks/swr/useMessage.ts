import {useRequest} from '../useRequest'

interface MessageObj {
    content: string
    id: number
    sender: string
    title: string
    url: string
    send_time: number
}

interface MessageDataProps {
    items: MessageObj[]
    page: {
        total: number
        page_size: number
    }
}

const getUnReadMessages = '/x/api/v1/common/notify/unread'

export const useUnReadMessages = (swrConfig = {}) => {
    return useRequest<MessageDataProps>(
        getUnReadMessages,
        {
            url: getUnReadMessages,
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}
