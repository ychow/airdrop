import {useRequest} from '../useRequest'

export interface PerpSymbolProps {
    amount_precision?: number
    base_asset?: string
    code?: string
    cycle_length?: number
    cycle_type?: number
    default_price_precision?: string
    direction?: number
    expiration_date?: number
    id?: number
    index_code?: string
    multiple?: number
    name?: string
    price_precision?: number
    quote?: {
        amount_24?: number
        high_24?: number
        highest_bid?: number
        leverage?: number
        low_24?: number
        lowest_ask?: number
        prev_24?: number
        sn?: number
        state?: number
        subtype?: string
        symbol?: string
        time?: number
        trade?: number
        type?: string
        volume_24?: number
    }
    quote_asset?: string
    settlement_cycle_seconds?: number
    settlement_next_time?: number
    state?: string
    strike_price?: number
    tag?: number
    type?: string
    underlying_asset?: string
    underlying_asset_id?: number
    underlying_name?: string
    volume_precision?: number
}

interface PerpSymbolRes {
    data: PerpSymbolProps[]
}

export interface OpenDataProps {
    available_balance?: number
    available_state?: number
    default_leverage?: number
    supposed_margin_volume?: number
    supposed_open_volume?: number
    underlying_asset_unit?: number
}

interface OpenDataResProps {
    data: OpenDataProps
    errno?: number
}

export interface ConfigProps {
    deposit_address: string
    leverage: number
    default_swap: string
    rpc_config: {
        chainId: string
        rpcUrls: string[]
        chainName: string
        nativeCurrency: {
            name: string
            decimals: number
            symbol: string
        }
    }
}
interface ConfigResProps {
    data: ConfigProps
}

export interface SwapOpsProps {
    add_pos_available?: boolean
    avg_open_price?: number
    base_asset?: string
    closing?: boolean
    code?: string
    cross_liqu?: number
    cross_mark_price?: number
    cross_type?: number
    frozen_volume?: number
    id?: number
    leverage?: number
    liquidate_price?: number
    loss_list?: []
    maintenance_margin?: number
    margin_asset_id?: number
    margin_mode?: number
    margin_rate?: number
    max_draw_amount?: number
    open_margin?: number
    open_mode?: number
    open_profit?: number
    open_volume?: number
    position_margin?: number
    position_margin_detail?: null
    profit?: number
    profit_list?: []
    profit_rate?: number
    s_id?: string
    symbol_id?: number
    uid?: number
    underlying_asset?: string
    user_avg_open_price?: number
}
export interface SwapOpsResProps {
    data: SwapOpsProps[]
}

export interface ChainProps {
    code?: string
    display_code?: string
    fee_amount: string
    id?: number
    info?: string
    need_memo?: boolean
    precision?: number
}
interface WithdrawConditionProps {
    auth_required: string[]
    chains: ChainProps[]
    day_max_amount: number
    day_next_amount: number
    day_remain_amount: number
    disabled_reason: number
    disabled_reason_msg: string
    enabled: boolean
    max_amount: number
    memo_required: boolean
    min_amount: number
    trade_pwd_verified: boolean
}
interface WithdrawConditionResProps {
    data: WithdrawConditionProps
}

interface SwapAccountProps {
    account_equity: number
}
interface SwapAccountResProps {
    data: SwapAccountProps
}

export const ALL_SYMBOL_API = '/x/pub/api/v1/symbol/all?market=swap'
export const OPEN_DATA_API = '/x/api/v1/order/open_data'
export const ORDER_PLACE_API = '/x/api/v1/order/swap_place'
export const CLOSE_PLACE_API = '/x/api/v1/order/place'
export const OPS_DATA_API = '/x/api/v1/swap/get_ops'
export const WITHDRAW_CONDITOIN_API = '/x/api/v1/asset/withdraw_condition'
const SWAP_ACCOUNT = '/x/api/v1/swap/account'

export const WITHDRAW_API = '/x/api/v1/dex/withdraw'

export const GLOBAL_CONFIG_API = '/x/pub/api/v2/user/cfg/dex'

export const STATIC_SYMBOL = 'BTCUSDT_SWAP'
export const STATIC_LEVERAGE = 50

export const useAllPerpSymbol = (swrConfig = {}) => {
    return useRequest<PerpSymbolRes>(
        ALL_SYMBOL_API,
        {
            url: ALL_SYMBOL_API,
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

// balance, avail open volume
export const useOpenData = (url = OPEN_DATA_API, data, swrConfig = {}) => {
    return useRequest<OpenDataResProps>(
        url,
        {
            url,
            method: 'get',
            params: data,
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useGlobalConfig = (swrConfig = {}) => {
    return useRequest<ConfigResProps>(
        GLOBAL_CONFIG_API,
        {
            url: GLOBAL_CONFIG_API,
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useSwapAccount = (isGet, swrConfig = {}) => {
    return useRequest<SwapAccountResProps>(
        isGet ? SWAP_ACCOUNT : null,
        {
            url: SWAP_ACCOUNT,
            method: 'get',
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useOpsData = (url = OPS_DATA_API, data = {num: 100}, swrConfig = {}) => {
    return useRequest<SwapOpsResProps>(
        url,
        {
            url,
            method: 'get',
            params: {...data, _t: new Date().getTime()},
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useWithdrawCondition = (url = WITHDRAW_CONDITOIN_API, asset = 'USDT', swrConfig = {}) => {
    return useRequest<WithdrawConditionResProps>(
        url,
        {
            url,
            method: 'get',
            params: {asset},
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}
