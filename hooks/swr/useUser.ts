import {useRequest, ResponseData} from 'hooks/useRequest'

interface UserRes extends ResponseData {
    data: {
        roles?: number[]
        mobile?: string
        email?: string
        id?: number
        oid?: number
        chain_address?: string
        auth?: {
            identity_verified: boolean
            mobile?: boolean
            trade_pwd?: boolean
        }
        region_code?: string
    }
}

interface UserCountryCode {
    data: {
        region_code: string
    }
}

interface UserProfileProps {
    country: string
    created_at: string
    deny_reason: string
    idcard_type: string
    pic1: string
    pic2: string
    pic3: string
    realname: string
    region_code: string
}

interface SwapRewardRes {
    data: string
    errmsg: string
    errno: number
}

interface UserProfileRes {
    data: UserProfileProps
}
interface UserSwapRewardRes {
    data: SwapRewardRes
}

export const isLogin = '/x/api/v1/user/is_login'
export const getUserCountry = '/x/pub/api/v1/common/get_country_code'
const getUserInfo = '/x/api/v1/user/info'
const getUserProfile = '/x/api/v1/user/get_user_profile'
const signRequest = '/x/pub/api/v2/user/wallet/connect'
const swapRewardTotal = '/x/api/v1/swap/match_amount'

export const useSignRequest = (data, swrConfig) => {
    return useRequest<UserRes>(
        signRequest,
        {
            url: signRequest,
            method: 'post',
            params: data,
        },
        {revalidateOnFocus: false, ...swrConfig}
    )
}

export const useIsLogin = (token) => {
    return useRequest<UserRes>(token ? isLogin : null, {
        url: isLogin,
        method: 'post',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
    })
}

export const useGetCountry = () => {
    return useRequest<UserCountryCode>(
        getUserCountry,
        {
            url: getUserCountry,
            method: 'get',
        },
        {revalidateOnFocus: false}
    )
}

export const useUserInfo = (token) => {
    return useRequest<UserRes>(
        token ? getUserInfo : null,
        {
            url: getUserInfo,
            method: 'get',
        },
        {revalidateOnFocus: false}
    )
}

export const useUserProfile = (token) => {
    return useRequest<UserProfileRes>(
        token ? getUserProfile : null,
        {
            url: getUserProfile,
            method: 'get',
        },
        {revalidateOnFocus: false}
    )
}

export const useSwapReward = (isGet) => {
    return useRequest<UserSwapRewardRes>(
        isGet ? swapRewardTotal : null,
        {
            url: swapRewardTotal,
            method: 'get',
        },
        {revalidateOnFocus: false}
    )
}
