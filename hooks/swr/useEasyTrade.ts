import {useRequest} from '../useRequest'

interface OptionsData {
    name: string
    code: string
    amount_precision: number
    base_asset: string
    index_code?: string
    multiple?: number
    price_precision?: number
    strike_price?: number
    volume_precision?: number
    expiration_date?: number
}

interface OptionsOrderProps {
    name: string
    code: string
    expiration_date: number
    oid: string
    open_mode?: number
    price_type?: string
    order_type?: number
    type?: number
    queue_volume?: number
    open_volume?: number
    frozen_volume?: number
    net_inflow?: number
    avg_cost?: number
    queue_price?: number
}

export interface GroupData {
    group: string
    items: OptionsData[]
}

interface IndexSymbolProps {
    data: OptionsData[]
}

interface OptionsListProps {
    data: GroupData[]
}

interface OptionsOptProps {
    data: {
        items: OptionsOrderProps[]
    }
}

interface OptionConditionProps {
    data: {
        open_fee: number
        amount_precision: number
        base_asset: string
        base_available: number
        open_volume: number
        user_level_auth: {
            close_buy: number
            close_covered_sell: number
            close_margin_sell: number
            open_buy: number
            open_covered_sell: number
            open_margin_sell: number
        }
    }
}

interface CancelResProps {
    errmsg: string
    errno: number
}

const getOptionsList = '/x/pub/api/v1/symbol/options'
const getOptionsOptData = '/x/api/v1/options/get_opt'
const getOptionsCondition = '/x/api/v1/order/condition?market=options'
const getOptionsPendingData = '/x/api/v1/order/pending?close=2&market=options'
const cancelOptionsOrder = '/x/api/v1/order/cancel'
const submitOptions = '/x/api/v1/order/place'
const getIndexSymbol = '/x/pub/api/v1/symbol/all?market=index'

export const useOptionsData = () => {
    return useRequest<OptionsListProps>(
        getOptionsList,
        {
            url: getOptionsList,
            method: 'get',
            params: {market: 'options'},
        },
        {revalidateOnFocus: false}
    )
}

export const useIndexSymbol = () => {
    return useRequest<IndexSymbolProps>(
        getIndexSymbol,
        {
            url: getIndexSymbol,
            method: 'get',
        },
        {revalidateOnFocus: false}
    )
}

export const useOptionsOptData = ({data}) => {
    return useRequest<OptionsOptProps>(
        data ? getOptionsOptData : null,
        {
            url: getOptionsOptData,
            method: 'get',
            params: data,
        },
        {revalidateOnFocus: false}
    )
}

export const userOptionCondition = (data) => {
    return useRequest<OptionConditionProps>(
        data ? `${getOptionsCondition}&symbol=${data.name}_${data.date}` : null,
        {
            url: data ? `${getOptionsCondition}&symbol=${data.name}` : '',
            method: 'get',
        },
        {revalidateOnFocus: false}
    )
}

export const userOptionsPendingData = (num) => {
    return useRequest<OptionsOptProps>(
        num ? getOptionsPendingData : null,
        {
            url: `${getOptionsPendingData}&num=${num}`,
            method: 'get',
        },
        {revalidateOnFocus: false}
    )
}

export const useOptionsCancel = (oid) => {
    return useRequest<CancelResProps>(oid ? cancelOptionsOrder : null, {
        url: oid ? cancelOptionsOrder : null,
        method: 'post',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: {
            market: 'options',
            oid,
        },
    })
}

export const useSubmitOrder = (data) => {
    return useRequest<CancelResProps>(data ? `${submitOptions}` : null, {
        url: data ? submitOptions : null,
        method: 'post',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        data,
    })
}
