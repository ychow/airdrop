import {useEffect, useReducer} from 'react'

const useForceUpdate = () => useReducer((state) => !state, false)[1]

const createSharedState = (reducer, initialState) => {
    const subscribers = []
    let state = initialState
    const dispatch = (action) => {
        state = reducer(state, action)
        subscribers.forEach((callback) => callback())
    }
    const useSharedState = () => {
        const forceUpdate = useForceUpdate()
        useEffect(() => {
            const callback = () => forceUpdate()
            subscribers.push(callback)
            callback() // in case it's already updated
            const cleanup = () => {
                const index = subscribers.indexOf(callback)
                subscribers.splice(index, 1)
            }
            return cleanup
        }, [])
        return [state, dispatch]
    }
    return useSharedState
}

const initialState = {
    quote: {},
    option: {},
    swap: {},
    depth: {},
    transaction: {},
    auth: null,
    orderChange: null,
    kline: {},
    otcExchangeRate: {},
    quote_spot_stream: {},
    quote_swap_stream: {},
    wellpoolBonus: [],
    oval_quote: {},
}

const reducer = (state, action) => {
    if (action?.key === 'orderChange') {
        return {
            ...state,
            orderChange: action.val,
        }
    }

    let _old = []

    if (action.type === 'transaction') {
        _old = state['transaction'][action.val['symbol']] || []
    }

    switch (action.type) {
        case 'transaction':
            if ((_old[0] && _old[0].time_us) === (action.val['val'][0] && action.val['val'][0].time_us)) {
                return {
                    ...state,
                    [action.key]: {
                        [action.val['symbol']]: [...action.val['val']],
                    },
                }
            }

            return {
                ...state,
                [action.key]: {
                    [action.val['symbol']]: [..._old, ...action.val['val']],
                },
            }
        case 'ws':
            return {
                ...state,
                [action.key]: {
                    ...state[action.key],
                    ...action.val,
                },
            }
        default:
            return state
    }
}

export default createSharedState(reducer, initialState)
