import {useState, useEffect} from 'react'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const MySwal = withReactContent(Swal)

interface AlertProps {
    type: '' | 'success' | 'error'
    message: string
}

export default function useAlert() {
    const [alertData, setAlert] = useState<AlertProps>({
        type: '',
        message: '',
    })

    useEffect(() => {
        if (alertData.type) {
            MySwal.fire({
                html: (
                    <>
                        {alertData.type === 'success' && (
                            <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path d="M4 12.3921L10.238 18L20 5" stroke="#101215" strokeWidth="2" />
                            </svg>
                        )}
                        {alertData.type === 'error' && (
                            <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <circle cx="12" cy="12" r="8" stroke="#101215" strokeWidth="2" />
                                <path d="M12 14.5V16.5" stroke="#101215" strokeWidth="2" />
                                <path d="M12 7.5V13" stroke="#101215" strokeWidth="2" />
                            </svg>
                        )}
                        {alertData.message}
                    </>
                ),
                position: 'top',
                showConfirmButton: false,
                timer: 2500,
                backdrop: false,
                customClass: {
                    container: `toastContainer ${alertData.type === 'success' ? 'success' : 'error'}`,
                },
                showClass: {
                    popup: 'fadeIn',
                    // backdrop: 'swal2-backdrop-show',
                    // icon: 'swal2-icon-show'
                },
                hideClass: {
                    popup: 'swal2-hide fadeOut',
                    backdrop: 'swal2-backdrop-hide',
                    icon: 'swal2-icon-hide',
                },
            })
        }

        // return () => {

        // }
    }, [alertData])

    return [setAlert]
}
