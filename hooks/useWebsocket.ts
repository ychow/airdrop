import {useEffect, useRef, useCallback, useState} from 'react'
import useWebSocket from 'react-use-websocket'

import useWsState from './useWsState'
import {getCurApi} from 'utils'
import {formatWsData} from 'utils/format'

type SubProps = {
    action: string
    symbol: string[]
    type: string
}

type AuthProps = {
    action: string
    type: string
    errmsg: string
    errno: number
}

type OrderChangeProp = {
    arr: string[]
}

export type KlineProps = {
    close: number
    high: number
    low: number
    open: number
    subtype: string
    symbol: string
    time: number
    volume: number
}

interface SendArrProps {
    quote?: SubProps
    orderBook?: SubProps
    transaction?: SubProps
    swap?: SubProps
    option?: SubProps
    lock_amount?: SubProps
    auth?: AuthProps
    orderChange?: OrderChangeProp
    kline?: KlineProps
}

interface SubOptionsProps {
    unsubPrev: boolean
    key:
        | 'quote'
        | 'orderBook'
        | 'transaction'
        | 'swap'
        | 'option'
        | 'lock_amount'
        | 'order'
        | 'kline'
        | 'pos_order'
        | 'airdrop'
        | 'quote_spot_stream'
        | 'quote_swap_stream'
}

let prevTime = 0

const parseWsType = (data: string) => {
    if (data && typeof data.indexOf === 'function' && data.indexOf('-quote=') > -1) {
        // quote
        const _sArr = data.split('-quote=')
        const _wsData = JSON.parse(_sArr[1])
        const {prev_24, trade} = _wsData
        _wsData.chg = prev_24 === 0 ? '-' : (trade - prev_24) / prev_24

        return {
            key: 'quote',
            data: {
                [_sArr[0] || '']: _wsData,
            },
        }
    } else if (data && data.indexOf('-option=') > -1) {
        // quote
        const _sArr = data.split('-option=')
        const _wsData = JSON.parse(_sArr[1])

        return {
            key: 'option',
            data: {
                [_sArr[0] || '']: _wsData,
            },
        }
    } else if (data && data.indexOf('-depth') > -1) {
        // quote
        const _sArr = data.split('=')
        const _wsData = JSON.parse(_sArr[1])
        const {symbol, subtype} = _wsData

        return {
            key: 'depth',
            data: {
                [symbol]: {
                    [subtype]: _wsData,
                },
            },
        }
    } else if (data && data.indexOf('-transaction=') > -1) {
        // quote
        const _sArr = data.split('-transaction=')
        const _wsData = JSON.parse(_sArr[1])
        const {symbol} = _wsData[0] || {}

        return {
            key: 'transaction',
            data: {
                symbol,
                val: _wsData,
            },
        }
    } else if (data && data.indexOf('-lock_amount=') > -1) {
        const _sArr = data.split('-lock_amount=')
        const _wsData = JSON.parse(_sArr[1])

        return {
            key: 'lock_amount',
            data: _wsData,
        }
    } else if (data && data.indexOf('"action":"auth"') > -1) {
        const _wsData = JSON.parse(data)

        if (_wsData.errno === 0) {
            return {
                key: 'auth',
                data: _wsData,
            }
        } else {
            return {
                key: 'auth',
                adata: null,
            }
        }
    } else if (data && data.indexOf('order_change_symbols') > -1) {
        const _sArr = data.split('-order_change_symbols=')
        const _wsData = JSON.parse(_sArr[1])

        return {
            key: 'orderChange',
            data: _wsData,
        }
    } else if (data && data.indexOf('pos_change_symbols') > -1) {
        const _sArr = data.split('-pos_change_symbols=')
        const _wsData = JSON.parse(_sArr[1])

        return {
            key: 'orderChange',
            data: _wsData,
        }
    } else if (data && data.indexOf('-kline-') > -1) {
        const _sArr = data.split('=')
        const _wsData = JSON.parse(_sArr[1])

        return {
            key: 'kline',
            data: {
                [`${_wsData.symbol}_${_wsData.subtype}`]: _wsData,
            },
        }
    } else if (data && data.indexOf('-volume24=') > -1) {
        const _sArr = data.split('-volume24=')
        const _wsData = JSON.parse(_sArr[1])

        return {
            key: 'volume24',
            data: {
                [_sArr[0] || '']: _wsData,
            },
        }
    } else if (data && data.indexOf('-activity_1=') > -1) {
        const _sArr = data.split('=')
        const _wsData = JSON.parse(_sArr[1])

        return {
            key: 'airdrop',
            data: _wsData,
        }
    } else if (data && data.indexOf('-otc-coin-price=') > -1) {
        const _sArr = data.split('=')
        const _wsData = JSON.parse(_sArr[1])
        return {
            key: 'otcExchangeRate',
            data: _wsData,
        }
    } else if (data && data.indexOf('-quote_spot_stream=') > -1) {
        // quote
        const _sArr = data.split('-quote_spot_stream=')
        const _wsData = JSON.parse(_sArr[1])
        const _data = _wsData.quotes
        const _rData = {}

        _data &&
            _data.length &&
            _data.map((item) => {
                item.chg = item.p === 0 ? '-' : (item.t - item.p) / item.p
                _rData[item.s] = {
                    chg: item.chg,
                    trade: item.t,
                    code: item.s,
                }
            })

        return {
            key: 'quote_spot_stream',
            data: _rData,
        }
    } else if (data && data.indexOf('-quote_swap_stream=') > -1) {
        // quote
        const _sArr = data.split('-quote_swap_stream=')
        const _wsData = JSON.parse(_sArr[1])

        const _data = _wsData.quotes
        const _rData = {}

        _data &&
            _data.length &&
            _data.map((item) => {
                item.chg = item.p === 0 ? '-' : (item.t - item.p) / item.p
                _rData[item.s] = {
                    chg: item.chg,
                    trade: item.t,
                    code: item.s,
                }
            })

        return {
            key: 'quote_swap_stream',
            data: _rData,
        }
    } else if (data && data.indexOf('-bonus_stat=') > -1) {
        const _sArr = data.split('=')
        const _wsData = JSON.parse(_sArr[1])
        return {
            key: 'wellpoolBonus',
            data: _wsData,
        }
    } else if (data && data.indexOf('CPP_STORE') > -1) {
        const _sArr = data.split('=')
        const _wsData = JSON.parse(_sArr[1])
        return {
            key: 'savingsTotalData',
            data: _wsData,
        }
    } else if (data && data.indexOf('-swap=') > -1) {
        const _sArr = data.split('-swap=')
        const _wsData = JSON.parse(_sArr[1])
        return {
            key: 'swap',
            data: {
                [_sArr[0] || '']: _wsData,
            },
        }
    } else if (data && data.indexOf('cross_liq_price') > -1) {
        const _sArr = data.split('-cross_liq_price=')
        const _wsData = JSON.parse(_sArr[1])

        return {
            key: 'cross_liq_price',
            data: _wsData,
        }
    } else if (data && data.indexOf('oval_quote') > -1) {
        const _sArr = data.split('-oval_quote=')
        const _wsData = JSON.parse(_sArr[1])

        return {
            key: 'oval_quote',
            data: _wsData,
        }
    } else {
        return {}
    }
}

export default () => {
    const [, dispatch] = useWsState()

    const [ifHasFailed, setHasFailed] = useState(false)

    const didUnmount = useRef(false)
    const sendArr = useRef<SendArrProps | null>(null)
    const {sendMessage, lastMessage, readyState, getWebSocket} = useWebSocket(
        process.env.NEXT_PUBLIC_APP_ENV === 'prod' ? getCurApi() : process.env.NEXT_PUBLIC_WS_URL,
        {
            share: true,
            shouldReconnect: () => {
                return didUnmount.current === false
            },
        }
    )

    useEffect(() => {
        return () => {
            didUnmount.current = true
        }
    }, [])

    useEffect(() => {
        if (lastMessage && lastMessage.data && prevTime !== lastMessage?.timeStamp) {
            prevTime = lastMessage?.timeStamp
            // console.log(lastMessage.data, 'lastMessage.data', new Date().getTime())
            formatWsData(lastMessage.data).then((wsResult) => {
                const {key, data} = parseWsType(wsResult as string)

                if (key === 'auth' && data?.errno === 0) {
                    send(JSON.stringify({action: 'sub', symbol: ['USER_SWAP'], type: 'order_change_symbols'}), {
                        unsubPrev: false,
                        key: 'order',
                    })
                    send(JSON.stringify({action: 'sub', symbol: ['USER_SWAP'], type: 'pos_change_symbols'}), {
                        unsubPrev: false,
                        key: 'order',
                    })
                }
                dispatch({
                    type: key === 'transaction' ? 'transaction' : 'ws',
                    key,
                    val: data,
                })
            })
        }
    }, [lastMessage?.timeStamp])

    useEffect(() => {
        // reconnect
        if (readyState === 1 && sendArr.current && ifHasFailed) {
            const _arr = Object.values(sendArr.current)
            _arr.map((item) => {
                sendMessage(JSON.stringify(item))
            })
            setHasFailed(false)
        } else if (readyState === 3) {
            setHasFailed(true)
        }
    }, [readyState, sendMessage, ifHasFailed])

    const send = useCallback((subStr: string, subOption?: SubOptionsProps) => {
        if (subOption && subOption.key === 'order') {
            if (sendArr?.current && sendArr?.current['order']) {
                return false
            }
        }

        if (subOption && subOption.key === 'pos_order') {
            if (sendArr?.current && sendArr?.current['pos_order']) {
                return false
            }
        }

        if (subOption && subOption.unsubPrev) {
            const {key} = subOption
            const _unsub = (sendArr.current && sendArr.current[key]) || ''

            _unsub && sendMessage(JSON.stringify({..._unsub, action: 'unsub'}))

            sendArr.current = {...sendArr.current, [key]: JSON.parse(subStr)}
        }

        sendMessage(subStr)
    }, [])

    return {
        send,
        lastMessage,
        readyState,
        getWebSocket,
    }
}
