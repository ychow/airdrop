import {useState, useCallback, useEffect, useRef} from 'react'
import TIM from 'tim-js-sdk-ws'
import TIMUploadPlugin from 'tim-upload-plugin'

import useAlert from 'hooks/useAlert'
import {useIM} from 'utils/im-context'

let pendingType = 'old'
interface HistoryMessageProps {
    messageList: any[]
    isCompleted?: boolean
}
export default () => {
    // const dispatch = useDispatch()
    const [setAlert] = useAlert()
    const {imData} = useIM()

    // const imData = useSelector(store => store.Otc.imData)
    // im 是否已登录，登录后才可展示聊天按钮
    const [imLogined, setImLogined] = useState(false)
    // 未读信息列表，groupId[]
    const [newMessageList, setNewMessageList] = useState([])
    // 储存 tim 实例
    const TimRef = useRef(null)

    // 储存信息标志，ref 实例供 tim 侧回调函数读取
    const newMessageFlagRef = useRef(0)
    const [curFlag, setCurFlag] = useState(0)
    // 储存历史信息，ref 实例供 tim 侧回调函数读取
    const historyMessageRef = useRef<HistoryMessageProps>({messageList: []})
    const [curHistoryMessages, setCurHistoryMessages] = useState([])
    const [messageLoading, setMessageLoading] = useState(false)
    // 获取对话列表，以过滤出需要展示未读消息的列表

    const chatVisibleRef = useRef(false)
    const [chatVisible, setChatVisible] = useState(false)
    const curRoomKeyRef = useRef('')
    const [curRoomKey, setCurRoomKey] = useState('')

    // 根据 roomkey 设置某一个对话为已读
    const handleSetMessageRead = useCallback(
        (roomKey) => {
            const promise = TimRef.current.setMessageRead({
                conversationID: `GROUP${roomKey}`,
            })
            promise
                .then(() => {
                    // 已读上报成功，指定 ID 的会话的 unreadCount 属性值被置为0
                    setNewMessageList(newMessageList.filter((x) => x !== roomKey))
                })
                .catch((imError) => {
                    // 已读上报失败
                    // console.warn('setMessageRead error:', imError)
                    throw new Error(imError)
                })
        },
        [newMessageList]
    )

    const getConvasationList = useCallback(() => {
        const conversations = TimRef.current.getConversationList()
        conversations
            .then((imResponse) => {
                // console.log(imResponse.data, '对话列表')
                const {conversationList = []} = imResponse.data
                if (conversationList.length) {
                    const newList = conversationList
                        .filter((x) => x.conversationID.indexOf('GROUP') !== -1 && x.unreadCount > 0)
                        .map((x) => x.conversationID.split('GROUP')[1])
                    newList.length > 0 && setNewMessageList(newList)

                    // 如果当前有打开的对话框，则立即设置该对话已读
                    if (curRoomKeyRef.current) {
                        handleSetMessageRead(curRoomKeyRef.current)
                    }
                }
            })
            .catch((imError) => {
                // console.warn('login error:', imError) // 登录失败的相关信息
                throw new Error(imError)
            })
    }, [])

    // 当收到新消息时，更新对话框内消息展示
    const updateMessage = useCallback((newMessage) => {
        // 如果没有打开对话框，或者收到的消息不是当前对话的消息，则不更新
        if (!chatVisibleRef.current || newMessage.conversationID.indexOf(curRoomKeyRef.current) === -1) {
            return
        }
        // 设置当前信息的种类，影响对话框内消息渲染的方式
        pendingType = 'new'
        historyMessageRef.current = {
            ...historyMessageRef.current,
            messageList: [...historyMessageRef.current.messageList, newMessage],
        }
        setCurHistoryMessages(historyMessageRef.current.messageList)
        newMessageFlagRef.current++
        setCurFlag(newMessageFlagRef.current)
    }, [])

    // 打开对话框时，根据 roomkey 获取该对话历史列表
    const getMessage = useCallback((roomKey, nextId = '') => {
        if (historyMessageRef.current.isCompleted) {
            return
        }
        setMessageLoading(true)
        // 设置当前信息的种类，影响对话框内消息渲染的方式
        pendingType = 'old'
        const messages = TimRef.current.getMessageList({
            conversationID: `GROUP${roomKey}`,
            nextReqMessageID: nextId || '',
            count: 15,
        })
        messages
            .then((imResponse) => {
                // console.log(imResponse.data, 'messages result')
                const {isCompleted, nextReqMessageID, messageList = []} = imResponse.data
                if (messageList.length) {
                    // setCurHistoryMessages(messageList)
                    historyMessageRef.current = {
                        ...imResponse.data,
                        messageList: [...messageList, ...historyMessageRef.current.messageList].map((x) => {
                            return {
                                ...x,
                                isHistory: true,
                            }
                        }),
                    }

                    if (isCompleted) {
                        setTimeout(() => {
                            setMessageLoading(false)
                        }, 500)
                        setCurHistoryMessages(historyMessageRef.current.messageList)
                    } else {
                        getMessage(roomKey, nextReqMessageID)
                    }
                }
            })
            .catch((imError) => {
                throw new Error(imError)
            })
    }, [])

    // 发送消息，先在对话框内渲染，同步进行消息发送
    const sendMessage = useCallback(
        (payload, roomKey) => {
            const params = {
                to: roomKey,
                conversationType: TIM.TYPES.CONV_GROUP,
                isSending: true,
                payload,
            }
            pendingType = 'new'

            let newMessage

            // console.log(newMessage, 'check new message 1')

            if (payload.text) {
                newMessage = TimRef.current.createTextMessage(params)
            } else if (payload.file) {
                newMessage = TimRef.current.createImageMessage({
                    ...params,
                    // onProgress: (event) => {
                    //     console.log('file uploading:', event)
                    // },
                })
            }

            historyMessageRef.current = {
                ...historyMessageRef.current,
                messageList: [...historyMessageRef.current.messageList, newMessage],
            }
            setCurHistoryMessages(historyMessageRef.current.messageList)
            newMessageFlagRef.current++
            setCurFlag(newMessageFlagRef.current)
            // const newMessage = TimRef.current.createTextMessage(params)
            //   console.log(newMessage, 'check new message')
            const promise = TimRef.current.sendMessage(newMessage)

            // console.log(promise, 'check new message 2')

            promise
                // .then((imResponse) => {
                //     // 发送成功
                //     console.log(imResponse, 'check im response')
                // })
                .catch((imError) => {
                    // 发送失败
                    // console.log(imError.message, 'check new message 3')
                    if (imError.message.indexOf('beat word') !== -1) {
                        setAlert({type: 'error', message: '输入内容含敏感字眼，请重新输入'})

                        const revokePromise = TimRef.current.revokeMessage(newMessage)
                        revokePromise
                            // .then((imResponse) => {
                            //     // 消息撤回成功
                            //     console.log(imResponse, 'check imResponse')
                            // })
                            .catch((error) => {
                                throw new Error(error)
                            })
                    }
                    //   console.warn('sendMessage error:', imError)
                })
        },
        [curHistoryMessages]
    )

    const clearHistoryMessages = useCallback(() => {
        historyMessageRef.current = {messageList: []}
        setCurHistoryMessages([])
        newMessageFlagRef.current = 0
        setCurFlag(0)
        curRoomKeyRef.current = ''
        setCurRoomKey('')
    }, [])

    // 当获取到 sdkAppid 并且 tim 实例未初始化时，进行初始化 tim 实例操作
    // console.log(imData.sdkAppId, 'check sdkapp id')
    useEffect(() => {
        if (imData?.sdkAppId && !TimRef.current) {
            const options = {
                SDKAppID: imData.sdkAppId,
            }
            TimRef.current = TIM.create(options)
            TimRef.current.setLogLevel(1)
            // TimRef.current.registerPlugin({ 'cos-js-sdk': COS })
            TimRef.current.registerPlugin({'tim-upload-plugin': TIMUploadPlugin})

            TimRef.current.on(TIM.EVENT.SDK_READY, () => {
                // 收到离线消息和会话列表同步完毕通知，接入侧可以调用 sendMessage 等需要鉴权的接口
                // event.name - TIM.EVENT.SDK_READY
                getConvasationList()
            })

            TimRef.current.on(TIM.EVENT.SDK_READY, () => {
                // 收到离线消息和会话列表同步完毕通知，接入侧可以调用 sendMessage 等需要鉴权的接口
                // event.name - TIM.EVENT.SDK_READY
                getConvasationList()
            })

            TimRef.current.on(TIM.EVENT.MESSAGE_RECEIVED, (event) => {
                // console.log(event, 'check event')
                const {data} = event
                getConvasationList()
                if (data.length) {
                    data.forEach((x) => {
                        updateMessage(x)
                    })
                }
            })

            // 对 tim 进行登录，才可发送消息
            const promise = TimRef.current.login({
                userID: imData.userId,
                userSig: imData.userSig,
            })

            promise
                .then((imResponse) => {
                    setImLogined(true)
                    // console.log(imResponse.data, '登录成功') // 登录成功
                    if (imResponse.data.repeatLogin === true) {
                        // 标识账号已登录，本次登录操作为重复登录。v2.5.1 起支持
                        getConvasationList()
                    }
                })
                .catch((imError) => {
                    throw new Error(imError)
                })
        }
    }, [imData])

    const showChatModal = useCallback(
        (roomKey) => {
            setCurRoomKey(roomKey)
            curRoomKeyRef.current = roomKey
            getMessage(roomKey)

            setChatVisible(true)
            chatVisibleRef.current = true
            handleSetMessageRead(roomKey)
        },
        [curRoomKey, newMessageList]
    )

    const hideChatModal = useCallback(() => {
        setChatVisible(false)
        chatVisibleRef.current = false
        clearHistoryMessages()
    }, [])

    return {
        imLogined,
        showChatModal,
        newMessageList,
        chatVisible,
        curRoomKey,
        sendMessage,
        curHistoryMessages,
        hideChatModal,
        pendingType,
        getMessage,
        curFlag,
        messageLoading,
    }
}
