import {useRouter} from 'next/router'
import {useCallback, useRef} from 'react'
import nookies from 'nookies'

const useChangeLanguage = () => {
    const queryRef = useRef(null)
    const {push, query, locale: currentLocale} = useRouter()
    queryRef.current = query
    const changeLanguage = useCallback(
        (locale: string) => {
            let oldLang = 'en'
            let _realQuery = queryRef.current

            if (currentLocale !== locale) {
                const {pathname} = window.location
                if (pathname.indexOf('trade/spot') > -1) {
                    const _symbolArr = pathname.split('/')
                    const _symbol = _symbolArr[_symbolArr.length - 1]
                    _realQuery = {
                        symbol: _symbol,
                    }
                }

                push({query: _realQuery}, undefined, {locale})
            }

            nookies.destroy(null, 'NEXT_LOCALE', {
                path: '/',
            })

            nookies.set(null, 'NEXT_LOCALE', locale, {
                path: '/',
                maxAge: 30 * 24 * 60 * 60,
            })

            if (['ZH-CN', 'ZH-HK'].indexOf(locale.toUpperCase()) > -1) {
                oldLang = locale.toUpperCase().indexOf('CN') > -1 ? 'zh' : 'hk'
            }

            localStorage.site_lang = oldLang
        },
        [query, currentLocale]
    )

    return changeLanguage
}

export default useChangeLanguage
